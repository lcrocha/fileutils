#pragma once

#define ROOT_ELEMENT	"nppFileUtils"
#define VERSION_ATTR	"version"

#define Q(x) #x
#define QUOTE(x) Q(x)

#define APP_VERSION_MAJOR	0
#define APP_VERSION_MINOR	5
#define APP_VERSION_RELEASE	0
#define APP_VERSION_BUILD	19

#define APP_VERSION_COMMA		APP_VERSION_MAJOR, APP_VERSION_MINOR, APP_VERSION_RELEASE, APP_VERSION_BUILD
#define APP_FILEVERSION_STR		QUOTE(APP_VERSION_MAJOR.APP_VERSION_MINOR.APP_VERSION_RELEASE.APP_VERSION_BUILD)
#define APP_PRODUCTVERSION_STR	QUOTE(APP_VERSION_MAJOR.APP_VERSION_MINOR.APP_VERSION_RELEASE.APP_VERSION_BUILD)

#define APP_CONTACT_STR				"leonardo.correa.rocha@hotmail.com"
#define APP_COMPANY_STR				"Leonardo Correa Rocha / leonardo.correa.rocha@hotmail.com"
#define APP_DESCRIPTION_STR			"A hasher, signer, cipher, encoder, random generator tool"
#define APP_INTERNALNAME_STR		"FU.exe"
#define APP_COPYRIGHT_STR			"Copyright (C) 2019 LCR"
#define APP_ORIGINALFILENAME_STR	"FU.exe"
#define APP_PRODUCTNAME_STR			"A File Utils Application"
#define APP_NAME_STR				"FileUtils"
