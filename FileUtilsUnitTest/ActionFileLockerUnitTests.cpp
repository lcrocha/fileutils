#pragma once
#include "stdafx.h"
#include "../FileUtilsEngine/ActionLocker.h"
#include "../../Libs/Helper/Concurrent/Event.h"
#include "../../Libs/Helper/Utils/Utils.h"
#include <thread>
using namespace FileUtilsEngine;

namespace FileUtilsEngineUnitTests
{
	TEST_CLASS(ActionFileLockerUnitTests)
	{
	public:
		TEST_METHOD(LockFile)
		{
			auto file = "_1";
			auto size = 1024ul;
			IO::FileHandler::Delete(file);
			Utils::CreateFileData(Helper::BufferType::BinaryRandom, file, size);
			Assert::IsTrue(ReturnTraits::IsSuccess(ActionLocker().LockFile(file, nullptr)));
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(file)));
		}

		TEST_METHOD(EnumerateLockingApps)
		{
			auto file = "_1";
			auto size = 1024ul;
			IO::FileHandler::Delete(file);
			Utils::CreateFileData(Helper::BufferType::BinaryRandom, file, size);
			//Creating another thread
			Concurrent::Event e(true);
			std::thread t([&] { Assert::IsTrue(ReturnTraits::IsSuccess(ActionLocker().LockFile(file, [&] { e.Wait(); }))); });
			//Listing locking apps
			AppInfos apps;
			ActionLocker().GetAppsLockingFile(file, apps);
			Assert::IsTrue(!apps.empty());
			//Exiting thread
			e.Notify();
			t.join();
			//Cleaning file
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(file)));
		}
	};
}