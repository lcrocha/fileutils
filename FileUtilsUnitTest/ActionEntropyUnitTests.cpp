#pragma once
#include "stdafx.h"
#include "../FileUtilsEngine/ActionEntropy.h"
#include "../../libs/Helper/Time/Stopwatch.h"
#include "../../libs/Helper/Utils/RandomNumberGenerator.h"
using namespace FileUtilsEngine;

namespace FileUtilsEngineUnitTests
{
	template<class... Args>
	static void LogMessage(const char* format, Args ...args)
	{
		std::vector<char> b(1 + std::snprintf(nullptr, 0, format, std::forward<Args>(args)...));
		std::snprintf(&b[0], b.size(), format, std::forward<Args>(args)...);
		Logger::WriteMessage(&b[0]);
	}

	TEST_CLASS(ActionEntropyUnitTests)
	{
	private:
		Utils::RandomNumberGenerator _rng;
		ulong _maxFileSize = 16384ul;
		const std::string _inputFilePath = "_1";
	public:
		ActionEntropyUnitTests() {}

		TEST_METHOD(CalculatingRandomSizeContent)
		{
			
			IO::FileHandler::Delete(_inputFilePath);
			for (auto& bt : Helper::BufferTypesList)
			{
				Time::Stopwatch sw;
				//Creating files
				auto size = _rng.GetUniformDistribution(1ul, _maxFileSize);
				Assert::IsTrue(Utils::CreateFileData(bt, _inputFilePath.c_str(), size));
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::IsExist(_inputFilePath)));
				//Calculating entropy
				float e = 0.0f;
				Assert::IsTrue(ReturnTraits::IsSuccess(ActionEntropy().CalcutateEntropyFromFile(_inputFilePath, e)));
				//Logging
				LogMessage("CRSC: %17s, %2.2f [%5dms]", Helper::GetBufferType(bt), e, sw.GetElapsed());
			}
		}

		TEST_METHOD(EmptyParameters)
		{
			float e = 0.0f;
			Assert::IsTrue(Return::InputFilePathEmpty == ActionEntropy().CalcutateEntropyFromFile(std::string(), e));
		}

		TEST_METHOD(FileEmpty)
		{
			IO::FileHandler::Delete("_1");
			IO::FileHandler f;
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(f.Open("_1", IO::FileHandler::AccessMode::Write)));
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(f.Close()));
			float e = 0.0f;
			Assert::IsTrue(ActionEntropy().CalcutateEntropyFromFile("_1", e) == Return::InputFileEmpty);
			IO::FileHandler::Delete("_1");
		}

		TEST_METHOD(FileNotFound)
		{
			IO::FileHandler::Delete("_1");
			float e = 0.0f;
			Assert::IsTrue(ActionEntropy().CalcutateEntropyFromFile("_1", e) == Return::InputFileNotFound);
		}
	};
}
