#pragma once
#include "stdafx.h"
#include "../FileUtilsEngine/ActionSymmetricCipher.h"
#include "../FileUtilsEngine/FileUtilsEngine.h"
#include "../../Libs/Helper/Utils/randomNumberGenerator.h"
#include "../../libs/Helper/Time/Stopwatch.h"
#include <iomanip>
#include <algorithm>
using namespace FileUtilsEngine;

namespace FileUtilsEngineUnitTests
{
	TEST_CLASS(ActionSymmetricCipherUnitTests)
	{
	private:
		Utils::RandomNumberGenerator _rng;
		const ushort _totalWorkers = 4u;
		const ushort _totalTests = 1u;
		const ulong _maxFileSize = 1ul * 1024ul;
		const char* _fileSecretPath = "_secret.bin";
		const char* _inputFilePath = "_file.bin";
		const char* _cipheredFilePath = "_file.bin.ciphered";
		const char* _uncipheredFilePath = "_file.bin.unciphered";
		const Crypto::HasherAlgorithm _hashAlg = Crypto::HasherAlgorithm::SHA1;
		Crypto::SymmetricChainingModeInfos _symInfos;
		Crypto::AuthSymmetricChainingModeInfos _authInfos;
		Crypto::EncodingInfos _encodingInfos;
		//Auxilliary
		struct TestOperationStatus
		{
			bool operator()(const StatusOperationType& ot, const ulonglong partial, const ulonglong total)
			{
				//LogMessage("%s %d/%d", text.c_str(), partial, total);
				auto sot = GetOperationType(ot);
				Assert::IsTrue(nullptr != sot && ::strlen(sot) > 0);
				Assert::IsTrue(partial <= total);
				Assert::IsTrue(total != 0);

				return false;
			}
		};

		static const char* GetBool(const bool& b, const char* c = "X")
		{
			return b ? c : " ";
		}

		const char* GetAcm(const Crypto::AuthSymmetricChainingMode& acm)
		{
			return std::find_if(_authInfos.begin(), _authInfos.end(), [&](const Crypto::AuthSymmetricChainingModeInfo& i) { return i.mode == acm; })->shortName;
		}

		void LogTest(const char* test, const uint& t, const Helper::BufferType& bt, const ulong& size, const char* encoding, const bool prettyPring, const ulong elapsed)
		{
			LogMessage("%4s: %2d, %16s, %4d, %s, [%s], %5dms", test, t, Helper::GetBufferType(bt), size, encoding,prettyPring ? "X" : " ", elapsed);
		}

		template<class... Args>
		static void LogMessage(const char* format, Args ...args)
		{
			std::vector<char> b(1 + std::snprintf(nullptr, 0, format, std::forward<Args>(args)...));
			std::snprintf(&b[0], b.size(), format, std::forward<Args>(args)...);
			Logger::WriteMessage(&b[0]);
		}

	public:
		ActionSymmetricCipherUnitTests()
		{
			Crypto::QuerySymmetricChainingModeInfos(_symInfos);
			Crypto::QueryAuthSymmetricChainingModeInfos(_authInfos);
			Crypto::QueryEncodingInfos(_encodingInfos);
		}

		TEST_METHOD(CipherUncipherRandomFileWithAGoodSecret)
		{
			IO::FileHandler::Delete(_inputFilePath);
			IO::FileHandler::Delete(_cipheredFilePath);
			IO::FileHandler::Delete(_uncipheredFilePath);
			for (auto t = 0ul; t < _totalTests; t++) for (auto& bt : Helper::BufferTypesList) for (const auto& e : _encodingInfos) for (auto prettyPrint : { true, false })
			{
				Time::Stopwatch sw;
				//Creating files
				auto size = _rng.GetUniformDistribution(1ul, _maxFileSize);
				Assert::IsTrue(Utils::CreateFileData(bt, _inputFilePath, size));
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::IsExist(_inputFilePath)));
				//Creating secret password
				Buffer secret(16);
				Assert::IsTrue(Crypto::ReturnTraits::IsSuccess(Crypto::GenRandom(secret)));
				//Calculating initial file hash
				Buffer initialFileHash;
				Assert::IsTrue(ReturnTraits::IsSuccess(CalculateHashFromFile(_inputFilePath, _hashAlg, initialFileHash)));
				//Ciphering file
				FileUtilsEngine::GetConfig()->SetConfig(e.encoding, prettyPrint);
				Assert::IsTrue(ReturnTraits::IsSuccess(ActionSymmetricCipher().CipherFile(secret, _inputFilePath, _cipheredFilePath, TestOperationStatus())));
				//Unciphering file
				Assert::IsTrue(ReturnTraits::IsSuccess(ActionSymmetricCipher().UncipherFile(secret, _cipheredFilePath, _uncipheredFilePath, TestOperationStatus())));
				//Getting hash from unciphered file
				Buffer uncipheredFileHash;
				Assert::IsTrue(ReturnTraits::IsSuccess(CalculateHashFromFile(_uncipheredFilePath, _hashAlg, uncipheredFileHash)));
				//Checking results
				Assert::IsTrue(initialFileHash == uncipheredFileHash);
				//Cleanup
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(_cipheredFilePath)));
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(_uncipheredFilePath)));
				//Cleanup
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(_inputFilePath)));
				//Logging
				LogTest("CRGS", t, bt, size, e.shortName, prettyPrint, sw.GetElapsed());
			}
		}

		TEST_METHOD(CipherUncipherRandomFileWithABadSecret)
		{
			IO::FileHandler::Delete(_inputFilePath);
			IO::FileHandler::Delete(_cipheredFilePath);
			IO::FileHandler::Delete(_uncipheredFilePath);
			for (auto t = 0ul; t < _totalTests; t++) for (auto& bt : Helper::BufferTypesList) for (const auto& e : _encodingInfos) for (auto prettyPrint : { true, false })
			{
				Time::Stopwatch sw;
				//Creating random content file
				auto size = _rng.GetUniformDistribution(1ul, _maxFileSize);
				Assert::IsTrue(Utils::CreateFileData(bt, _inputFilePath, size));
				//Calculating initial file hash
				Buffer initialFileHash;
				Assert::IsTrue(ReturnTraits::IsSuccess(CalculateHashFromFile(_inputFilePath, _hashAlg, initialFileHash)));
				//Ciphering file
				FileUtilsEngine::GetConfig()->SetConfig(e.encoding, prettyPrint);
				Buffer s1("T", "T" + 1), s2("1", "1" + 1);
				Assert::IsTrue(ReturnTraits::IsSuccess(ActionSymmetricCipher().CipherFile(s1, _inputFilePath, _cipheredFilePath, TestOperationStatus())));
				//Unciphering file with bad secret
				Assert::IsTrue(ActionSymmetricCipher().UncipherFile(s2, _cipheredFilePath, _uncipheredFilePath, TestOperationStatus()) == Return::CipheredReadingBreak);
				//Unciphering file with good secret
				Assert::IsTrue(ReturnTraits::IsSuccess(ActionSymmetricCipher().UncipherFile(s1, _cipheredFilePath, _uncipheredFilePath, TestOperationStatus())));
				//Getting hash from unciphered file
				Buffer uncipheredFileHash;
				Assert::IsTrue(ReturnTraits::IsSuccess(CalculateHashFromFile(_uncipheredFilePath, _hashAlg, uncipheredFileHash)));
				//Checking results
				Assert::IsTrue(initialFileHash == uncipheredFileHash);
				//Cleanup
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(_cipheredFilePath)));
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(_uncipheredFilePath)));
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(_inputFilePath)));
				//Logging
				LogTest("CRBS", t, bt, size, e.shortName, prettyPrint, sw.GetElapsed());
			}
		}

		TEST_METHOD(CipherUncipherRandomFileUsingSecretFile)
		{
			IO::FileHandler::Delete(_inputFilePath);
			IO::FileHandler::Delete(_cipheredFilePath);
			IO::FileHandler::Delete(_uncipheredFilePath);
			for (auto t = 0ul; t < _totalTests; t++) for (auto& bt : Helper::BufferTypesList) for (const auto& e : _encodingInfos) for (auto prettyPrint : { true, false })
			{
				Time::Stopwatch sw;
				//Creating random content file
				auto size = _rng.GetUniformDistribution(1, _maxFileSize);
				Assert::IsTrue(Utils::CreateFileData(bt, _inputFilePath, size));
				//Creating secret password
				Buffer secret(128);
				Assert::IsTrue(Crypto::ReturnTraits::IsSuccess(Crypto::GenRandom(secret)));
				//Saving secret file
				Assert::IsTrue(ReturnTraits::IsSuccess(WriteDataOnFile(FileType::Secret, _fileSecretPath, secret)));
				//Calculating initial file hash
				Buffer initialFileHash;
				Assert::IsTrue(ReturnTraits::IsSuccess(CalculateHashFromFile(_inputFilePath, _hashAlg, initialFileHash)));
				//Ciphering file
				FileUtilsEngine::GetConfig()->SetConfig(e.encoding, prettyPrint);
				Assert::IsTrue(ReturnTraits::IsSuccess(ActionSymmetricCipher().CipherFileUsingSecretFile(_fileSecretPath, _inputFilePath, _cipheredFilePath, TestOperationStatus())));
				//Unciphering file
				Assert::IsTrue(ReturnTraits::IsSuccess(ActionSymmetricCipher().UncipherFileUsingSecretFile(_fileSecretPath, _cipheredFilePath, _uncipheredFilePath, TestOperationStatus())));
				//Getting hash from unciphered file
				Buffer uncipheredFileHash;
				Assert::IsTrue(ReturnTraits::IsSuccess(CalculateHashFromFile(_uncipheredFilePath, _hashAlg, uncipheredFileHash)));
				//Checking results
				Assert::IsTrue(initialFileHash == uncipheredFileHash);
				//Cleanup
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(_cipheredFilePath)));
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(_uncipheredFilePath)));
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(_fileSecretPath)));
				//Logging
				LogTest("CRSF", t, bt, size, e.shortName, prettyPrint, sw.GetElapsed());
			}
			//Cleanup
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(_inputFilePath)));
		}

		TEST_METHOD(UncipherDamagedCipheredFile)
		{
			for (const auto& e : _encodingInfos) for (auto prettyPrint : { true, false })
			{
				FileUtilsEngine::GetConfig()->SetConfig(e.encoding, prettyPrint);
				//Initializing
				IO::FileHandler::Delete(_cipheredFilePath);
				IO::FileHandler::Delete(_uncipheredFilePath);
				//Creating random content file
				Assert::IsTrue(Utils::CreateFileData(Helper::BufferType::BinaryRandom, _cipheredFilePath, 1024ul));
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::IsExist(_cipheredFilePath)));
				//Unciphering file with good secret
				Assert::IsTrue(ActionSymmetricCipher().UncipherFile(Buffer("_", "_" + 1), _cipheredFilePath, _uncipheredFilePath) == Return::CipheredFileNotRecognized);
				//Cleanup
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(_cipheredFilePath)));
			}
		}

		TEST_METHOD(EmptyParameters)
		{
			for (const auto& e : _encodingInfos) for (auto prettyPrint : { true, false })
			{
				FileUtilsEngine::GetConfig()->SetConfig(e.encoding, prettyPrint);
				//CipherFile
				Assert::IsTrue(ActionSymmetricCipher().CipherFile(Buffer(), std::string(), std::string()) == Return::SecretBufferEmpty);
				Assert::IsTrue(ActionSymmetricCipher().CipherFile(Buffer("_", "_" + 1), std::string(), std::string()) == Return::InputFilePathEmpty);
				Assert::IsTrue(ActionSymmetricCipher().CipherFile(Buffer("_", "_" + 1), "_1", std::string()) == Return::CipheredFilePathEmpty);
				//CipherFileUsingSecretFile
				Assert::IsTrue(ActionSymmetricCipher().CipherFileUsingSecretFile(std::string(), std::string(), std::string()) == Return::SecretFilePathEmpty);
				Assert::IsTrue(ActionSymmetricCipher().CipherFileUsingSecretFile("_", std::string(), std::string()) == Return::InputFilePathEmpty);
				Assert::IsTrue(ActionSymmetricCipher().CipherFileUsingSecretFile("_", "_1", std::string()) == Return::CipheredFilePathEmpty);
				//UncipherFile
				Assert::IsTrue(ActionSymmetricCipher().UncipherFile(Buffer(), std::string(), std::string()) == Return::SecretBufferEmpty);
				Assert::IsTrue(ActionSymmetricCipher().UncipherFile(Buffer("_", "_" + 1), std::string(), std::string()) == Return::CipheredFilePathEmpty);
				Assert::IsTrue(ActionSymmetricCipher().UncipherFile(Buffer("_", "_" + 1), "_1", std::string()) == Return::UncipheredFilePathEmpty);
				//UncipherFileUsingSecretFile
				Assert::IsTrue(ActionSymmetricCipher().UncipherFileUsingSecretFile(std::string(), std::string(), std::string()) == Return::SecretFilePathEmpty);
				Assert::IsTrue(ActionSymmetricCipher().UncipherFileUsingSecretFile("_", std::string(), std::string()) == Return::CipheredFilePathEmpty);
				Assert::IsTrue(ActionSymmetricCipher().UncipherFileUsingSecretFile("_", "_1", std::string()) == Return::UncipheredFilePathEmpty);
			}
		}

		TEST_METHOD(FileNotFound)
		{
			for (const auto& e : _encodingInfos) for (auto prettyPrint : { true, false })
			{
				FileUtilsEngine::GetConfig()->SetConfig(e.encoding, prettyPrint);
				IO::FileHandler::Delete("_0");
				IO::FileHandler::Delete("_1");
				IO::FileHandler::Delete("_2");
				IO::FileHandler f0, f1;
				//All secret not found
				Assert::IsTrue(ActionSymmetricCipher().CipherFileUsingSecretFile("_0", "_1", "_2") == Return::SecretFileNotFound);
				Assert::IsTrue(ActionSymmetricCipher().UncipherFileUsingSecretFile("_0", "_1", "_2") == Return::SecretFileNotFound);
				//Creating f0
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(f0.Open("_0", IO::FileHandler::AccessMode::Write)));
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(f0.WriteData(1024, Buffer("T", "T" + 1))));
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(f0.Flush()));
				//All input not found
				Assert::IsTrue(ActionSymmetricCipher().CipherFile(Buffer("T", "T" + 1), "_1", "_2") == Return::InputFileNotFound);
				Assert::IsTrue(ActionSymmetricCipher().CipherFileUsingSecretFile("_0", "_1", "_2") == Return::InputFileNotFound);
				Assert::IsTrue(ActionSymmetricCipher().UncipherFile(Buffer("T", "T" + 1), "_1", "_2") == Return::CipheredFileNotFound);
				Assert::IsTrue(ActionSymmetricCipher().UncipherFileUsingSecretFile("_0", "_1", "_2") == Return::CipheredFileNotFound);
				//Creating f1
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(f1.Open("_1", IO::FileHandler::AccessMode::Write)));
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(f1.WriteData(1024, Buffer("T", "T" + 1))));
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(f1.Flush()));
				//All cipher ok
				Assert::IsTrue(ReturnTraits::IsSuccess(ActionSymmetricCipher().CipherFile(Buffer("T", "T" + 1), "_1", "_2")));
				Assert::IsTrue(ReturnTraits::IsSuccess(ActionSymmetricCipher().CipherFileUsingSecretFile("_0", "_1", "_2")));
				Assert::IsTrue(ReturnTraits::IsSuccess(ActionSymmetricCipher().UncipherFile(Buffer("T", "T" + 1), "_2", "_1")));
				Assert::IsTrue(ReturnTraits::IsSuccess(ActionSymmetricCipher().UncipherFileUsingSecretFile("_0", "_2", "_1")));
				//Cleanup
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(f0.Delete()));
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(f1.Delete()));
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete("_2")));
			}
		}

		TEST_METHOD(FileEmpty)
		{
			for (const auto& e : _encodingInfos) for (auto prettyPrint : { true, false })
			{
				FileUtilsEngine::GetConfig()->SetConfig(e.encoding, prettyPrint);
				IO::FileHandler::Delete("_0");
				IO::FileHandler::Delete("_1");
				IO::FileHandler::Delete("_2");
				IO::FileHandler f0, f1;
				//Empty f0
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(f0.Open("_0", IO::FileHandler::AccessMode::Write)));
				//All secret empty
				Assert::IsTrue(ActionSymmetricCipher().CipherFileUsingSecretFile("_0", "_1", "_2") == Return::SecretFileEmpty);
				Assert::IsTrue(ActionSymmetricCipher().UncipherFileUsingSecretFile("_0", "_1", "_2") == Return::SecretFileEmpty);
				//Updating f0
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(f0.WriteData(1024, Buffer("T", "T" + 1))));
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(f0.Flush()));
				//Empty f1
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(f1.Open("_1", IO::FileHandler::AccessMode::Write)));
				//All input empty
				Assert::IsTrue(ActionSymmetricCipher().CipherFile(Buffer("T", "T" + 1), "_1", "_2") == Return::InputFileEmpty);
				Assert::IsTrue(ActionSymmetricCipher().CipherFileUsingSecretFile("_0", "_1", "_2") == Return::InputFileEmpty);
				Assert::IsTrue(ActionSymmetricCipher().UncipherFile(Buffer("T", "T" + 1), "_1", "_2") == Return::CipheredFileEmpty);
				Assert::IsTrue(ActionSymmetricCipher().UncipherFileUsingSecretFile("_0", "_1", "_2") == Return::CipheredFileEmpty);
				//Updating f1
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(f1.WriteData(1024, Buffer("T", "T" + 1))));
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(f1.Flush()));
				//All cipher ok
				Assert::IsTrue(ReturnTraits::IsSuccess(ActionSymmetricCipher().CipherFile(Buffer("T", "T" + 1), "_1", "_2")));
				Assert::IsTrue(ReturnTraits::IsSuccess(ActionSymmetricCipher().CipherFileUsingSecretFile("_0", "_1", "_2")));
				Assert::IsTrue(ReturnTraits::IsSuccess(ActionSymmetricCipher().UncipherFile(Buffer("T", "T" + 1), "_2", "_1")));
				Assert::IsTrue(ReturnTraits::IsSuccess(ActionSymmetricCipher().UncipherFileUsingSecretFile("_0", "_2", "_1")));
				//Cleanup
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(f0.Delete()));
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(f1.Delete()));
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete("_2")));
			}
		}
	};
}