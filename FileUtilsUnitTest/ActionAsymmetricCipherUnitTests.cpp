#pragma once
#include "stdafx.h"
#include "../FileUtilsEngine/ActionAsymmetricCipher.h"
#include "../FileUtilsEngine/FileUtilsEngine.h"
#include "../../Libs/Helper/Utils\randomNumberGenerator.h"
using namespace FileUtilsEngine;

namespace FileUtilsEngineUnitTests
{
	TEST_CLASS(ActionAsymmetricCipherUnitTests)
	{
	private:
		Utils::RandomNumberGenerator _rng;
		const ulong _maxFileSize = 4096ul;
		std::vector<Crypto::HasherAlgorithm> _hashAlgs;
		std::vector<Crypto::AsymmetricKeySize> _keySizes;
		const char* _privateKeyFilePath = "_prk";
		const char* _publicKeyFilePath = "_puk";
		const char* _inputFilePath = "_input";
		const char* _cipheredFilePath = "_ciphered";
		const char* _uncipheredFilePath = "_unciphered";
	public:
		ActionAsymmetricCipherUnitTests()
		{
			_hashAlgs =
			{
				Crypto::HasherAlgorithm::MD2,
				Crypto::HasherAlgorithm::MD4,
				Crypto::HasherAlgorithm::MD5,
				Crypto::HasherAlgorithm::SHA1,
				Crypto::HasherAlgorithm::SHA256,
				Crypto::HasherAlgorithm::SHA384,
				Crypto::HasherAlgorithm::SHA512
			};

			_keySizes =
			{
				Crypto::AsymmetricKeySize::RSA_512,
				Crypto::AsymmetricKeySize::RSA_1024,
				Crypto::AsymmetricKeySize::RSA_2048,
				Crypto::AsymmetricKeySize::RSA_4096,
			};
		}

		TEST_METHOD(CipherUncipherRandomFileWithABadKeys)
		{
			//Cleaning up
			IO::FileHandler::Delete(_privateKeyFilePath);
			IO::FileHandler::Delete(_publicKeyFilePath);
			IO::FileHandler::Delete(_inputFilePath);
			IO::FileHandler::Delete(_cipheredFilePath);
			//Creating input file
			Assert::IsTrue(Utils::CreateFileData(Helper::BufferType::BinaryRandom, _inputFilePath, 1024ul));
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::IsExist(_inputFilePath)));
			//Creating publicKey file
			Assert::IsTrue(Utils::CreateFileData(Helper::BufferType::BinaryRandom, _publicKeyFilePath, 1024ul));
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::IsExist(_publicKeyFilePath)));
			//Creating privateKey file
			Assert::IsTrue(Utils::CreateFileData(Helper::BufferType::BinaryRandom, _privateKeyFilePath, 1024ul));
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::IsExist(_privateKeyFilePath)));
			//ciphering input file
			Assert::IsTrue(ActionAsymmetricCipher().CipherFile(_publicKeyFilePath, _inputFilePath, _cipheredFilePath) == Return::InputReadingBreak);
			Assert::IsTrue(IO::FileHandler::IsExist(_cipheredFilePath) == IO::Return::NotFound);
			//Creating ciphered file
			Assert::IsTrue(Utils::CreateFileData(Helper::BufferType::BinaryRandom, _cipheredFilePath, 1024ul));
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::IsExist(_cipheredFilePath)));
			//unciphering ciphered file
			Assert::IsTrue(ActionAsymmetricCipher().UncipherFile(_privateKeyFilePath, _cipheredFilePath, _uncipheredFilePath) == Return::CipheredReadingBreak);
			Assert::IsTrue(IO::FileHandler::IsExist(_uncipheredFilePath) == IO::Return::NotFound);
			//Cleaning up
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(_privateKeyFilePath)));
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(_publicKeyFilePath)));
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(_inputFilePath)));
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(_cipheredFilePath)));
		}

		TEST_METHOD(CipherUncipherRandomFileWithAGoodKeys)
		{
			//Cleaning up
			IO::FileHandler::Delete(_privateKeyFilePath);
			IO::FileHandler::Delete(_publicKeyFilePath);
			IO::FileHandler::Delete(_inputFilePath);
			IO::FileHandler::Delete(_cipheredFilePath);
			for (auto keySize : _keySizes)
			{
				//Creating input file
				Assert::IsTrue(Utils::CreateFileData(Helper::BufferType::BinaryRandom, _inputFilePath, _rng.GetUniformDistribution(1, _maxFileSize)));
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::IsExist(_inputFilePath)));
				//creating publicKey and privateKey
				Assert::IsTrue(ReturnTraits::IsSuccess(ActionAsymmetricCipher().GenerateKeyPair(keySize, _privateKeyFilePath, _publicKeyFilePath)));
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::IsExist(_privateKeyFilePath)));
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::IsExist(_publicKeyFilePath)));
				//ciphering input file
				Assert::IsTrue(ReturnTraits::IsSuccess(ActionAsymmetricCipher().CipherFile(_publicKeyFilePath, _inputFilePath, _cipheredFilePath)));
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::IsExist(_cipheredFilePath)));
				//unciphering ciphered file
				Assert::IsTrue(ReturnTraits::IsSuccess(ActionAsymmetricCipher().UncipherFile(_privateKeyFilePath, _cipheredFilePath, _uncipheredFilePath)));
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::IsExist(_uncipheredFilePath)));
				for (auto hashAlg : _hashAlgs)
				{
					//getting hash from input
					Buffer inputHash;
					Assert::IsTrue(ReturnTraits::IsSuccess(CalculateHashFromFile(_inputFilePath, hashAlg, inputHash)));
					//getting hash from input
					Buffer uncipheredHash;
					Assert::IsTrue(ReturnTraits::IsSuccess(CalculateHashFromFile(_uncipheredFilePath, hashAlg, uncipheredHash)));
					//Checking results
					Assert::IsTrue(inputHash == uncipheredHash);
				}
				//Cleaning up
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(_privateKeyFilePath)));
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(_publicKeyFilePath)));
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(_inputFilePath)));
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(_cipheredFilePath)));
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(_uncipheredFilePath)));
			}
		}

		TEST_METHOD(UncipherDamagedCipheredFile)
		{
			//Initializing
			IO::FileHandler::Delete(_privateKeyFilePath);
			IO::FileHandler::Delete(_publicKeyFilePath);
			IO::FileHandler::Delete(_cipheredFilePath);
			IO::FileHandler::Delete(_uncipheredFilePath);
			//Creating random ciphered file
			Assert::IsTrue(Utils::CreateFileData(Helper::BufferType::BinaryRandom, _cipheredFilePath, 1024ul));
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::IsExist(_cipheredFilePath)));
			//Creating random unciphered file
			Assert::IsTrue(Utils::CreateFileData(Helper::BufferType::BinaryRandom, _uncipheredFilePath, 1024ul));
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::IsExist(_uncipheredFilePath)));
			for (auto keySize : _keySizes)
			{
				//creating publicKey and privateKey
				Assert::IsTrue(ReturnTraits::IsSuccess(ActionAsymmetricCipher().GenerateKeyPair(keySize, _privateKeyFilePath, _publicKeyFilePath)));
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::IsExist(_privateKeyFilePath)));
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::IsExist(_publicKeyFilePath)));
				//Unciphering file with good secret
				Assert::IsTrue(ActionAsymmetricCipher().UncipherFile(_privateKeyFilePath, _cipheredFilePath, _uncipheredFilePath) == Return::CipheredReadingBreak);
				//Cleaning up
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(_privateKeyFilePath)));
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(_publicKeyFilePath)));
			}
			//Cleaning up
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(_cipheredFilePath)));
		}

		TEST_METHOD(EmptyParameters)
		{
			for (auto keySize : _keySizes)
			{
				//GenerateKeyPair
				Assert::IsTrue(ActionAsymmetricCipher().GenerateKeyPair(keySize, std::string(), std::string()) == Return::PrivateKeyFilePathEmpty);
				Assert::IsTrue(ActionAsymmetricCipher().GenerateKeyPair(keySize, "_1", std::string()) == Return::PublicKeyFilePathEmpty);
				//CipherFile
				Assert::IsTrue(ActionAsymmetricCipher().CipherFile(std::string(), std::string(), std::string()) == Return::PublicKeyFilePathEmpty);
				Assert::IsTrue(ActionAsymmetricCipher().CipherFile("_1", std::string(), std::string()) == Return::InputFilePathEmpty);
				Assert::IsTrue(ActionAsymmetricCipher().CipherFile("_1", "_2", std::string()) == Return::CipheredFilePathEmpty);
				//UncipheredFile
				Assert::IsTrue(ActionAsymmetricCipher().UncipherFile(std::string(), std::string(), std::string()) == Return::PrivateKeyFilePathEmpty);
				Assert::IsTrue(ActionAsymmetricCipher().UncipherFile("_1", std::string(), std::string()) == Return::InputFilePathEmpty);
				Assert::IsTrue(ActionAsymmetricCipher().UncipherFile("_1", "_2", std::string()) == Return::UncipheredFilePathEmpty);
			}
		}

		TEST_METHOD(FileNotFound)
		{
			//Cleaning up
			IO::FileHandler::Delete(_privateKeyFilePath);
			IO::FileHandler::Delete(_publicKeyFilePath);
			IO::FileHandler::Delete(_inputFilePath);
			IO::FileHandler::Delete(_cipheredFilePath);
			for (auto keySize : _keySizes)
			{
				//privateKey not found
				Assert::IsTrue(ActionAsymmetricCipher().UncipherFile(_privateKeyFilePath, _cipheredFilePath, _uncipheredFilePath) == Return::PrivateKeyFileNotFound);
				//publicKey not found
				Assert::IsTrue(ActionAsymmetricCipher().CipherFile(_publicKeyFilePath, _inputFilePath, _cipheredFilePath) == Return::PublicKeyFileNotFound);
				//Updating publicKey and privateKey
				Assert::IsTrue(ReturnTraits::IsSuccess(ActionAsymmetricCipher().GenerateKeyPair(keySize, _privateKeyFilePath, _publicKeyFilePath)));
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::IsExist(_privateKeyFilePath)));
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::IsExist(_publicKeyFilePath)));
				//input file not found
				Assert::IsTrue(ActionAsymmetricCipher().CipherFile(_publicKeyFilePath, _inputFilePath, _cipheredFilePath) == Return::InputFileNotFound);
				//Empty ciphered file
				Assert::IsTrue(ActionAsymmetricCipher().UncipherFile(_privateKeyFilePath, _cipheredFilePath, _uncipheredFilePath) == Return::CipheredFileNotFound);
				//Cleaning up
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(_privateKeyFilePath)));
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(_publicKeyFilePath)));
			}
		}

		TEST_METHOD(FileEmpty)
		{
			//Cleaning up
			IO::FileHandler::Delete(_privateKeyFilePath);
			IO::FileHandler::Delete(_publicKeyFilePath);
			IO::FileHandler::Delete(_inputFilePath);
			IO::FileHandler::Delete(_cipheredFilePath);
			IO::FileHandler::Delete(_uncipheredFilePath);
			for (auto keySize : _keySizes)
			{
				//Empty privateKey
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler().Open(_privateKeyFilePath, IO::FileHandler::AccessMode::Write)));
				Assert::IsTrue(ActionAsymmetricCipher().UncipherFile(_privateKeyFilePath, _cipheredFilePath, _uncipheredFilePath) == Return::PrivateKeyFileEmpty);
				//Empty publicKey
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler().Open(_publicKeyFilePath, IO::FileHandler::AccessMode::Write)));
				Assert::IsTrue(ActionAsymmetricCipher().CipherFile(_publicKeyFilePath, _inputFilePath, _cipheredFilePath) == Return::PublicKeyFileEmpty);
				//Updating publicKey and privateKey
				Assert::IsTrue(ReturnTraits::IsSuccess(ActionAsymmetricCipher().GenerateKeyPair(keySize, _privateKeyFilePath, _publicKeyFilePath)));
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::IsExist(_privateKeyFilePath)));
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::IsExist(_publicKeyFilePath)));
				//Empty input file
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler().Open(_inputFilePath, IO::FileHandler::AccessMode::Write)));
				Assert::IsTrue(ActionAsymmetricCipher().CipherFile(_publicKeyFilePath, _inputFilePath, _cipheredFilePath) == Return::InputFileEmpty);
				//Empty ciphered file
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler().Open(_cipheredFilePath, IO::FileHandler::AccessMode::Write)));
				Assert::IsTrue(ActionAsymmetricCipher().UncipherFile(_privateKeyFilePath, _cipheredFilePath, _uncipheredFilePath) == Return::CipheredFileEmpty);
				//Cleaning up
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(_privateKeyFilePath)));
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(_publicKeyFilePath)));
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(_inputFilePath)));
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(_cipheredFilePath)));
			}
		}
	};
}
