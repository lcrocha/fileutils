#include "stdafx.h"
#include "../FileUtilsEngine/FileUtilsEngine.h"
#include "../../libs/Helper/Utils/RandomNumberGenerator.h"

BOOL APIENTRY DllMain(HMODULE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved)
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		FileUtilsEngine::InitializeConfig(hModule);
		break;
	case DLL_PROCESS_DETACH:
		FileUtilsEngine::FinalizeConfig();
		break;
	}

	return TRUE;
}

using namespace FileUtilsEngine;

namespace FileUtilsEngineUnitTests
{
	TEST_CLASS(FileUtilsEngineUnitTests)
	{
	private:
		const ushort _maxTests = 5;
		const ulong _maxSize = 4096ul;
		const Crypto::HasherAlgorithm _hashAlg = Crypto::HasherAlgorithm::SHA1;
		const std::string _inputFilePath = ".\\_input.bin";
		const std::string _outputFilePath = ".\\_output.bin";
		const std::string _tempFilePath = ".\\_temp.bin";
		Utils::RandomNumberGenerator _rng;
	public:
		FileUtilsEngineUnitTests()
		{
		}

		TEST_METHOD(DataReaderWriterBuffer)
		{
			for (ushort t = 0; t < _maxTests; t++) for (auto& bt : Helper::BufferTypesList)
			{
				auto dataSize = _rng.GetUniformDistribution(1ul, _maxSize);
				auto pageSize = _rng.GetUniformDistribution(1ul, 64ul);
				//Creating data
				Buffer in, ou;
				Helper::GenerateBuffer(in, bt, dataSize);
				//Buffers envelop
				ReadDataBufferFunctor rb(in);
				WriteDataBufferFunctor wb(ou);
				//Creating reader and writer
				DataReader reader(rb);
				DataWriter writer(wb);
				//Brwosing by pageSize
				bool lastBlock = false;
				while (!lastBlock)
				{
					Buffer d(pageSize);
					Assert::IsTrue(ReturnTraits::IsSuccess(reader.Read(d, lastBlock)));
					Assert::IsTrue(ReturnTraits::IsSuccess(writer.Write(d, lastBlock)));
				}
				//Checking results
				Assert::IsTrue(in == ou);
			}
		}

		TEST_METHOD(DataReaderWriterBufferBase64)
		{
			for (ushort t = 0; t < _maxTests; t++) for (auto& bt : Helper::BufferTypesList)
			{
				auto dataSize = _rng.GetUniformDistribution(1ul, _maxSize);
				auto pageSize = _rng.GetUniformDistribution(1ul, 64ul);
				//Creating data
				Buffer in, en, ou;
				Helper::GenerateBuffer(in, bt, dataSize);
				//Encoding
				{
					//Buffers envelop
					ReadDataBufferFunctor rin(in);
					WriteDataBufferFunctor wen(en);
					//Creting converter
					ConverterBase64Encoder encoder;
					//Creating reader and writer
					DataReader readerIn(rin);
					DataWriter writerEn(wen, { encoder });
					//Brwosing by pageSize
					bool lastBlock = false;
					while (!lastBlock)
					{
						Buffer d(pageSize);
						Assert::IsTrue(ReturnTraits::IsSuccess(readerIn.Read(d, lastBlock)));
						Assert::IsTrue(ReturnTraits::IsSuccess(writerEn.Write(d, lastBlock)));
					}
				}
				//Decoding
				{
					//Buffers envelop
					ReadDataBufferFunctor ren(en);
					WriteDataBufferFunctor wou(ou);
					//Creting converter
					ConverterBase64Decoder decoder;
					//Creating reader and writer
					DataReader readerEn(ren, { decoder });
					DataWriter writerOu(wou);
					//Brwosing by pageSize
					bool lastBlock = false;
					while (!lastBlock)
					{
						Buffer d(pageSize);
						Assert::IsTrue(ReturnTraits::IsSuccess(readerEn.Read(d, lastBlock)));
						Assert::IsTrue(ReturnTraits::IsSuccess(writerOu.Write(d, lastBlock)));
					}
				}
				//Checking results
				Assert::IsTrue(in == ou);
				Assert::IsTrue(in != en);
			}
		}

		TEST_METHOD(DataReaderWriterBufferZip)
		{
			for (ushort t = 0; t < _maxTests; t++) for (auto& bt : Helper::BufferTypesList)
			{
				auto dataSize = _rng.GetUniformDistribution(1ul, _maxSize);
				auto pageSize = _rng.GetUniformDistribution(1ul, 64ul);
				//Creating data
				Buffer in, en, ou;
				Helper::GenerateBuffer(in, bt, dataSize);
				//Encoding
				{
					//Buffers envelop
					ReadDataBufferFunctor rin(in);
					WriteDataBufferFunctor wen(en);
					//Creting converter
					ConverterDeflator deflator;
					//Creating reader and writer
					DataReader readerIn(rin);
					DataWriter writerEn(wen, { deflator });
					//Brwosing
					bool lastBlock = false;
					while (!lastBlock)
					{
						Buffer d(pageSize);
						Assert::IsTrue(ReturnTraits::IsSuccess(readerIn.Read(d, lastBlock)));
						Assert::IsTrue(ReturnTraits::IsSuccess(writerEn.Write(d, lastBlock)));
					}
				}
				//Decoding
				{
					//Buffers envelop
					ReadDataBufferFunctor ren(en);
					WriteDataBufferFunctor wou(ou);
					//Creting converter
					ConverterInflator inflator;
					//Creating reader and writer
					DataReader readerEn(ren, { inflator });
					DataWriter writerOu(wou);
					//Brwosing by pageSize
					bool lastBlock = false;
					while (!lastBlock)
					{
						Buffer d(pageSize);
						Assert::IsTrue(ReturnTraits::IsSuccess(readerEn.Read(d, lastBlock)));
						Assert::IsTrue(ReturnTraits::IsSuccess(writerOu.Write(d, lastBlock)));
					}
				}
				//Checking results
				Assert::IsTrue(in == ou);
				Assert::IsTrue(in != en);
			}
		}

		TEST_METHOD(DataReaderWriterBufferHash)
		{
			auto hashAlg = Crypto::HasherAlgorithm::SHA1;
			for (ushort t = 0; t < _maxTests; t++) for (auto& bt : Helper::BufferTypesList)
			{
				auto dataSize = _rng.GetUniformDistribution(1ul, _maxSize);
				auto pageSize = _rng.GetUniformDistribution(1ul, 64ul);
				//Creating data
				Buffer in, en, ou;
				Helper::GenerateBuffer(in, bt, dataSize);
				//Encoding
				{
					//Buffers envelop
					ReadDataBufferFunctor rin(in);
					WriteDataBufferFunctor wen(en);
					//Creting converter
					ConverterHashGenerator generator;
					//Creating reader and writer
					DataReader readerIn(rin);
					DataWriter writerEn(wen, { generator });
					//Brwosing
					bool lastBlock = false;
					while (!lastBlock)
					{
						Buffer d(pageSize);
						Assert::IsTrue(ReturnTraits::IsSuccess(readerIn.Read(d, lastBlock)));
						Assert::IsTrue(ReturnTraits::IsSuccess(writerEn.Write(d, lastBlock)));
					}
				}
				//Decoding
				{
					//Buffers envelop
					ReadDataBufferFunctor ren(en);
					WriteDataBufferFunctor wou(ou);
					//Creting converter
					ConverterHashChecker checker;
					//Creating reader and writer
					DataReader readerEn(ren, { checker });
					DataWriter writerOu(wou);
					//Brwosing by pageSize
					bool lastBlock = false;
					while (!lastBlock)
					{
						Buffer d(pageSize);
						Assert::IsTrue(ReturnTraits::IsSuccess(readerEn.Read(d, lastBlock)));
						Assert::IsTrue(ReturnTraits::IsSuccess(writerOu.Write(d, lastBlock)));
					}
				}
				//Checking results
				Assert::IsTrue(in == ou);
				Assert::IsTrue(in != en);
			}
		}

		TEST_METHOD(DataReaderWriterBadBufferHash)
		{
			auto hashAlg = Crypto::HasherAlgorithm::SHA1;
			for (ushort t = 0; t < _maxTests; t++) for (auto& bt : Helper::BufferTypesList)
			{
				auto dataSize = _rng.GetUniformDistribution(1ul, _maxSize);
				auto pageSize = _rng.GetUniformDistribution(1ul, 64ul);
				//Creating data
				Buffer in, en, ou;
				Helper::GenerateBuffer(in, bt, dataSize);
				//Encoding
				{
					//Buffers envelop
					ReadDataBufferFunctor rin(in);
					WriteDataBufferFunctor wen(en);
					//Creting converter
					ConverterHashGenerator generator;
					//Creating reader and writer
					DataReader readerIn(rin);
					DataWriter writerEn(wen, { generator });
					//Brwosing
					bool lastBlock = false;
					while (!lastBlock)
					{
						Buffer d(pageSize);
						Assert::IsTrue(ReturnTraits::IsSuccess(readerIn.Read(d, lastBlock)));
						Assert::IsTrue(ReturnTraits::IsSuccess(writerEn.Write(d, lastBlock)));
					}
				}
				//Damaging buffer
				en[0]++;
				//Decoding
				{
					//Buffers envelop
					ReadDataBufferFunctor ren(en);
					WriteDataBufferFunctor wou(ou);
					//Creting converter
					ConverterHashChecker checker;
					//Creating reader and writer
					DataReader readerEn(ren, { checker });
					DataWriter writerOu(wou);
					//Brwosing by pageSize
					bool lastBlock = false;
					while (!lastBlock)
					{
						Buffer d(pageSize);
						if (ReturnTraits::IsFail(readerEn.Read(d, lastBlock)))
							break;
						Assert::IsTrue(ReturnTraits::IsSuccess(writerOu.Write(d, lastBlock)));
					}
				}
				//Checking results
				Assert::IsTrue(in != ou);
				Assert::IsTrue(in != en);
			}
		}

		TEST_METHOD(DataReaderWriterBufferMixed)
		{
			auto hashAlg = Crypto::HasherAlgorithm::SHA1;
			for (ushort t = 0; t < _maxTests; t++) for (auto& bt : Helper::BufferTypesList)
			{
				auto dataSize = _rng.GetUniformDistribution(1ul, _maxSize);
				auto pageSize = _rng.GetUniformDistribution(1ul, 64ul);
				//Creating data
				Buffer in, en, ou;
				Helper::GenerateBuffer(in, bt, dataSize);
				//Encoding
				{
					//Buffers envelop
					ReadDataBufferFunctor rin(in);
					WriteDataBufferFunctor wen(en);
					//Creting converter
					ConverterDeflator deflator;
					ConverterHashGenerator generator;
					ConverterBase64Encoder encoder;
					//Creating reader and writer
					DataReader readerIn(rin);
					DataWriter writerEn(wen, { deflator, generator, encoder });
					//Brwosing
					bool lastBlock = false;
					while (!lastBlock)
					{
						Buffer d(pageSize);
						Assert::IsTrue(ReturnTraits::IsSuccess(readerIn.Read(d, lastBlock)));
						Assert::IsTrue(ReturnTraits::IsSuccess(writerEn.Write(d, lastBlock)));
					}
				}
				//Decoding
				{
					//Buffers envelop
					ReadDataBufferFunctor ren(en);
					WriteDataBufferFunctor wou(ou);
					//Creting converter
					ConverterBase64Decoder decoder;
					ConverterHashChecker checker;
					ConverterInflator inflator;
					//Creating reader and writer
					DataReader readerEn(ren, { decoder, checker, inflator });
					DataWriter writerOu(wou);
					//Brwosing by pageSize
					bool lastBlock = false;
					while (!lastBlock)
					{
						Buffer d(pageSize);
						Assert::IsTrue(ReturnTraits::IsSuccess(readerEn.Read(d, lastBlock)));
						Assert::IsTrue(ReturnTraits::IsSuccess(writerOu.Write(d, lastBlock)));
					}
				}
				//Checking results
				Assert::IsTrue(in == ou);
				Assert::IsTrue(in != en);
			}
		}
	};
}