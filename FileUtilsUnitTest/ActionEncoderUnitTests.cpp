#pragma once
#include "stdafx.h"
#include "../FileUtilsEngine/ActionEncoder.h"
#include "../../Libs/Helper/IO/FileHandler.h"
using namespace FileUtilsEngine;

namespace FileUtilsEngineUnitTests
{
	TEST_CLASS(ActionEncoderUnitTests)
	{
	private:
		std::vector<Crypto::Encoding> _encodings;
	public:
		ActionEncoderUnitTests()
		{
			_encodings =
			{
				Crypto::Encoding::Bin,
				Crypto::Encoding::Base64,
				Crypto::Encoding::Hex
			};
		}

		TEST_METHOD(EncodingAndDecodingRandomData)
		{
			::srand(::GetTickCount());
			for (ulong i = 0; i < 10; ++i)
			{
				auto size = (ulong)(1 + ::rand() % 1024ul);
				Buffer data(size);
				Assert::IsTrue(Crypto::ReturnTraits::IsSuccess(Crypto::GenRandom(data)));
				for (auto inputEncoding : _encodings)
				{
					Buffer input;
					if (inputEncoding == Crypto::Encoding::Bin)
					{
						input = data;
					}
					else if (inputEncoding == Crypto::Encoding::Base64)
					{
						std::string tmp;
						Assert::IsTrue(Crypto::ReturnTraits::IsSuccess(Crypto::Base64Encode(data, tmp)));
						input.assign(tmp.begin(), tmp.end());
					}
					else if (inputEncoding == Crypto::Encoding::Hex)
					{
						std::string tmp;
						Assert::IsTrue(Crypto::ReturnTraits::IsSuccess(Crypto::HexEncode(data, tmp)));
						input.assign(tmp.begin(), tmp.end());
					}
					//Browse all encodings for output
					for (auto outputEncoding : _encodings)
					{
						Buffer output;
						Assert::IsTrue(ReturnTraits::IsSuccess(ActionEncoder().ConvertData(input, inputEncoding, output, outputEncoding)));
						Buffer outputDeconverted;
						Assert::IsTrue(ReturnTraits::IsSuccess(ActionEncoder().ConvertData(output, outputEncoding, outputDeconverted, inputEncoding)));
						Assert::IsTrue(input == outputDeconverted);
					}
				}
			}
		}

		TEST_METHOD(GenerateRandomDataOnFile)
		{
			::srand(::GetTickCount());
			const char* file = "file.bin";
			for (auto i = 0; i < 10; ++i) for (auto& bt : Helper::BufferTypesList)
			{
				auto size = (ulong)(1 + ::rand() % 1024);
				Assert::IsTrue(ReturnTraits::IsSuccess(ActionEncoder().GenerateFileRandomData(bt, file, size)));
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::IsExist(file)));
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(file)));
			}
		}

		TEST_METHOD(EmptyParameters)
		{
			//GenerateFileRandomData
			Assert::IsTrue(ActionEncoder().GenerateFileRandomData(Helper::BufferType(), std::string(), 0L) == Return::InputFilePathEmpty);
			Assert::IsTrue(ActionEncoder().GenerateFileRandomData(Helper::BufferType(), "_", 0L) == Return::SizeEmpty);
			//ConvertData
			Assert::IsTrue(ActionEncoder().ConvertData(Buffer(), Crypto::Encoding(), Buffer(), Crypto::Encoding()) == Return::InputFilePathEmpty);
			//ConvertFile
			Assert::IsTrue(ActionEncoder().ConvertFile(std::string(), Crypto::Encoding(), std::string(), Crypto::Encoding()) == Return::InputFilePathEmpty);
			Assert::IsTrue(ActionEncoder().ConvertFile("_", Crypto::Encoding(), std::string(), Crypto::Encoding()) == Return::EncodedFilePathEmpty);
		}

		TEST_METHOD(FileEmpty)
		{
			IO::FileHandler::Delete("_1");
			IO::FileHandler::Delete("_2");
			IO::FileHandler f1;
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(f1.Open("_1", IO::FileHandler::AccessMode::Write)));
			Assert::IsTrue(ActionEncoder().ConvertFile("_1", Crypto::Encoding(), "_2", Crypto::Encoding()) == Return::InputFileEmpty);
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(f1.Delete()));
		}

		TEST_METHOD(FileNotFound)
		{
			IO::FileHandler::Delete("_1");
			IO::FileHandler::Delete("_2");
			Assert::IsTrue(ActionEncoder().ConvertFile("_1", Crypto::Encoding(), "_2", Crypto::Encoding()) == Return::InputFileNotFound);
		}

		TEST_METHOD(EncodingDecodingFile)
		{
			::srand(::GetTickCount());
			const char* inputFilePath = "_1";
			const char* inputEncodedFilePath = "_2";
			const char* encodedFilePath = "_3";
			const char* decodedFilePath = "_4";
			auto hashAlg = Crypto::HasherAlgorithm::SHA384;
			//Cleaning up
			IO::FileHandler::Delete(encodedFilePath);
			IO::FileHandler::Delete(decodedFilePath);
			IO::FileHandler::Delete(inputFilePath);
			IO::FileHandler::Delete(inputEncodedFilePath);
			//browsing encodings
			for (auto inputEncoding : _encodings) for (auto outputEncoding : _encodings) for (auto& bt : Helper::BufferTypesList)
			{
				//Creating input file
				Assert::IsTrue(ReturnTraits::IsSuccess(ActionEncoder().GenerateFileRandomData(bt, inputFilePath, (1 + ::rand() % 999))));
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::IsExist(inputFilePath)));
				//Encoding input
				Assert::IsTrue(ReturnTraits::IsSuccess(ActionEncoder().ConvertFile(inputFilePath, Crypto::Encoding::Bin, inputEncodedFilePath, inputEncoding)));
				//Getting hash from input file
				Buffer inputFileHash;
				Assert::IsTrue(ReturnTraits::IsSuccess(CalculateHashFromFile(inputEncodedFilePath, hashAlg, inputFileHash)));
				//Encoding
				Assert::IsTrue(ReturnTraits::IsSuccess(ActionEncoder().ConvertFile(inputEncodedFilePath, inputEncoding, encodedFilePath, outputEncoding)));
				//Decoding
				Assert::IsTrue(ReturnTraits::IsSuccess(ActionEncoder().ConvertFile(encodedFilePath, outputEncoding, decodedFilePath, inputEncoding)));
				//Getting hash from decoded file
				Buffer decodedFileHash;
				Assert::IsTrue(ReturnTraits::IsSuccess(CalculateHashFromFile(decodedFilePath, hashAlg, decodedFileHash)));
				//Checking results
				Assert::IsTrue(inputFileHash == decodedFileHash);
				//Cleanup
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(inputFilePath)));
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(inputEncodedFilePath)));
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(encodedFilePath)));
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(decodedFilePath)));
			}
		}

		/*TEST_METHOD(EncodingDecodingFileWithBadParameters)
		{
			::srand(::GetTickCount());
			const char* binFilePath = "_1";
			const char* hexFilePath = "_2";
			const char* base64FilePath = "_3";
			const char* encodedFilePath = "_4";
			auto hashAlg = Crypto::HasherAlgorithm::SHA384;
			//Cleaning up
			IO::FileHandler::Delete(binFilePath);
			IO::FileHandler::Delete(hexFilePath);
			IO::FileHandler::Delete(base64FilePath);
			IO::FileHandler::Delete(encodedFilePath);
			//Creating hex file
			Assert::IsTrue(ReturnTraits::IsSuccess(ActionEncoder().GenerateFileRandomData(binFilePath, (1 + ::rand() % 999))));
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::IsExist(binFilePath)));
			Assert::IsTrue(ReturnTraits::IsSuccess(ActionEncoder().ConvertFile(binFilePath, Crypto::Encoding::Bin, hexFilePath, Crypto::Encoding::Hex)));
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::IsExist(hexFilePath)));
			//Creating base64 file
			Assert::IsTrue(ReturnTraits::IsSuccess(ActionEncoder().GenerateFileRandomData(binFilePath, (1 + ::rand() % 999))));
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::IsExist(binFilePath)));
			Assert::IsTrue(ReturnTraits::IsSuccess(ActionEncoder().ConvertFile(binFilePath, Crypto::Encoding::Bin, base64FilePath, Crypto::Encoding::Base64)));
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::IsExist(base64FilePath)));
			//Creating bin file
			Assert::IsTrue(ReturnTraits::IsSuccess(ActionEncoder().GenerateFileRandomData(binFilePath, (1 + ::rand() % 999))));
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::IsExist(binFilePath)));
			//Forcing hex error
			Assert::IsTrue(ActionEncoder().ConvertFile(binFilePath, Crypto::Encoding::Base64, encodedFilePath, Crypto::Encoding::Bin) == Return::Base64DecodeError);
			//Cleaning up
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(binFilePath)));
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(hexFilePath)));
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(base64FilePath)));
			//Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(encodedFilePath)));
		}*/

		/*TEST_METHOD(LockedFile)
		{
			IO::FileHandler lockedFile;
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(lockedFile.Open("_", IO::FileHandler::AccessMode::Write)));
			Assert::IsTrue(ReturnTraits::IsSuccess(ActionEncoder().GenerateFileRandomData("_", 512L)));
			Assert::IsTrue(IO::FileHandler::Delete("_") == IO::Return::RemoveError);
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(lockedFile.Delete()));
		}*/
	};
}