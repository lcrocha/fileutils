#pragma once
#include "stdafx.h"
#include "../FileUtilsEngine/ActionSigner.h"
#include "../FileUtilsEngine/FileUtilsEngine.h"
using namespace FileUtilsEngine;

namespace FileUtilsEngineUnitTests
{
	TEST_CLASS(ActionSignerUnitTests)
	{
	public:
		TEST_METHOD(EmptyParameters)
		{
			//GenerateKeyPair
			Assert::IsTrue(ActionSigner().GenerateKeyPair(std::string(), std::string()) == Return::PrivateKeyFilePathEmpty);
			Assert::IsTrue(ActionSigner().GenerateKeyPair("_1", std::string()) == Return::PublicKeyFilePathEmpty);
			//SignFile
			Assert::IsTrue(ActionSigner().SignFile(std::string(), std::string(), std::string()) == Return::PrivateKeyFilePathEmpty);
			Assert::IsTrue(ActionSigner().SignFile("_1", std::string(), std::string()) == Return::InputFilePathEmpty);
			Assert::IsTrue(ActionSigner().SignFile("_1", "_2", std::string()) == Return::SignatureFilePathEmpty);
			//VerifySignature
			Assert::IsTrue(ActionSigner().VerifySignature(std::string(), std::string(), std::string()) == Return::PublicKeyFilePathEmpty);
			Assert::IsTrue(ActionSigner().VerifySignature("_1", std::string(), std::string()) == Return::InputFilePathEmpty);
			Assert::IsTrue(ActionSigner().VerifySignature("_1", "_2", std::string()) == Return::SignatureFilePathEmpty);
		}

		TEST_METHOD(FileEmpty)
		{
			const char* privateKeyFilePath = "_0";
			const char* publicKeyFilePath = "_1";
			const char* inputFilePath = "_2";
			const char* signatureFilePath = "_3";
			//Cleaning up
			IO::FileHandler::Delete(privateKeyFilePath);
			IO::FileHandler::Delete(publicKeyFilePath);
			IO::FileHandler::Delete(inputFilePath);
			IO::FileHandler::Delete(signatureFilePath);
			//Empty privateKey
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler().Open(privateKeyFilePath, IO::FileHandler::AccessMode::Write)));
			Assert::IsTrue(ActionSigner().SignFile(privateKeyFilePath, inputFilePath, signatureFilePath) == Return::PrivateKeyFileEmpty);
			//Empty publicKey
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler().Open(publicKeyFilePath, IO::FileHandler::AccessMode::Write)));
			Assert::IsTrue(ActionSigner().VerifySignature(publicKeyFilePath, inputFilePath, signatureFilePath) == Return::PublicKeyFileEmpty);
			//Updating publicKey and privateKey
			Assert::IsTrue(ReturnTraits::IsSuccess(ActionSigner().GenerateKeyPair(privateKeyFilePath, publicKeyFilePath)));
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::IsExist(privateKeyFilePath)));
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::IsExist(publicKeyFilePath)));
			//Empty input file
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler().Open(inputFilePath, IO::FileHandler::AccessMode::Write)));
			Assert::IsTrue(ActionSigner().SignFile(privateKeyFilePath, inputFilePath, signatureFilePath) == Return::InputFileEmpty);
			//Updating input file
			Assert::IsTrue(Utils::CreateFileData(Helper::BufferType::BinaryRandom, inputFilePath, 1024ul));
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::IsExist(inputFilePath)));
			//Empty signature file
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler().Open(signatureFilePath, IO::FileHandler::AccessMode::Write)));
			Assert::IsTrue(ActionSigner().VerifySignature(publicKeyFilePath, inputFilePath, signatureFilePath) == Return::SignatureFileEmpty);
			//Cleaning up
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(privateKeyFilePath)));
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(publicKeyFilePath)));
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(inputFilePath)));
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(signatureFilePath)));
		}

		TEST_METHOD(FileNotFound)
		{
			const char* privateKeyFilePath = "_0";
			const char* publicKeyFilePath = "_1";
			const char* inputFilePath = "_2";
			const char* signatureFilePath = "_3";
			//Cleaning up
			IO::FileHandler::Delete(privateKeyFilePath);
			IO::FileHandler::Delete(publicKeyFilePath);
			IO::FileHandler::Delete(inputFilePath);
			IO::FileHandler::Delete(signatureFilePath);
			//Empty privateKey
			Assert::IsTrue(ActionSigner().SignFile(privateKeyFilePath, inputFilePath, signatureFilePath) == Return::PrivateKeyFileNotFound);
			//Empty publicKey
			Assert::IsTrue(ActionSigner().VerifySignature(publicKeyFilePath, inputFilePath, signatureFilePath) == Return::PublicKeyFileNotFound);
			//Updating publicKey and privateKey
			Assert::IsTrue(ReturnTraits::IsSuccess(ActionSigner().GenerateKeyPair(privateKeyFilePath, publicKeyFilePath)));
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::IsExist(privateKeyFilePath)));
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::IsExist(publicKeyFilePath)));
			//Empty input file
			Assert::IsTrue(ActionSigner().SignFile(privateKeyFilePath, inputFilePath, signatureFilePath) == Return::InputFileNotFound);
			//Updating input file
			Assert::IsTrue(Utils::CreateFileData(Helper::BufferType::BinaryRandom, inputFilePath, 1024ul));
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::IsExist(inputFilePath)));
			//Signature file not found
			Assert::IsTrue(ActionSigner().VerifySignature(publicKeyFilePath, inputFilePath, signatureFilePath) == Return::SignatureFileNotFound);
			//Cleaning up
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(privateKeyFilePath)));
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(publicKeyFilePath)));
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(inputFilePath)));
		}

		TEST_METHOD(SignVerifyRandomFileWithGoodKeys)
		{
			const char* privateKeyFilePath = "_0";
			const char* publicKeyFilePath = "_1";
			const char* inputFilePath = "_2";
			const char* signatureFilePath = "_3";
			//Cleaning up
			IO::FileHandler::Delete(privateKeyFilePath);
			IO::FileHandler::Delete(publicKeyFilePath);
			IO::FileHandler::Delete(inputFilePath);
			IO::FileHandler::Delete(signatureFilePath);
			//Creating input file
			Assert::IsTrue(Utils::CreateFileData(Helper::BufferType::BinaryRandom, inputFilePath, 1024ul));
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::IsExist(inputFilePath)));
			//Updating publicKey and privateKey
			Assert::IsTrue(ReturnTraits::IsSuccess(ActionSigner().GenerateKeyPair(privateKeyFilePath, publicKeyFilePath)));
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::IsExist(privateKeyFilePath)));
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::IsExist(publicKeyFilePath)));
			//Signing input file
			Assert::IsTrue(ReturnTraits::IsSuccess(ActionSigner().SignFile(privateKeyFilePath, inputFilePath, signatureFilePath)));
			//Verifying signature
			Assert::IsTrue(ReturnTraits::IsSuccess(ActionSigner().VerifySignature(publicKeyFilePath, inputFilePath, signatureFilePath)));
			//Cleaning up
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(privateKeyFilePath)));
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(publicKeyFilePath)));
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(inputFilePath)));
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(signatureFilePath)));
		}

		TEST_METHOD(SignVerifyRandomFileWithBadKeys)
		{
			const char* privateKeyFilePath = "_0";
			const char* publicKeyFilePath = "_1";
			const char* inputFilePath = "_2";
			const char* signatureFilePath = "_3";
			//Cleaning up
			IO::FileHandler::Delete(privateKeyFilePath);
			IO::FileHandler::Delete(publicKeyFilePath);
			IO::FileHandler::Delete(inputFilePath);
			IO::FileHandler::Delete(signatureFilePath);
			//Creating input file
			Assert::IsTrue(Utils::CreateFileData(Helper::BufferType::BinaryRandom, inputFilePath, 1024ul));
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::IsExist(inputFilePath)));
			//Creating privateKey file
			Assert::IsTrue(Utils::CreateFileData(Helper::BufferType::BinaryRandom, privateKeyFilePath, 1024ul));
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::IsExist(privateKeyFilePath)));
			//Signing input file
			Assert::IsTrue(ActionSigner().SignFile(privateKeyFilePath, inputFilePath, signatureFilePath) == Return::SignerSignError);
			//Creating publicKey file
			Assert::IsTrue(Utils::CreateFileData(Helper::BufferType::BinaryRandom, publicKeyFilePath, 1024ul));
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::IsExist(publicKeyFilePath)));
			//Creating Signature file
			Assert::IsTrue(Utils::CreateFileData(Helper::BufferType::BinaryRandom, signatureFilePath, 64L));
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::IsExist(signatureFilePath)));
			//Verifying signature
			Assert::IsTrue(ActionSigner().VerifySignature(privateKeyFilePath, inputFilePath, signatureFilePath) == Return::SignerVerifyError);
			//Cleaning up
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(privateKeyFilePath)));
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(publicKeyFilePath)));
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(inputFilePath)));
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(signatureFilePath)));
		}

		TEST_METHOD(VerifyFileWithBadSignature)
		{
			const char* privateKeyFilePath = "_0";
			const char* publicKeyFilePath = "_1";
			const char* inputFilePath = "_2";
			const char* signatureFilePath = "_3";
			//Cleaning up
			IO::FileHandler::Delete(privateKeyFilePath);
			IO::FileHandler::Delete(publicKeyFilePath);
			IO::FileHandler::Delete(inputFilePath);
			IO::FileHandler::Delete(signatureFilePath);
			//Creating input file
			Assert::IsTrue(Utils::CreateFileData(Helper::BufferType::BinaryRandom, inputFilePath, 1024ul));
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::IsExist(inputFilePath)));
			//Updating publicKey and privateKey
			Assert::IsTrue(ReturnTraits::IsSuccess(ActionSigner().GenerateKeyPair(privateKeyFilePath, publicKeyFilePath)));
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::IsExist(privateKeyFilePath)));
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::IsExist(publicKeyFilePath)));
			//Signing input file
			Assert::IsTrue(ReturnTraits::IsSuccess(ActionSigner().SignFile(privateKeyFilePath, inputFilePath, signatureFilePath)));
			//Creating Signature file
			Assert::IsTrue(Utils::CreateFileData(Helper::BufferType::BinaryRandom, signatureFilePath, 64L));
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::IsExist(signatureFilePath)));
			//Verifying signature
			Assert::IsTrue(ActionSigner().VerifySignature(privateKeyFilePath, inputFilePath, signatureFilePath) == Return::SignerVerifyError);
			//Cleaning up
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(privateKeyFilePath)));
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(publicKeyFilePath)));
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(inputFilePath)));
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(signatureFilePath)));
		}

	};
}