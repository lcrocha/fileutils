#pragma once
#include "stdafx.h"
#include "../FileUtilsEngine/ActionHasher.h"
using namespace FileUtilsEngine;

namespace FileUtilsEngineUnitTests
{
	TEST_CLASS(ActionHasherUnitTests)
	{
	private:
		struct HashData
		{
			Crypto::HasherAlgorithm alg;
			std::string hex;
		};
		std::vector<Crypto::Encoding> _encodings;
		std::vector<Crypto::HasherAlgorithm> _hashAlgs;
		const char* _message = "Ora (direis) ouvir estrelas!";
		std::vector<HashData> _hashDatas;
	public:
		ActionHasherUnitTests()
		{
			_encodings =
			{
				Crypto::Encoding::Bin,
				Crypto::Encoding::Base64,
				Crypto::Encoding::Hex
			};
			_hashAlgs =
			{
				Crypto::HasherAlgorithm::MD2,
				Crypto::HasherAlgorithm::MD4,
				Crypto::HasherAlgorithm::MD5,
				Crypto::HasherAlgorithm::SHA1,
				Crypto::HasherAlgorithm::SHA256,
				Crypto::HasherAlgorithm::SHA384,
				Crypto::HasherAlgorithm::SHA512
			};
			_hashDatas =
			{
				{ Crypto::HasherAlgorithm::MD2, "00b89b95ce8e55f249304a95ccaaaf24" },
				{ Crypto::HasherAlgorithm::MD4, "99b3ecbee502f4505bc3caf3bdc19788" },
				{ Crypto::HasherAlgorithm::MD5, "74892ef455f89f038a1bd86096d73727" },
				{ Crypto::HasherAlgorithm::SHA1, "8da8ddbb9fa8216359213f510de217b8a6428820" },
				{ Crypto::HasherAlgorithm::SHA256, "07bc34da4b0deea4db32cf349c4d1e930f1b040d8b35a636f8dbb6c667b1a25d" },
				{ Crypto::HasherAlgorithm::SHA384, "9d2bf39a166ce995f8032aa9770590c3f5c92016aa6eb1bd668181dc88661c779fb9e9433d04d94c4068188a499733a8" },
				{ Crypto::HasherAlgorithm::SHA512, "05e325b9823ce8949609cc236c334775a3b45bec222b8386ca6ca225e8b855090f8b5271d6120fa79aa628abd3341fe0c34a16cbb1bbc8efe717b5744fcc4fe0" },
			};
		}

		TEST_METHOD(HashingRandomContentFile)
		{
			const char* fileReadPath = "file.bin";
			const char* fileWritePath = "file.bin.hash";
			//Browsing algorithms
			for (auto hashAlg : _hashAlgs)
			{
				for (auto encoding : _encodings)
				{
					//Hashing
					Assert::IsTrue(Utils::CreateFileData(Helper::BufferType::BinaryRandom, fileReadPath, 8 * 1024));
					Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::IsExist(fileReadPath)));
					Assert::IsTrue(ReturnTraits::IsSuccess(ActionHasher().CreateHashFromFile(hashAlg, fileReadPath, fileWritePath, encoding)));
					Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::IsExist(fileWritePath)));
					Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(fileWritePath)));
					Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(fileReadPath)));
				}
			}
		}

		TEST_METHOD(HashingFixedContentFile)
		{
			const char* fileReadPath = "file.bin";
			const char* fileWritePath = "file.bin.hash";
			//Setting a specific data
			Buffer data(_message, _message + ::strlen(_message));
			//Creating read file
			Assert::IsTrue(Utils::CreateAFileAndWriteData(fileReadPath, data));
			//Looping hashDatas
			for (auto hashData : _hashDatas)
			{
				//Hashing
				Assert::IsTrue(ReturnTraits::IsSuccess(ActionHasher().CreateHashFromFile(hashData.alg, fileReadPath, fileWritePath, Crypto::Encoding::Bin)));
				//Opening output file
				IO::FileHandler fileHash;
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(fileHash.Open(fileWritePath, IO::FileHandler::AccessMode::Read)));
				//reading contents
				Buffer hash;
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(fileHash.ReadData(1024, hash)));
				//Encoding
				std::string hex;
				Assert::IsTrue(Crypto::ReturnTraits::IsSuccess(Crypto::HexEncode(hash, hex)));
				//Checking results
				Assert::IsTrue(hashData.hex == hex);
				//Deleting file
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(fileHash.Delete()));
			}
			//Checking results
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::IsExist(fileReadPath)));
			//Cleaning
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(fileReadPath)));
		}

		TEST_METHOD(SeveralHashAlgsWithFixedFile)
		{
			const char* fileReadPath = "file.bin";
			const char* fileWritePath = "file.bin.hash";
			//Creating file
			Assert::IsTrue(Utils::CreateAFileAndWriteData(fileReadPath, Buffer(_message, _message + ::strlen(_message))));
			//Looping hashDatas
			for (auto hashData : _hashDatas)
			{
				for (auto encoding : _encodings)
				{
					IO::FileHandler f;
					//Creating file to write
					Assert::IsTrue(IO::ReturnTraits::IsSuccess(f.Open(fileWritePath, IO::FileHandler::AccessMode::Write)));
					//Encoding if necessary
					Buffer encoded(hashData.hex.begin(), hashData.hex.end());
					if (encoding == Crypto::Encoding::Bin)
					{
						encoded.clear();
						Assert::IsTrue(Crypto::ReturnTraits::IsSuccess(Crypto::HexDecode(hashData.hex, encoded)));
					}
					else if (encoding == Crypto::Encoding::Base64)
					{
						Buffer bin;
						Assert::IsTrue(Crypto::ReturnTraits::IsSuccess(Crypto::HexDecode(hashData.hex, bin)));
						std::string tmp;
						Assert::IsTrue(Crypto::ReturnTraits::IsSuccess(Crypto::Base64Encode(bin, tmp)));
						encoded.assign(tmp.begin(), tmp.end());
					}
					//Saving data
					Assert::IsTrue(IO::ReturnTraits::IsSuccess(f.WriteData(1024, encoded)));
					//Flushing
					Assert::IsTrue(IO::ReturnTraits::IsSuccess(f.Flush()));
					//Checking now
					Assert::IsTrue(ReturnTraits::IsSuccess(ActionHasher().CheckHashFromFile(hashData.alg, fileReadPath, fileWritePath, encoding)));
					//Deleting file
					Assert::IsTrue(IO::ReturnTraits::IsSuccess(f.Delete()));
				}
			}
			//Checking results
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::IsExist(fileReadPath)));
			//Cleaning
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(fileReadPath)));
		}

		TEST_METHOD(IntegratingAllFunctionalities)
		{
			::srand(::GetTickCount());
			//Creating test batch
			std::vector<ulong> sizes;
			for (auto e = 0; e < 1; ++e)
			{
				auto exp = (ulong)::powf(1024.0f, (float)e);
				for (auto c = 0; c < 10; ++c)
				{
					auto size = (ulong)(1 + ::rand() % 998) * exp;
					sizes.push_back(size);
				}
			}
			Assert::IsTrue(sizes.size() > 0);
			//Querying algs
			Crypto::HasherInfos infos;
			Crypto::QueryHasherInfos(infos);
			Assert::IsTrue(infos.size() > 0);
			//Browsing sizes
			const char* fileReadPath = "file.bin";
			const char* fileWritePath = "file.bin.hash";
			for (auto size : sizes) for (auto data : infos) for (auto encoding : _encodings)
			{
				//Creating file
				IO::FileHandler f;
				//Creating file to write
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(f.Open(fileReadPath, IO::FileHandler::AccessMode::Write)));
				//Encoding if necessary
				Buffer hashData(size);
				Crypto::GenRandom(hashData);
				//Encoding
				Buffer encoded;
				if (encoding == Crypto::Encoding::Bin)
				{
					encoded = hashData;
				}
				else if (encoding == Crypto::Encoding::Base64)
				{
					std::string tmp;
					Assert::IsTrue(Crypto::ReturnTraits::IsSuccess(Crypto::Base64Encode(hashData, tmp)));
					encoded.assign(tmp.begin(), tmp.end());
				}
				else if (encoding == Crypto::Encoding::Hex)
				{
					std::string tmp;
					Assert::IsTrue(Crypto::ReturnTraits::IsSuccess(Crypto::HexEncode(hashData, tmp)));
					encoded.assign(tmp.begin(), tmp.end());
				}
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(f.WriteData(1024, encoded)));
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(f.Flush()));
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(f.Close()));
				//Testing
				Assert::IsTrue(ReturnTraits::IsSuccess(ActionHasher().CreateHashFromFile(data.alg, fileReadPath, fileWritePath, encoding)));
				Assert::IsTrue(ReturnTraits::IsSuccess(ActionHasher().CheckHashFromFile(data.alg, fileReadPath, fileWritePath, encoding)));
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(fileReadPath)));
				Assert::IsTrue(IO::ReturnTraits::IsSuccess(IO::FileHandler::Delete(fileWritePath)));
			}
		}

		TEST_METHOD(QueryHasherInfoTest)
		{
			Crypto::HasherInfos infos;
			Crypto::QueryHasherInfos(infos);
			//Checking results
			Assert::IsTrue(infos.size() > 0);
		}

		TEST_METHOD(EmptyParameters)
		{
			//CheckHashFromFile
			Assert::IsTrue(ActionHasher().CheckHashFromFile(Crypto::HasherAlgorithm(), std::string(), std::string(), Crypto::Encoding()) == Return::InputFilePathEmpty);
			Assert::IsTrue(ActionHasher().CheckHashFromFile(Crypto::HasherAlgorithm(), "_", std::string(), Crypto::Encoding()) == Return::HashedFilePathEmpty);
			//CreateHashFromFile
			Assert::IsTrue(ActionHasher().CreateHashFromFile(Crypto::HasherAlgorithm(), std::string(), std::string(), Crypto::Encoding()) == Return::InputFilePathEmpty);
			Assert::IsTrue(ActionHasher().CreateHashFromFile(Crypto::HasherAlgorithm(), "_", std::string(), Crypto::Encoding()) == Return::HashedFilePathEmpty);
		}

		TEST_METHOD(FileEmpty)
		{
			IO::FileHandler::Delete("_1");
			IO::FileHandler::Delete("_2");
			IO::FileHandler f1, f2;
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(f1.Open("_1", IO::FileHandler::AccessMode::Write)));
			Assert::IsTrue(ActionHasher().CheckHashFromFile(Crypto::HasherAlgorithm(), "_1", "_2", Crypto::Encoding()) == Return::InputFileEmpty);
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(f1.WriteData(1024, Buffer("T", "T" + 1))));
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(f1.Flush()));
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(f2.Open("_2", IO::FileHandler::AccessMode::Write)));
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(f2.WriteData(1024, Buffer("T", "T" + 1))));
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(f2.Flush()));
			Assert::IsTrue(ActionHasher().CheckHashFromFile(Crypto::HasherAlgorithm(), "_1", "_2", Crypto::Encoding()) == Return::HashNotMatch);
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(f1.Delete()));
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(f2.Delete()));
		}

		TEST_METHOD(FileNotFound)
		{
			IO::FileHandler::Delete("_1");
			IO::FileHandler::Delete("_2");
			IO::FileHandler f1, f2;
			Assert::IsTrue(ActionHasher().CheckHashFromFile(Crypto::HasherAlgorithm(), "_1", "_2", Crypto::Encoding()) == Return::InputFileNotFound);
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(f1.Open("_1", IO::FileHandler::AccessMode::Write)));
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(f1.WriteData(1024, Buffer("T", "T" + 1))));
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(f1.Flush()));
			Assert::IsTrue(ActionHasher().CheckHashFromFile(Crypto::HasherAlgorithm(), "_1", "_2", Crypto::Encoding()) == Return::HashedFileNotFound);
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(f2.Open("_2", IO::FileHandler::AccessMode::Write)));
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(f2.WriteData(1024, Buffer("T", "T" + 1))));
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(f2.Flush()));
			Assert::IsTrue(ActionHasher().CheckHashFromFile(Crypto::HasherAlgorithm(), "_1", "_2", Crypto::Encoding()) == Return::HashNotMatch);
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(f1.Delete()));
			Assert::IsTrue(IO::ReturnTraits::IsSuccess(f2.Delete()));
		}
	};
}
