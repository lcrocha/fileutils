#include "FileUtilsEngine.h"
#include <numeric>
#include <sstream>
#include "../Common/Version.h"
#include "../../Libs/Helper/XML/Converter.h"
#include "../../libs/Helper/Windows/DlgInput.h"
#include "DlgConfig.h"
#include "resource.h"
using namespace FileUtilsEngine;

#define WORKER_ELEMENT	"worker"
#define IV_ATTR			"iv"
#define NONCE_ATTR		"nonce"
#define TAG_ATTR		"tag"
#define PAGESIZE_ATTR	"pageSize"
#define ENCODING_ATTR	"encoding"
#define SYMMETRICCIPHER_ELEMENT "symmetricCipher"

#pragma region Return Translation
FileTypedUtilReturnsMap ReturnUtil::_fileTypedUtilReturnsMap =
{
	{
		FileType::Input,
		{
			{ IO::Return::NotFound, Return::InputFileNotFound},
			{ IO::Return::FilePathEmpty, Return::InputFilePathEmpty},
			{ IO::Return::BufferEmpty, Return::InputFileEmpty },
			{ IO::Return::FileEmpty, Return::InputFileEmpty },
			{ IO::Return::ReadAccessError,Return::InputReadAccessError},
			{ IO::Return::WriteAccessError, Return::InputWriteAccessError},
			{ IO::Return::ReadingBreak, Return::InputReadingBreak },
		},
	},
	{
		FileType::Hashed,
		{
			{ IO::Return::NotFound, Return::HashedFileNotFound },
			{ IO::Return::FilePathEmpty, Return::HashedFilePathEmpty },
			{ IO::Return::FileEmpty, Return::HashedFileEmpty },
			{ IO::Return::ReadAccessError,Return::HashedReadAccessError },
			{ IO::Return::WriteAccessError, Return::HashedWriteAccessError },
		},
	},
	{
		FileType::Ciphered,
		{
			{ IO::Return::NotFound, Return::CipheredFileNotFound },
			{ IO::Return::FilePathEmpty, Return::CipheredFilePathEmpty },
			{ IO::Return::FileEmpty, Return::CipheredFileEmpty },
			{ IO::Return::ReadAccessError,Return::CipheredReadAccessError },
			{ IO::Return::WriteAccessError, Return::CipheredWriteAccessError },
			{ IO::Return::ReadingBreak, Return::CipheredReadingBreak },
		},
	},
	{
		FileType::Unciphered,
		{
			{ IO::Return::NotFound, Return::UncipheredFileNotFound },
			{ IO::Return::FilePathEmpty, Return::UncipheredFilePathEmpty },
			{ IO::Return::FileEmpty, Return::UncipheredFileEmpty },
			{ IO::Return::ReadAccessError,Return::UncipheredReadAccessError },
			{ IO::Return::WriteAccessError, Return::UncipheredWriteAccessError },
		},
	},
	{
		FileType::Secret,
		{
			{ IO::Return::NotFound, Return::SecretFileNotFound },
			{ IO::Return::FilePathEmpty, Return::SecretFilePathEmpty },
			{ IO::Return::FileEmpty, Return::SecretFileEmpty },
			{ IO::Return::ReadAccessError,Return::SecretReadAccessError },
			{ IO::Return::WriteAccessError, Return::SecretWriteAccessError },
		},
	},
	{
		FileType::Random,
		{
			{ IO::Return::NotFound, Return::RandomFileNotFound },
			{ IO::Return::FilePathEmpty, Return::RandomFilePathEmpty },
			{ IO::Return::FileEmpty, Return::RandomFileEmpty },
			{ IO::Return::ReadAccessError,Return::RandomReadAccessError },
			{ IO::Return::WriteAccessError, Return::RandomWriteAccessError },
		},
	},
	{
		FileType::PrivateKey,
		{
			{ IO::Return::NotFound, Return::PrivateKeyFileNotFound },
			{ IO::Return::FilePathEmpty, Return::PrivateKeyFilePathEmpty },
			{ IO::Return::FileEmpty, Return::PrivateKeyFileEmpty },
			{ IO::Return::ReadAccessError,Return::PrivateKeyReadAccessError },
			{ IO::Return::WriteAccessError, Return::PrivateKeyWriteAccessError },
		},
	},
	{
		FileType::PublicKey,
		{
			{ IO::Return::NotFound, Return::PublicKeyFileNotFound },
			{ IO::Return::FilePathEmpty, Return::PublicKeyFilePathEmpty },
			{ IO::Return::FileEmpty, Return::PublicKeyFileEmpty },
			{ IO::Return::ReadAccessError,Return::PublicKeyReadAccessError },
			{ IO::Return::WriteAccessError, Return::PublicKeyWriteAccessError },
		},
	},
	{
		FileType::Signature,
		{
			{ IO::Return::NotFound, Return::SignatureFileNotFound },
			{ IO::Return::FilePathEmpty, Return::SignatureFilePathEmpty },
			{ IO::Return::FileEmpty, Return::SignatureFileEmpty },
			{ IO::Return::ReadAccessError,Return::SignatureReadAccessError },
			{ IO::Return::WriteAccessError, Return::SignatureWriteAccessError },
			{ IO::Return::ReadingBreak, Return::SignatureReadingBreak },
		},
	},
	{
		FileType::Encoded,
		{
			{ IO::Return::NotFound, Return::EncodedFileNotFound },
			{ IO::Return::FilePathEmpty, Return::EncodedFilePathEmpty },
			{ IO::Return::FileEmpty, Return::EncodedFileEmpty },
			{ IO::Return::ReadAccessError,Return::EncodedReadAccessError },
			{ IO::Return::WriteAccessError, Return::EncodedWriteAccessError },
		},
	},
};
#pragma endregion

const Crypto::HasherAlgorithm AuthCipherData::GetHashAlgorithm() const
{
	byte b = 0;
	for (auto& wd : WorkerDatas) if(!wd.Iv.empty() && !wd.Nonce.empty())
	{
		b = std::accumulate(wd.Iv.begin(), wd.Iv.end(), b, [](const byte b1, const byte b2) { return b1 ^ b2; });
		b = std::accumulate(wd.Nonce.begin(), wd.Nonce.end(), b, [](const byte b1, const byte b2) { return b1 ^ b2; });
	}

	return AuthCipherData::GetHashAlgorithm(b);
}

const Crypto::AuthSymmetricChainingMode AuthCipherData::GetAcm() const
{
	return Crypto::AuthSymmetricChainingMode::AES_GCM;
}

void AuthCipherData::GetSaltedSecret(const Crypto::HasherAlgorithm& hashAlg, const Buffer& secret, const Buffer& salt, Buffer& saltedSecret)
{
	Buffer ss(secret.rbegin(), secret.rend());
	ss.insert(ss.end(), salt.rbegin(), salt.rend());

	Crypto::HashBuffer(hashAlg, ss, saltedSecret);
}

const Crypto::HasherAlgorithm AuthCipherData::GetHashAlgorithm(const byte& b)
{
	return (Crypto::HasherAlgorithm)(b % 7);
}

Return ReturnUtil::Convert(const FileType fileType, IO::Return uRet)
{
	if (uRet == IO::Return::OK)
		return Return::OK;
	//Looking for filetype
	if (
		_fileTypedUtilReturnsMap.find(fileType) != _fileTypedUtilReturnsMap.end() &&
		_fileTypedUtilReturnsMap[fileType].find(uRet) != _fileTypedUtilReturnsMap[fileType].end())
	{
		return _fileTypedUtilReturnsMap[fileType][uRet];
	}

	return Return::UtilLibNotMapped;
}

Return FileUtilsEngine::OpenAsymmetricCipher(Crypto::IAsymmetricCipher** s)
{
	//Validating parameters
	if (s == nullptr)
		return Return::NullValue;
	//Creating hasher
	Crypto::CreateAsymmetricCipher(s);
	//Initializing hasher
	if (*s != nullptr && Crypto::ReturnTraits::IsFail((*s)->Initialize()))
		return Return::AsymmetricCipherInitializationError;

	return Return::OK;
}

Return FileUtilsEngine::OpenAuthSymmetricCipher(Crypto::IAuthSymmetricCipher** s, Crypto::IAuthInfo** a, const Crypto::AuthSymmetricChainingMode& acm, const Buffer& secret, const Buffer& nonce, uint& blockSize, uint& tagSize)
{
	//Validating parameters
	if (s == nullptr)
		return Return::NullValue;
	if (secret.size() == 0)
		return Return::SecretBufferEmpty;
	//Creating Symmetric cipher
	Crypto::CreateAuthSymmetricCipher(s);
	//Initializing cipher
	if (*s != nullptr && Crypto::ReturnTraits::IsFail((*s)->Initialize(acm)))
		return Return::SymmetricCipherInitializationError;
	//Creating auth tag
	Crypto::CreateAuthInfo(a);
	//Generating symmetric key
	if (secret.size() > 0 && Crypto::ReturnTraits::IsFail((*s)->GenerateSymmetricKey(secret, nonce, *a)))
		return Return::SymmetricCipherGenerateKeyError;
	(*s)->GetBlockSize(blockSize);
	tagSize = (*a)->GetTagSize();

	return Return::OK;
}

Return FileUtilsEngine::GetSymmetricCipherBlockSize(const Crypto::SymmetricChainingMode& scm, ulong& blockSize)
{
	ulong bs = 0ul;
	WRL::ComPtr<Crypto::ISymmetricCipher> c;
	auto r = OpenSymmetricCipher(c.GetAddressOf(), scm, Buffer(4), bs);
	if (ReturnTraits::IsFail(r))
		return r;
	//All ok
	blockSize = bs;

	return Return::OK;
}

Return FileUtilsEngine::GetAuthSymmetricCipherBlockSize(const Crypto::AuthSymmetricChainingMode& acm, ulong& blockSize, ulong& tagSize)
{
	uint bs = 0ul, ts = 0ul;
	WRL::ComPtr<Crypto::IAuthSymmetricCipher> ac;
	WRL::ComPtr<Crypto::IAuthInfo> a;
	auto r = OpenAuthSymmetricCipher(ac.GetAddressOf(), a.GetAddressOf(), acm, Buffer(4), Buffer(12), bs, ts);
	if (ReturnTraits::IsFail(r))
		return r;
	//All ok
	blockSize = bs;
	tagSize = ts;

	return Return::OK;
}

Return FileUtilsEngine::OpenSymmetricCipher(Crypto::ISymmetricCipher** s, const Crypto::SymmetricChainingMode& scm, const Buffer& secret, ulong& blockSize)
{
	//Validating parameters
	if (s == nullptr)
		return Return::NullValue;
	if (secret.size() == 0)
		return Return::SecretBufferEmpty;
	//Creating Symmetric cipher
	Crypto::CreateSymmetricCipher(s);
	//Initializing cipher
	if (*s != nullptr && Crypto::ReturnTraits::IsFail((*s)->Initialize(scm)))
		return Return::SymmetricCipherInitializationError;
	//Generating symmetric key
	if (secret.size() > 0 && Crypto::ReturnTraits::IsFail((*s)->GenerateSymmetricKey(secret)))
		return Return::SymmetricCipherGenerateKeyError;
	//Getting block size
	(*s)->GetBlockSize(blockSize);

	return Return::OK;
}

Return FileUtilsEngine::OpenHasher(Crypto::IHasher** h, Crypto::HasherAlgorithm alg)
{
	//Validating parameters
	if (h == nullptr)
		return Return::NullValue;
	//Creating hasher
	Crypto::CreateHasher(h);
	//Initializing hasher
	if (*h != nullptr && Crypto::ReturnTraits::IsFail((*h)->Initialize(alg)))
		return Return::HasherInitializationError;

	return Return::OK;
}

Return FileUtilsEngine::OpenSigner(Crypto::ISigner** s)
{
	//Validating parameters
	if (s == nullptr)
		return Return::NullValue;
	//Creating hasher
	Crypto::CreateSigner(s);
	//Initializing hasher
	if (*s != nullptr && Crypto::ReturnTraits::IsFail((*s)->Initialize(Crypto::SignerKeySize::ECDSA_256)))
		return Return::SignerInitializationError;

	return Return::OK;
}

Return FileUtilsEngine::OpenFile(const FileType fileType, IO::FileHandler& f, const std::string& filePath, const IO::FileHandler::AccessMode accessMode)
{
	return ReturnUtil::Convert(fileType, f.Open(filePath, accessMode));
}


Return FileUtilsEngine::ReadDataFromFile(const FileType fileType, const std::string& filePath, Buffer& data)
{
	IO::FileHandler f;
	auto ret = OpenFile(fileType, f, filePath, IO::FileHandler::AccessMode::Read);
	if (ReturnTraits::IsFail(ret))
		return ret;

	return ReturnUtil::Convert(fileType, f.ReadData(FileUtilsEngine::GetConfig()->GetGlobalConfig().PageSize, data));
}

Return FileUtilsEngine::ReadDataFromFile(const FileType fileType, const std::string& filePath, const IO::FuncReadIterateFile& fRead)
{
	IO::FileHandler f;
	//Opening file
	auto ret = OpenFile(fileType, f, filePath, IO::FileHandler::AccessMode::Read);
	if (ReturnTraits::IsFail(ret))
		return ret;

	return ReturnUtil::Convert(fileType, f.ReadData(FileUtilsEngine::GetConfig()->GetGlobalConfig().PageSize, fRead));
}

Return FileUtilsEngine::CalculateHashFromFile(const std::string& filePath, Crypto::HasherAlgorithm alg, Buffer& hash, const StatusOperationFunc& s)
{
	//Creating hasher
	WRL::ComPtr<Crypto::IHasher> h;
	auto ret = OpenHasher(h.GetAddressOf(), alg);
	if (ReturnTraits::IsFail(ret))
		return ret;
	//Iterating file
	auto fileType = FileType::Input;
	IO::FileHandler f;
	ret = OpenFile(fileType, f, filePath, IO::FileHandler::AccessMode::Read);
	if (ReturnTraits::IsFail(ret))
		return ret;
	//Checking size
	if (0ul == f.GetFileSize())
		return Return::InputFileEmpty;
	//Reading
	auto index = 0ull;
	auto totalSize = f.GetFileSize();
	auto& pageSize = FileUtilsEngine::GetConfig()->GetGlobalConfig().PageSize;
	while (index < totalSize)
	{
		auto size = std::min<ulonglong>(pageSize, totalSize - index);
		auto br = 0ull;
		Buffer b(size);
		ret = ReturnUtil::Convert(fileType, f.ReadData(b, br));
		if (ReturnTraits::IsFail(ret))
			return ret;
		//Hashing
		if (Crypto::ReturnTraits::IsFail(h->HashData(b)))
			return Return::InputReadingBreak;
		//Incrementing
		index += size;
		//Logging
		if (s)
			s(StatusOperationType::Hashing, index, totalSize);
	}
	//Calculating hash
	Buffer hashTmp;
	if (Crypto::ReturnTraits::IsFail(h->Calculate(hashTmp)))
		return Return::HashCalculateError;
	//All ok
	hash = hashTmp;

	return Return::OK;
}

Return FileUtilsEngine::WriteDataOnFile(const FileType fileType, const std::string& filePath, const Buffer& data)
{
	//Opening to write hash on file
	IO::FileHandler f;
	auto ret = OpenFile(fileType, f, filePath, IO::FileHandler::AccessMode::Write);
	if (ReturnTraits::IsFail(ret))
		return ret;

	return ReturnUtil::Convert(fileType, f.WriteData(FileUtilsEngine::GetConfig()->GetGlobalConfig().PageSize, data));
}

Return FileUtilsEngine::WriteDataOnFile(const FileType fileType, const std::string& filePath, const ulong bytesToWrite, IO::FuncWriteIterateFile fWrite)
{
	//Opening to write hash on file
	IO::FileHandler f;
	auto ret = OpenFile(fileType, f, filePath, IO::FileHandler::AccessMode::Write);
	if (ReturnTraits::IsFail(ret))
		return ret;

	return ReturnUtil::Convert(fileType, f.WriteData(bytesToWrite, fWrite));
}

const ushort FileUtilsEngine::GetTotalThreads()
{
	SYSTEM_INFO si = { 0 };
	::GetSystemInfo(&si);

	return std::max<ushort>((ushort)si.dwNumberOfProcessors, 1);
}

const std::string FileUtilsEngine::GetAppName()
{
	std::ostringstream oss;
	oss << APP_NAME_STR << " [" << APP_PRODUCTVERSION_STR << "]";

	return std::move(oss.str());
}

const std::string FileUtilsEngine::GetAppDescription()
{
	return APP_DESCRIPTION_STR;
}

const char* FileUtilsEngine::GetReturnCodeMessge(const Return& c)
{
	if (c == Return::OK) return "Ok";
	if (c == Return::Break) return "Process interrupted";
	if (c == Return::HasherInitializationError) return "Hasher initialization error";
	if (c == Return::SignerInitializationError) return "Signer initialization error";
	if (c == Return::HashDataError) return "Hash data error";
	if (c == Return::HashCalculateError) return "Hash calculate error";
	if (c == Return::ReadingFileOpeningError) return "Reading file opening error";
	if (c == Return::ReadingFileError) return "Reading file error";
	if (c == Return::ReadingFileIterateError) return "Reading file iterate error";
	if (c == Return::WritingFileOpeningError) return "Writing file opening error";
	if (c == Return::WritingFileError) return "Writing file error";
	if (c == Return::Base64EncodeError) return "Base64 encode error";
	if (c == Return::Base64DecodeError) return "Base64 decode error";
	if (c == Return::HexEncodeError) return "Hex encode error";
	if (c == Return::HexDecodeError) return "Hex decode";
	if (c == Return::HashNotMatch) return "Hash not match";
	if (c == Return::AsymmetricCipherInitializationError) return "Asymmetric cipher initialization error";
	if (c == Return::AsymmetricCipherGenerateKeyError) return "Asymmetric cipher generate key error";
	if (c == Return::AsymmetricCipherExportKeyPairError) return "Asymmetric cipher export key pair error";
	if (c == Return::SignerGenerateKeyError) return "Signer generate key error";
	if (c == Return::SignerExportKeyPairError) return "Signer export key pair error";
	if (c == Return::SignerSignError) return "Signer sign error";
	if (c == Return::SignerVerifyError) return "signer verify error";
	if (c == Return::SymmetricCipherInitializationError) return "Symmetric cipher initialization error";
	if (c == Return::SymmetricCipherGenerateKeyError) return " Symmetric cipher generate key error";
	if (c == Return::GenerateRandomError) return "Generate random error";
	if (c == Return::NullValue) return "Null value";
	if (c == Return::SizeEmpty) return "Size empty";
	if (c == Return::SecretBufferEmpty) return "Secret buffer empty";
	if (c == Return::InputFileNotFound) return "Input file not found";
	if (c == Return::InputFilePathEmpty) return "Input file path empty";
	if (c == Return::InputFileEmpty) return "Input file empty";
	if (c == Return::InputReadAccessError) return "Input read access error";
	if (c == Return::InputWriteAccessError) return "Input write access error";
	if (c == Return::InputReadingBreak) return "Input reading break";
	if (c == Return::HashedFileNotFound) return "Hashed file not found";
	if (c == Return::HashedFilePathEmpty) return "Hashed filepath empty";
	if (c == Return::HashedFileEmpty) return "Hashed file empty";
	if (c == Return::HashedReadAccessError) return "Hashed read access error";
	if (c == Return::HashedWriteAccessError) return "Hashed write access error";
	if (c == Return::CipheredFileNotFound) return "Ciphered file not found";
	if (c == Return::CipheredFilePathEmpty) return "Ciphered file path empty";
	if (c == Return::CipheredFileEmpty) return "Ciphered file empty";
	if (c == Return::CipheredReadAccessError) return "Ciphered read access error";
	if (c == Return::CipheredWriteAccessError) return "Ciphered write access error";
	if (c == Return::CipheredReadingBreak) return "Ciphered reading break";
	if (c == Return::UncipheredFileNotFound) return "Unciphered file not found";
	if (c == Return::UncipheredFilePathEmpty) return "Unciphered file path empty";
	if (c == Return::UncipheredFileEmpty) return "Unciphered file empty";
	if (c == Return::UncipheredReadAccessError) return "Unciphered read access error";
	if (c == Return::UncipheredWriteAccessError) return "Unciphered write access error";
	if (c == Return::SecretFileNotFound) return "Secret file not found";
	if (c == Return::SecretFilePathEmpty) return "Secret file path empty";
	if (c == Return::SecretFileEmpty) return "Secret file empty";
	if (c == Return::SecretReadAccessError) return "Secret read access error";
	if (c == Return::SecretWriteAccessError) return "Secret write access error";
	if (c == Return::RandomFileNotFound) return "Random file not found";
	if (c == Return::RandomFilePathEmpty) return "Random file path empty";
	if (c == Return::RandomFileEmpty) return "Random file empty";
	if (c == Return::RandomReadAccessError) return "Random read access error";
	if (c == Return::RandomWriteAccessError) return "Random write access error";
	if (c == Return::PrivateKeyFileNotFound) return "Private key file not found";
	if (c == Return::PrivateKeyFilePathEmpty) return "Private key file path empty";
	if (c == Return::PrivateKeyFileEmpty) return "Private key file empty";
	if (c == Return::PrivateKeyReadAccessError) return "Private key read access error";
	if (c == Return::PrivateKeyWriteAccessError) return "Private key write access error";
	if (c == Return::PublicKeyFileNotFound) return "Public key file not found";
	if (c == Return::PublicKeyFilePathEmpty) return "Public key file path empty";
	if (c == Return::PublicKeyFileEmpty) return "Public key file empty";
	if (c == Return::PublicKeyReadAccessError) return "Public key read access error";
	if (c == Return::PublicKeyWriteAccessError) return "Public key write access error";
	if (c == Return::SignatureFileNotFound) return "Signature file not found";
	if (c == Return::SignatureFilePathEmpty) return "Signature file path empty";
	if (c == Return::SignatureFileEmpty) return "Signature file empty";
	if (c == Return::SignatureReadAccessError) return "Signature read access error";
	if (c == Return::SignatureWriteAccessError) return "Signature write access error";
	if (c == Return::SignatureReadingBreak) return "Signature reading break";
	if (c == Return::EncodedFileNotFound) return "Encoded file not found";
	if (c == Return::EncodedFilePathEmpty) return "Encoded file path empty";
	if (c == Return::EncodedFileEmpty) return "Encoded file empty";
	if (c == Return::EncodedReadAccessError) return "Encoded read access error";
	if (c == Return::EncodedWriteAccessError) return "Encoded write access error";
	if (c == Return::UtilLibNotMapped) return "UtilLib not mapped error";

	_ASSERT(false);
	return "Error IMPOSSIBLE!!!";
}

Return Envelopper::Generate(tinyxml2::XMLDocument& doc, tinyxml2::XMLElement* father, const Node& node)
{
	auto current = doc.NewElement(node.name.c_str());
	father->InsertEndChild(current);
	//Setting text
	if (!node.value.empty())
		current->SetText(node.value.c_str());
	//Browsing attributes
	for (const auto& attr : node.attrs) if (!attr.first.empty())
	{
		current->SetAttribute(attr.first.c_str(), attr.second.c_str());
	}
	//Browsing nodes
	for (const auto& n : node.nodes)
	{
		auto r = Generate(doc, current, n);
		if (ReturnTraits::IsFail(r))
			return r;
	}

	return Return::OK;
}

Return Envelopper::Generate(const bool& prettyPrint, std::string& envelop, const Node& node)
{
	tinyxml2::XMLDocument doc;
	auto root = doc.NewElement(ROOT_ELEMENT);
	doc.InsertEndChild(root);
	//Attributes
	root->SetAttribute(VERSION_ATTR, APP_PRODUCTVERSION_STR);
	auto r = Generate(doc, root, node);
	if (ReturnTraits::IsFail(r))
		return r;
	//Getting output
	tinyxml2::XMLPrinter out(nullptr, !prettyPrint);
	doc.Print(&out);
	//Setting output
	envelop = std::move(out.CStr());

	return Return::OK;
}

Return Envelopper::Parse(tinyxml2::XMLElement* father, Node& node)
{
	node.name = father->Name();
	auto text = father->GetText();
	if (nullptr != text)
		node.value.assign(text);
	//Browsing attributes
	auto attr = father->FirstAttribute();
	while (nullptr != attr)
	{
		node.attrs.insert({ attr->Name(), attr->Value() });
		attr = attr->Next();
	}
	//Browsing inner nodes
	auto child = father->FirstChildElement();
	while (nullptr != child)
	{
		Node childNode;
		auto r = Parse(child, childNode);
		if (ReturnTraits::IsFail(r))
			return r;
		node.nodes.push_back(std::move(childNode));
		child = child->NextSiblingElement();
	}

	return Return::OK;
}

Return Envelopper::Parse(const std::string& envelop, Node& node)
{
	if (envelop.empty())
		return Return::CipheredFileEmpty;
	//Parsing
	tinyxml2::XMLDocument doc;
	if (0 != doc.Parse(envelop.c_str()))
		return Return::CipheredFileNotRecognized;

	return Parse(doc.FirstChildElement(), node);
}

static const Crypto::AuthSymmetricChainingMode GetAcm(const char* acm)
{
	if (0 == ::strcmp("AES_CCM", acm))
		return Crypto::AuthSymmetricChainingMode::AES_CCM;
	else if (0 == ::strcmp("AES_GCM", acm))
		return Crypto::AuthSymmetricChainingMode::AES_GCM;

	throw std::runtime_error("Invalid mode");
}

static const char* GetAcm(const Crypto::AuthSymmetricChainingMode& acm)
{
	if (acm == Crypto::AuthSymmetricChainingMode::AES_CCM)
		return "AES_CCM";

	return "AES_GCM";
}

Return Envelopper::GenerateSymmetricCipher(const AuthCipherData& acd, std::string& envelop)
{
	Node root;
	root.name = SYMMETRICCIPHER_ELEMENT;
	root.attrs =
	{
		{ PAGESIZE_ATTR, std::to_string(acd.PageSize) },
		{ ENCODING_ATTR, Crypto::GetEncoding(acd.Encoding) },
	};
	//Browsing workerdatas
	for (const auto& workerData : acd.WorkerDatas) if (!workerData.Iv.empty() && !workerData.Nonce.empty() && !workerData.Tag.empty())
	{
		Node workerNode;
		workerNode.name = WORKER_ELEMENT;
		workerNode.attrs =
		{
			{ IV_ATTR, Crypto::Base64Encode(workerData.Iv).c_str() },
			{ NONCE_ATTR, Crypto::Base64Encode(workerData.Nonce).c_str() },
			{ TAG_ATTR, Crypto::Base64Encode(workerData.Tag).c_str() }
		};
		root.nodes.push_back(std::move(workerNode));
	}

	return Generate(FileUtilsEngine::GetConfig()->IsPretyPrint(), envelop, root);
}

Return Envelopper::ParseSymmetricCipher(const std::string& envelop, std::string& version, AuthCipherData& acd)
{
	Node root;
	auto r = Parse(envelop, root);
	if (ReturnTraits::IsFail(r))
		return r;
	//Browsing root attributes
	for (auto& attr : root.attrs)
	{
		if (attr.first == VERSION_ATTR)
			version = attr.second.c_str();
	}
	//Browsing nodes
	for (const auto& node : root.nodes) if (node.name == SYMMETRICCIPHER_ELEMENT)
	{
		//Browsing sc attributes
		for (auto& attr : node.attrs)
		{
			if (attr.first == PAGESIZE_ATTR)
				acd.PageSize = std::atol(attr.second.c_str());
			else if (attr.first == ENCODING_ATTR)
				acd.Encoding = Crypto::GetEncoding(attr.second.c_str());
		}
		//Browsing inner
		for (auto& inner : node.nodes) if (inner.name == WORKER_ELEMENT)
		{
			AuthCipherWorkerData workerData;
			for (auto& attr : inner.attrs)
			{
				if (attr.first == IV_ATTR)
					workerData.Iv = Crypto::Base64Decode(attr.second.c_str());
				else if (attr.first == NONCE_ATTR)
					workerData.Nonce = Crypto::Base64Decode(attr.second.c_str());
				else if (attr.first == TAG_ATTR)
					workerData.Tag = Crypto::Base64Decode(attr.second.c_str());
			}
			acd.WorkerDatas.push_back(std::move(workerData));
		}
	}

	return (!acd.WorkerDatas.empty() && 0 != acd.PageSize ? Return::OK : Return::CipheredFileNotRecognized);
}

#define SYMMETRICCIPHER_CONFIG_ELEMENT "symmetricCipherConfig"
#define PRETTYPRINT_ATTR "prettyPrint"
#define SECRET_ATTR "secret"
#define SHOWPROGRESS_ATTR "showProgress"

Return Envelopper::GenerateConfig(const IConfig* cfg, std::string& envelop)
{
	//Getting secret
	Buffer s;
	cfg->GetSecret(s);
	//creating config
	Node root;
	root.name = SYMMETRICCIPHER_CONFIG_ELEMENT;
	root.attrs =
	{
		{ PRETTYPRINT_ATTR, XML::GetBool(cfg->IsPretyPrint()) },
		{ SHOWPROGRESS_ATTR, XML::GetBool(cfg->IsShowProgress()) },
		{ ENCODING_ATTR, Crypto::GetEncoding(cfg->GetEncoding()) },
		{ SECRET_ATTR, Crypto::Base64Encode(s) },
	};

	return Generate(true, envelop, root);
}

Return Envelopper::ParseConfig(const std::string& envelop, std::string& version, IConfig* cfg)
{
	auto configCount = 0u;
	Node root;
	auto r = Parse(envelop, root);
	if (ReturnTraits::IsFail(r))
		return r;
	//Browsing root attributes
	for (auto& attr : root.attrs)
	{
		if (attr.first == VERSION_ATTR)
			version = attr.second.c_str();
	}
	//Browsing nodes
	for (const auto& node : root.nodes) if (node.name == SYMMETRICCIPHER_CONFIG_ELEMENT)
	{
		//Browsing sc attributes
		for (auto& attr : node.attrs)
		{
			if (attr.first == PRETTYPRINT_ATTR)
			{
				cfg->SetPretyPrint(XML::GetBool(attr.second.c_str()));
				configCount++;
			}
			else if (attr.first == SHOWPROGRESS_ATTR)
			{
				cfg->SetShowProgress(XML::GetBool(attr.second.c_str()));
				configCount++;
			}
			else if (attr.first == ENCODING_ATTR)
			{
				cfg->SetEncoding(Crypto::GetEncoding(attr.second.c_str()));
				configCount++;
			}
			else if (attr.first == SECRET_ATTR)
			{
				cfg->SetSecret(Crypto::Base64Decode(attr.second.c_str()));
				configCount++;
			}
		}
	}

	//return (4 == configCount ? Return::OK : Return::ConfigNotRecognized);
	return Return::OK;
}

#define HASHER_CONFIG_ELEMENT "hasher"
#define FILENAME_ATTR "fileName"

Return Envelopper::GenerateHashes(const Hashes& hh, const IConfig* cfg, const std::string& file, std::string& envelop)
{
	Node root;
	root.name = HASHER_CONFIG_ELEMENT;
	root.attrs =
	{
		{ ENCODING_ATTR, Crypto::GetEncoding(cfg->GetEncoding()) },
		{ FILENAME_ATTR, file.c_str() },
	};
	//Browsing hashes
	for (auto& h : hh)
	{
		Node hash;
		hash.name = h.first;
		hash.value = cfg->GetEncoding() == Crypto::Encoding::Base64 ? Crypto::Base64Encode(h.second) : Crypto::HexEncode(h.second);
		root.nodes.push_back(std::move(hash));
	}

	return Generate(cfg->IsPretyPrint(), envelop, root);
}

const bool FileUtilsEngine::GetSecretOrOpenDialog(HINSTANCE instance, const bool retype, Buffer& secret)
{
	if (FileUtilsEngine::GetConfig()->GetSecret(secret))
		return true;
	//Asking for secret
	secret.clear();
	Windows::InputConfig config = { FileUtilsEngine::GetAppName(), IDD_INPUT, "Enter a Secret", IDC_LABEL1, "", IDC_LABEL2, true, IDC_EDIT, "" };
	Windows::InputDialog dlg(instance, config, IDI_TOOL);
	if (!dlg.DoModal())
		return false;
	auto pass1 = dlg.Get();
	//Retype secret
	if (retype)
	{
		config.label1 = "Retype the secret";
		if (!dlg.DoModal())
			return false;
		auto pass2 = dlg.Get();
		if (pass1 != pass2)
			return false;
	}
	//Hashing secret
	Buffer s;
	auto r = Crypto::HashBuffer(Crypto::HasherAlgorithm::SHA1, Crypto::Encoding::Bin, Buffer(pass1.begin(), pass1.end()), Crypto::Encoding::Bin, s);
	if (Crypto::ReturnTraits::IsFail(r))
		return false;
	//Testing if secret matches with salt
	secret = std::move(s);

	return true;
}

const bool FileUtilsEngine::OpenConfigDialog(HINSTANCE instance)
{
	auto config = FileUtilsEngine::GetConfig();
	config->LoadConfig();
	FileUtilsEngine::DlgData data = { IDD_CONFIG, IDC_COMBO_ENCODING, IDC_CHECK_PRETTYPRINT, IDC_LABEL_SECRET, ID_SAVE_SECRET, ID_CLEAR_SECRET };
	FileUtilsEngine::DlgConfig dlg(instance, data, [=](Buffer& s) { return GetSecretOrOpenDialog(instance, true, s); }, IDI_CONFIG);

	return dlg.DoModal();
}

const char* FileUtilsEngine::GetOperationType(const FileUtilsEngine::StatusOperationType& operationType)
{
	if (operationType == FileUtilsEngine::StatusOperationType::Encoding)
		return "Encoding";
	if (operationType == FileUtilsEngine::StatusOperationType::Hashing)
		return "Hashing";
	if (operationType == FileUtilsEngine::StatusOperationType::Entropy)
		return "Entropy";
	if (operationType == FileUtilsEngine::StatusOperationType::Ciphering)
		return "Ciphering";
	if (operationType == FileUtilsEngine::StatusOperationType::Unciphering)
		return "Unciphering";
	if (operationType == FileUtilsEngine::StatusOperationType::Signing)
		return "Signing";
	if (operationType == FileUtilsEngine::StatusOperationType::Reading)
		return "Reading";
	if (operationType == FileUtilsEngine::StatusOperationType::Writing)
		return "Writing";
	if (operationType == FileUtilsEngine::StatusOperationType::GeneratingRandom)
		return "GeneratingRandom";
	if (operationType == FileUtilsEngine::StatusOperationType::VerifyingSignature)
		return "VerifyingSignature";

	return "";
}