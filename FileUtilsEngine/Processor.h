#pragma once
#include "FileUtilsEngine.h"
#include "../../libs/Helper/Concurrent/Consumer.h"

namespace FileUtilsEngine
{
	class ProcessorAuthCipher
	{
	private:
		AuthCipherData& _acd;
		const ProcessorOperation& _operation;
		//Cipher
		WRL::ComPtr<Crypto::IAuthSymmetricCipher> _ac;
		WRL::ComPtr<Crypto::IAuthInfo> _a;
		//Control
		uint _blockSize = 0u;
		uint _tagSize = 0u;
		bool _initialized = false;
	public:
		ProcessorAuthCipher(const ProcessorOperation& operation, AuthCipherData& acd);
		virtual ~ProcessorAuthCipher();
		//Operations
		Return operator()(Concurrent::ProcessorData<InputData, OutputData>& data);
	};
}