#pragma once
#include <Windows.h>
#include "../../libs/Helper/Utils/RandomNumberGenerator.h"
#include "../../libs/Helper/Windows/Controls.h"
#include "FileUtilsEngine.h"
#include "resource.h"

namespace FileUtilsEngine
{
	class DlgAbout : public Windows::Dialog
	{
	private:
		Utils::RandomNumberGenerator _rng;
		const uint _size;
		//Windows::Dialog::SetIcon(icon);
		bool _initialized = false;
		POINT _pt = { 6, 20 };
		RECT _rc;
	public:
		DlgAbout(const HINSTANCE instance, const unsigned dlgId, const unsigned& iconId);
		//Events
		virtual void OnOk() override;
		virtual void OnInitialize() override;
		virtual void OnTimer(const unsigned) override;
		virtual void OnPaint(HDC dc, PAINTSTRUCT& ps) override;
		virtual void OnNotify(WPARAM wParam, LPARAM lParam) override;
	};
}