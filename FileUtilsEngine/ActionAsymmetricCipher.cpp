#include "FileUtilsEngine.h"
#include "ActionAsymmetricCipher.h"
using namespace FileUtilsEngine;

Return ActionAsymmetricCipher::GenerateKeyPair(const Crypto::AsymmetricKeySize keySize, const std::string& privateKeyFilePath, const std::string& publicKeyFilePath, const StatusOperationFunc& s)
{
	if (privateKeyFilePath.size() == 0)
		return Return::PrivateKeyFilePathEmpty;
	if (publicKeyFilePath.size() == 0)
		return Return::PublicKeyFilePathEmpty;
	//Creating cipher
	WRL::ComPtr<Crypto::IAsymmetricCipher> c;
	auto ret = FileUtilsEngine::OpenAsymmetricCipher(c.GetAddressOf());
	if (ReturnTraits::IsFail(ret))
		return ret;
	//Generating keyPair
	if (Crypto::ReturnTraits::IsFail(c->GenerateKeyPair(keySize)))
		return Return::AsymmetricCipherGenerateKeyError;
	//Exporting key
	Buffer privateKey, publicKey;
	if (Crypto::ReturnTraits::IsFail(c->ExportKeyPair(privateKey, publicKey)))
		return Return::AsymmetricCipherExportKeyPairError;
	//Saving PrivateKey
	ret = FileUtilsEngine::WriteDataOnFile(FileType::PrivateKey, privateKeyFilePath, privateKey);
	if (ReturnTraits::IsFail(ret))
		return ret;
	//Saving PublicKey
	ret = FileUtilsEngine::WriteDataOnFile(FileType::PublicKey, publicKeyFilePath, publicKey);
	if (ReturnTraits::IsFail(ret))
	{
		IO::FileHandler::Delete(privateKeyFilePath);
		return ret;
	}

	return Return::OK;
}

Return ActionAsymmetricCipher::CipherFile(const std::string& publicKeyFilePath, const std::string& inputFilePath, const std::string& outputFilePath, const StatusOperationFunc& s)
{
	if (publicKeyFilePath.size() == 0)
		return Return::PublicKeyFilePathEmpty;
	if (inputFilePath.size() == 0)
		return Return::InputFilePathEmpty;
	if (outputFilePath.size() == 0)
		return Return::CipheredFilePathEmpty;
	//Getting publicKey
	Buffer publicKey;
	auto ret = FileUtilsEngine::ReadDataFromFile(FileType::PublicKey, publicKeyFilePath, publicKey);
	if (ReturnTraits::IsFail(ret))
		return ret;
	//Creating cipher
	WRL::ComPtr<Crypto::IAsymmetricCipher> c;
	ret = FileUtilsEngine::OpenAsymmetricCipher(c.GetAddressOf());
	if (ReturnTraits::IsFail(ret))
		return ret;
	//Opening file to write
	IO::FileHandler w;
	ret = FileUtilsEngine::OpenFile(FileType::Ciphered, w, outputFilePath, IO::FileHandler::AccessMode::Write);
	if (ReturnTraits::IsFail(ret))
		return ret;
	//Ciphering file
	ret = FileUtilsEngine::ReadDataFromFile(FileType::Input, inputFilePath, [&s, &w, &c, &publicKey](const Buffer& buffer, const ulonglong partialBytes, const ulonglong totalBytes, ulonglong pageSize)
	{
		//Getting block size
		auto plainBlockSize = 0ul;
		if (Crypto::ReturnTraits::IsFail(c->GetPlainBlockSize(publicKey, plainBlockSize)))
			return false;
		//Looping if needed
		Buffer encrypted;
		for (auto i = 0ul; i < buffer.size(); i += plainBlockSize)
		{
			auto size = i + plainBlockSize < buffer.size() ? plainBlockSize : buffer.size() - i;
			Buffer e(size), m(&buffer[i], &buffer[i] + size);
			//Encrypting
			if (Crypto::ReturnTraits::IsFail(c->EncryptBlock(publicKey, m, e)))
				return false;
			encrypted.insert(encrypted.end(), e.begin(), e.end());
		}
		//Saving file
		if (IO::ReturnTraits::IsFail(w.WriteData(totalBytes, encrypted)))
			return false;
		//Logging
		if (s)
			s(StatusOperationType::Ciphering, partialBytes, totalBytes);

		return true;
	});
	if (ReturnTraits::IsFail(ret))
	{
		w.Delete();
		return ret;
	}
	//Logging
	if (s)
		s(StatusOperationType::Ciphering, 100ull, 100ull);

	return Return::OK;
}

Return ActionAsymmetricCipher::UncipherFile(const std::string& privateKeyFilePath, const std::string& inputFilePath, const std::string& outputFilePath, const StatusOperationFunc& s)
{
	if (privateKeyFilePath.size() == 0)
		return Return::PrivateKeyFilePathEmpty;
	if (inputFilePath.size() == 0)
		return Return::InputFilePathEmpty;
	if (outputFilePath.size() == 0)
		return Return::UncipheredFilePathEmpty;
	//Getting privateKey
	Buffer privateKey;
	auto ret = FileUtilsEngine::ReadDataFromFile(FileType::PrivateKey, privateKeyFilePath, privateKey);
	if (ReturnTraits::IsFail(ret))
		return ret;
	//Creating cipher
	WRL::ComPtr<Crypto::IAsymmetricCipher> c;
	ret = FileUtilsEngine::OpenAsymmetricCipher(c.GetAddressOf());
	if (ReturnTraits::IsFail(ret))
		return ret;
	//Opening file to write
	IO::FileHandler w;
	ret = FileUtilsEngine::OpenFile(FileType::Unciphered, w, outputFilePath, IO::FileHandler::AccessMode::Write);
	if (ReturnTraits::IsFail(ret))
		return ret;
	//Unciphering file
	ret = FileUtilsEngine::ReadDataFromFile(FileType::Ciphered, inputFilePath, [&s, &w, &c, &privateKey](const Buffer& buffer, const ulonglong partialBytes, const ulonglong totalBytes, ulonglong pageSize)
	{
		//Getting block size
		auto encryptedBlockSize = 0ul;
		if (Crypto::ReturnTraits::IsFail(c->GetEncryptedBlockSize(privateKey, encryptedBlockSize)))
			return false;
		//Looping if needed
		Buffer decrypted;
		for (auto i = 0ul; i < buffer.size(); i += std::min<ulong>(encryptedBlockSize, (ulong)buffer.size() - i))
		{
			auto size = std::min<ulong>(encryptedBlockSize, (ulong)buffer.size() - i);
			Buffer d(size), e(buffer.begin() + i, buffer.begin() + i + size);
			//Encrypting
			if (Crypto::ReturnTraits::IsFail(c->DecryptBlock(privateKey, e, d)))
				return false;
			decrypted.insert(decrypted.end(), d.begin(), d.end());
			//Logging
			if (s)
				s(StatusOperationType::Ciphering, partialBytes, totalBytes);
		}
		//Saving file
		if (IO::ReturnTraits::IsFail(w.WriteData(totalBytes, decrypted)))
			return false;

		return true;
	});
	if (ReturnTraits::IsFail(ret))
	{
		w.Delete();
		return ret;
	}
	//Logging
	if (s)
		s(StatusOperationType::Ciphering, 100ull, 100ull);

	return Return::OK;
}
