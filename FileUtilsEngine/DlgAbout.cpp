#include "DlgAbout.h"
using namespace FileUtilsEngine;

DlgAbout::DlgAbout(const HINSTANCE instance, const unsigned dlgId, const unsigned& iconId) : Windows::Dialog(instance, dlgId, iconId), _size(FileUtilsEngine::GetConfig()->GetIconIdsSize())
{
	//_rc = { _pt.x, _pt.y, _pt.x + 64, _pt.y + 64 };
}

void DlgAbout::OnOk()
{
	Windows::Dialog::Close();
}

void DlgAbout::OnInitialize()
{
	Windows::Dialog::SetTimer(0, 200);
}

void DlgAbout::OnTimer(const unsigned)
{
	if (!_initialized)
		return;
	Windows::Dialog::Invalidate(true, &_rc);
}

void DlgAbout::OnPaint(HDC dc, PAINTSTRUCT& ps)
{
	auto cfg = FileUtilsEngine::GetConfig();
	if (!_initialized)
	{
		auto x = ::GetDeviceCaps(dc, LOGPIXELSX);
		auto y = ::GetDeviceCaps(dc, LOGPIXELSY);
		auto ratio = LONG(32.0f*(float(y) / float(96)));
		_rc = { _pt.x, _pt.y, _pt.x + ratio, _pt.y + ratio };
		_initialized = true;
	}
	::DrawIcon(dc, _pt.x, _pt.y, cfg->GetIcon(cfg->GetIconId(_rng.GetUniformDistribution(0, _size - 1))));
}

void DlgAbout::OnNotify(WPARAM wParam, LPARAM lParam)
{
	LPNMHDR pnmh = (LPNMHDR)lParam;
	if (pnmh->idFrom == IDC_LINK_EMAIL || pnmh->idFrom == IDC_LINK_SOURCECODE)
	{
		if ((pnmh->code == NM_CLICK) || (pnmh->code == NM_RETURN))
		{
			PNMLINK link = (PNMLINK)lParam;
			::ShellExecute(NULL, L"open", link->item.szUrl, NULL, NULL, SW_SHOWNORMAL);
		}
	}
}