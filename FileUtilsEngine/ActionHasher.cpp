#include "FileUtilsEngine.h"
#include "ActionHasher.h"
using namespace FileUtilsEngine;

Return ActionHasher::DoHashes(ReadDataFunctor& readFunctor, Hashes& hashes, const Crypto::Encoding& encoding, const StatusOperationFunc& s)
{
	if (readFunctor.IsEmpty())
		return Return::Empty;
	//Querying all hashes
	Crypto::HasherInfos hasherInfos;
	Crypto::QueryHasherInfos(hasherInfos);
	//Creating and initializing hashers
	std::vector<WRL::ComPtr<Crypto::IHasher>> hashers(hasherInfos.size());
	for (ushort i = 0; i < (ushort)hasherInfos.size(); i++)
	{
		auto& hasherInfo = hasherInfos[i];
		auto& hasher = hashers[i];
		//Creating hasher
		if (S_OK != Crypto::CreateHasher(hasher.GetAddressOf()))
			return Return::HasherInitializationError;
		//Initializing
		if (Crypto::ReturnTraits::IsFail(hasher->Initialize(hasherInfo.alg)))
			return Return::HasherInitializationError;
	}
	//Open streamer
	auto total = 0ull;
	readFunctor.GetTotalBytes(total);
	StatusOperationFunctor sf(StatusOperationType::Hashing, s);
	DataReader reader(readFunctor);
	//Browsing content
	auto lastBlock = false;
	while (!lastBlock)
	{
		Buffer b(FileUtilsEngine::GetConfig()->GetGlobalConfig().PageSize);
		auto r = reader.Read(b, lastBlock);
		if (ReturnTraits::IsFail(r))
			return r;
		if (lastBlock && b.empty())
			break;
		//Hashing
		for (auto& hasher : hashers)
		{
			if (Crypto::ReturnTraits::IsFail(hasher->HashData(b)))
				return Return::HashDataError;
		}
		if (sf(b.size(), total))
			return Return::Break;
	}
	//Getting all hashes
	for (ushort i = 0; i < (ushort)hasherInfos.size(); i++)
	{
		auto& hasherInfo = hasherInfos[i];
		auto& hasher = hashers[i];
		//Calculate
		Buffer h;
		if (Crypto::ReturnTraits::IsFail(hasher->Calculate(h)))
			return Return::HashCalculateError;
		//Converting
		Buffer e;
		Crypto::ConvertEncoding(Crypto::Encoding::Bin, h, encoding, e);
		//Adding to map
		hashes[hasherInfo.shortName] = std::move(e);
	}

	return Return::OK;
}

Return ActionHasher::CreateHashesFromFile(const std::string& inputFilePath, Hashes& hashes, const Crypto::Encoding& encoding, const StatusOperationFunc& s)
{
	//Validating parameters
	if (inputFilePath.size() == 0)
		return Return::InputFilePathEmpty;

	return DoHashes(ReadDataFileFunctor(FileType::Input, inputFilePath), hashes, encoding, s);
}

Return ActionHasher::CreateHashesFromBuffer(const Buffer& input, Hashes& hashes, const Crypto::Encoding& encoding, const StatusOperationFunc& s)
{
	return DoHashes(ReadDataBufferFunctor(input), hashes, encoding, s);
}

Return ActionHasher::CreateHashFromFile(const Crypto::HasherAlgorithm alg, const std::string& inputFilePath, const std::string& outputFilePath, Crypto::Encoding encoding, const StatusOperationFunc& s)
{
	//Validating parameters
	if (inputFilePath.size() == 0)
		return Return::InputFilePathEmpty;
	if (outputFilePath.size() == 0)
		return Return::HashedFilePathEmpty;
	//Getting hash from file
	Buffer hash;
	auto ret = FileUtilsEngine::CalculateHashFromFile(inputFilePath, alg, hash, s);
	if (ReturnTraits::IsFail(ret))
		return ret;
	//Encoding if needed
	Buffer encoded;
	auto r = Crypto::ConvertEncoding(Crypto::Encoding::Bin, hash, encoding, encoded);
	if (Crypto::ReturnTraits::IsFail(r))
	{
		if (Crypto::Return::InvalidBase64 == r)
			return Return::Base64EncodeError;
		if (Crypto::Return::InvalidHex == r)
			return Return::HexEncodeError;
		return Return::UtilLibNotMapped;
	}
	//Saving hash on file
	ret = FileUtilsEngine::WriteDataOnFile(FileType::Hashed, outputFilePath, encoded);
	if (ReturnTraits::IsFail(ret))
		return Return::WritingFileError;

	return Return::OK;
}

Return ActionHasher::CheckHashFromFile(const Crypto::HasherAlgorithm alg, const std::string& inputFilePath, const std::string& outputFilePath, Crypto::Encoding encoding, const StatusOperationFunc& s)
{
	//Validating parameters
	if (inputFilePath.size() == 0)
		return Return::InputFilePathEmpty;
	if (outputFilePath.size() == 0)
		return Return::HashedFilePathEmpty;
	//Calculate hash from data file
	Buffer fileDataHash;
	auto ret = FileUtilsEngine::CalculateHashFromFile(inputFilePath, alg, fileDataHash, s);
	if (ReturnTraits::IsFail(ret))
		return ret;
	//Getting hash from hashFile
	Buffer fileHashHash;
	ret = FileUtilsEngine::ReadDataFromFile(FileType::Hashed, outputFilePath, fileHashHash);
	if (ReturnTraits::IsFail(ret))
		return ret;
	//Decoding if necessary
	Buffer decoded;
	auto r = Crypto::ConvertEncoding(encoding, fileHashHash, Crypto::Encoding::Bin, decoded);
	if (Crypto::ReturnTraits::IsFail(r))
	{
		if (Crypto::Return::InvalidBase64 == r)
			return Return::Base64DecodeError;
		if (Crypto::Return::InvalidHex == r)
			return Return::HexDecodeError;
		return Return::UtilLibNotMapped;
	}

	return decoded == fileDataHash ? Return::OK : Return::HashNotMatch;
}