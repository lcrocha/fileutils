#include "DlgConfig.h"
#include "FileUtilsEngine.h"
using namespace FileUtilsEngine;

DlgConfig::DlgConfig(const HINSTANCE instance, const DlgData& data, const GetSecretFunc& getSecretFunc, const unsigned& iconId) :
	_data(data),
	_getSecretFunc(getSecretFunc),
	Dialog(instance, data.DialogId, iconId),
	Encoding(Dialog::GetHwnd(), data.EncodingId),
	PrettyPrint(Dialog::GetHwnd(), data.PrettyPrintId),
	Secret(Dialog::GetHwnd(), data.SecretId),
	ButtonSaveSecret(Dialog::GetHwnd(), data.SaveSecretId),
	ButtonClearSecret(Dialog::GetHwnd(), data.ClearSecretId),
	ButtonOk(Dialog::GetHwnd(), IDOK),
	ButtonCancel(Dialog::GetHwnd(), IDCANCEL)
{
	_config = FileUtilsEngine::GetConfig();
}

void DlgConfig::Save()
{
	_config->SetPretyPrint(PrettyPrint.IsChecked());
	_config->SetEncoding(Encoding.GetCursorSel() == 0 ? Crypto::Encoding::Base64 : Crypto::Encoding::Hex);
	_config->SaveConfig();
}

void DlgConfig::OnInitialize()
{
	Dialog::SetText(FileUtilsEngine::GetAppName());
	PrettyPrint.SetChecked(_config->IsPretyPrint());
	Encoding.Clear();
	Encoding.AddString(Crypto::GetEncoding(Crypto::Encoding::Base64));
	Encoding.AddString(Crypto::GetEncoding(Crypto::Encoding::Hex));
	Encoding.SetCursor(_config->GetEncoding() == Crypto::Encoding::Base64 ? 0 : 1);
	ButtonOk.SetEnable(true);
	ButtonCancel.SetEnable(true);
	Update();
}

void DlgConfig::OnOk()
{
	Save();
}

void DlgConfig::Update()
{
	auto ss = _config->IsSecretSaved();
	Secret.SetText(ss ? "Secret saved!" : "No secret saved");
	ButtonSaveSecret.SetEnable(!ss);
	ButtonClearSecret.SetEnable(ss);
}

void DlgConfig::OnCommand(UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	auto config = FileUtilsEngine::GetConfig();
	if (wParam == _data.SaveSecretId)
	{
		Buffer s;
		if (_getSecretFunc(s))
		{
			config->SetSecret(s);
			Save();
			Update();
		}
	}
	else if (wParam == _data.ClearSecretId)
	{
		config->ClearSecret();
		Save();
		Update();
	}
}