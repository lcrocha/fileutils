#pragma once
#include "FileUtilsEngine.h"
#include "../../libs/Crypto/ICrypto.h"
#include "resource.h"
#include <Windows.h>

namespace FileUtilsEngine
{
	class Config : public FileUtilsEngine::IConfig
	{
	private:
		const HMODULE _instance;
		//Media
		const ushort _iconSize = 16;
		const std::vector<unsigned> _iconIds = { IDI_CIPHER, IDI_CONFIG, IDI_ENTROPY, IDI_HASH, IDI_LOCK, IDI_LOCKERS, IDI_TOOL, IDI_UNCIPHER, IDI_UNLOCK, IDI_GEAR0, IDI_GEAR1, IDI_EXCLAMATION0, IDI_EXCLAMATION1, IDI_ABOUT, IDI_EXIT, IDI_CUBE, IDI_REPORT };
		std::map<unsigned, HICON> _icons;
		std::map<unsigned, HBITMAP> _bitmaps;
		//Configuration persistence
		std::string _folderPath;
		std::string _filePath;
		//Configs
		GlobalConfig _globalConfig;
		//Secret
		const ushort _ivSize = 16;
		const ushort _saltSize = 12;
		const ushort _nonceSize = 12;
		const ushort _tagSize = 16;
		//Config
		Crypto::Encoding _encoding = Crypto::Encoding::Bin;
		bool _prettyPrint = false;
		bool _showProgress = true;
		Crypto::CipheredBuffer _secret;
		//Auxilliary
		Buffer GetMasterSecret() const;
		bool DoEnvelop(const Buffer& data) const;
		bool DoUnenvelop(Buffer& data);
	public:
		Config(const HMODULE& instance);
		virtual ~Config();
		//Operations
		virtual void LoadConfig() override;
		virtual void SaveConfig() const override;
		//IConfig
		virtual GlobalConfig& GetGlobalConfig() override { return _globalConfig; }
		//Secret
		virtual const bool IsSecretSaved() const override;
		virtual const bool& IsShowProgress() const override { return _showProgress; }
		virtual void ClearSecret() override;
		virtual const bool GetSecret(Buffer& secret) const override;
		virtual void SetSecret(const Buffer& secret) override { _secret.Set(secret); }
		//Get's and Set's
		virtual void SetConfig(const Crypto::Encoding& e, const bool& pp) override { SetEncoding(e); SetPretyPrint(pp); }
		virtual const Crypto::Encoding& GetEncoding() const override { return _encoding; }
		virtual void SetEncoding(const Crypto::Encoding& e) override { _encoding = e; }
		virtual const bool& IsPretyPrint() const override { return _prettyPrint; }
		virtual void SetPretyPrint(const bool& pp) override { _prettyPrint = pp; }
		virtual void SetShowProgress(bool v) override { _showProgress = v; }
		//Resources
		virtual unsigned GetIconId(unsigned index) override { return _iconIds[index]; }
		virtual uint GetIconIdsSize() const override { return uint(_iconIds.size()); }
		virtual HICON GetIcon(unsigned id) override { return _icons[id]; }
		virtual uint GetIconsSize() const override { return uint(_icons.size()); }
		virtual HBITMAP GetBitmap(unsigned id) override { return _bitmaps[id]; }
		virtual uint GetBitmapsSize() const override { return uint(_bitmaps.size()); }
	};
}
