#include "PosProcessor.h"
#include <iomanip>
#include <sstream>
using namespace FileUtilsEngine;

PosProcessorAuthCipher::PosProcessorAuthCipher(DataWriter& writer) : _writer(writer)
{
}

PosProcessorAuthCipher::~PosProcessorAuthCipher()
{}

Return PosProcessorAuthCipher::operator()(Concurrent::PosProcessorData<InputData>& data)
{
	//Testing buffer
	auto& output = data.GetOutput();
	if (output.empty())
		return Return::OK;

	return _writer.Write(output, data.IsLastPage());
}
