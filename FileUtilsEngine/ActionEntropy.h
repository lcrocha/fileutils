#pragma once
#pragma once
#include "FileUtilsEngine.h"

namespace FileUtilsEngine
{
	class ActionEntropy
	{
	public:
		ActionEntropy() {}
		virtual ~ActionEntropy() {}
		//Operations
		Return CalcutateEntropyFromFile(const std::string& inputFilePath, float& entropy, const StatusOperationFunc& s = nullptr);
	};
}

