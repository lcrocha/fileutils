#include "Processor.h"
using namespace FileUtilsEngine;

ProcessorAuthCipher::ProcessorAuthCipher(const ProcessorOperation& operation, AuthCipherData& acd) : _operation(operation), _acd(acd)
{}

ProcessorAuthCipher::~ProcessorAuthCipher()
{}

Return ProcessorAuthCipher::operator()(Concurrent::ProcessorData<InputData, OutputData>& data)
{
	if (!_initialized)
	{
		auto& globalConfig = FileUtilsEngine::GetConfig()->GetGlobalConfig();
		//Initializing
		auto& workerData = _acd.WorkerDatas[data.GetWorkerIndex()];
		if (ProcessorOperation::Cipher == _operation)
		{
			workerData.Nonce = Crypto::GenRandom(globalConfig.NonceSize);
			workerData.Iv = Crypto::GenRandom(globalConfig.IvSize);
		}
		//Creating cipher
		auto r = OpenAuthSymmetricCipher(_ac.GetAddressOf(), _a.GetAddressOf(), _acd.GetAcm(), _acd.Secret, workerData.Nonce, _blockSize, _tagSize);
		if (ReturnTraits::IsFail(r))
			return r;
		//Setting IV
		if (Crypto::ReturnTraits::IsFail(_ac->SetIV(workerData.Iv)))
			return Return::SymmetricCipherIVError;
		//Importing tag
		if (ProcessorOperation::Uncipher == _operation)
		{
			if (Crypto::ReturnTraits::IsFail(_a->ImportAuthTag(workerData.Tag)))
				return Return::CipheredReadingBreak;
		}
		//All good
		_initialized = true;
	}
	//Executing operation
	Buffer output;
	auto& input = data.GetInput();
	if (_operation == ProcessorOperation::Cipher)
	{
		if (Crypto::ReturnTraits::IsFail(_ac->EncryptBlock(input, output, data.IsPartialLastPage())))
			return Return::InputReadingBreak;
		//Exporting tag
		if (data.IsPartialLastPage())
		{
			OutputData authTag;
			if (Crypto::ReturnTraits::IsFail(_a->ExportAuthTag(authTag)))
				return Return::InputReadingBreak;
			_acd.WorkerDatas[data.GetWorkerIndex()].Tag = std::move(authTag);
		}
	}
	else //if (_operation == ProcessorOperation::Uncipher)
	{
		if (Crypto::ReturnTraits::IsFail(_ac->DecryptBlock(input, output, data.IsPartialLastPage())))
			return Return::CipheredReadingBreak;
	}
	//Saving block
	data.SetOutput(std::move(output));

	return Return::OK;
}