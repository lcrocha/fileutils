#pragma once
#include "FileUtilsEngine.h"

namespace FileUtilsEngine
{
	class ActionEncoder
	{
	public:
		ActionEncoder() {}
		//Operations
		Return ConvertData(const Buffer& input, const Crypto::Encoding inputEncoding, Buffer& output, const Crypto::Encoding outputEncoding);
		Return ConvertFile(const std::string& inputFilePath, const Crypto::Encoding inputEncoding, const std::string& outputFilePath, const Crypto::Encoding outputEncoding, const StatusOperationFunc& s = nullptr);
		Return GenerateFileRandomData(const Helper::BufferType& bufferType, const std::string& filePath, const ulonglong& totalSize, const StatusOperationFunc& s = nullptr);
	};
}