#include "FileUtilsEngine.h"
#include "ActionLocker.h"
#include <cstdio>
#include <RestartManager.h>
#include <string>
#pragma comment(lib, "rstrtmgr.lib")
using namespace FileUtilsEngine;

#define GETSTRING(s) s, s + ::wcslen(s)

LockFileControl::LockFileControl(FILE* f) : _f(f)
{
}

LockFileControl::LockFileControl(LockFileControl&& lfc) : _f(std::move(lfc._f))
{
	lfc._f = nullptr;
}

LockFileControl::~LockFileControl()
{
	Unlock();
}

bool LockFileControl::Unlock()
{
	if (nullptr == _f)
		return false;
	std::fclose(_f);
	_f = nullptr;

	return true;
}

void LockFileControl::operator=(FILE*&& f)
{
	_f = f;
	f = nullptr;
}

Return ActionLocker::LockFile(const std::string& filePath, LockFileControl& lfc)
{
	if (filePath.empty())
		return Return::InputFilePathEmpty;
	FILE* f = nullptr;
	if (0 != ::fopen_s(&f, filePath.c_str(), "r+"))
		return Return::InputFileNotFound;
	//All ok
	lfc = std::move(f);

	return Return::OK;
}

Return ActionLocker::LockFile(const std::string& filePath, std::function<void()> func)
{
	LockFileControl lfc;
	auto r = LockFile(filePath, lfc);
	if (ReturnTraits::IsFail(r))
		return r;
	//All ok
	if (func)
		func();

	return Return::OK;
}

Return ActionLocker::GetAppsLockingFile(const std::string& filePath, AppInfos& apps)
{
	if (filePath.empty())
		return Return::InputFilePathEmpty;
	//Starting session
	auto ret = Return::InputReadAccessError;
	DWORD session = 0l;
	WCHAR sessionKey[CCH_RM_SESSION_KEY + 1] = { 0 };
	if (ERROR_SUCCESS == ::RmStartSession(&session, 0, sessionKey))
	{
		//registering resources
		std::wstring wFile(filePath.begin(), filePath.end());
		LPCWSTR wpFile = wFile.c_str();
		if (ERROR_SUCCESS == ::RmRegisterResources(session, 1, &wpFile, 0, NULL, 0, NULL))
		{
			DWORD reason = 0l;
			UINT procInfoNeeded = 0u, procInfo = 0u;
			if (ERROR_MORE_DATA == ::RmGetList(session, &procInfoNeeded, &procInfo, nullptr, &reason) && procInfoNeeded > 0)
			{
				procInfo = procInfoNeeded;
				std::vector<RM_PROCESS_INFO> rgpi(procInfoNeeded);
				if (ERROR_SUCCESS == ::RmGetList(session, &procInfoNeeded, &procInfo, &rgpi[0], &reason))
				{
					for (auto i = 0u; i < procInfo; i++)
					{
						auto process = ::OpenProcess(PROCESS_QUERY_LIMITED_INFORMATION, FALSE, rgpi[i].Process.dwProcessId);
						if (NULL != process)
						{
							FILETIME ftCreate = { 0 }, ftExit = { 0 }, ftKernel = { 0 }, ftUser = { 0 };
							if (::GetProcessTimes(process, &ftCreate, &ftExit, &ftKernel, &ftUser) && ::CompareFileTime(&rgpi[i].Process.ProcessStartTime, &ftCreate) == 0)
							{
								WCHAR sz[MAX_PATH] = { L"" };
								DWORD cch = MAX_PATH;
								if (::QueryFullProcessImageNameW(process, 0, sz, &cch) && cch <= MAX_PATH)
								{
									apps.emplace_back(
										(ushort)rgpi[i].ApplicationType,
										std::string(GETSTRING(rgpi[i].strAppName)),
										(uint)rgpi[i].Process.dwProcessId,
										std::string(GETSTRING(sz)));
									ret = Return::OK;
								}
							}
							::CloseHandle(process);
						}
					}
				}
			}
		}
		::RmEndSession(session);
	}

	return ret;
}
