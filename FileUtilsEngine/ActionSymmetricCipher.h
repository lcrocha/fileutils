#pragma once
#include "FileUtilsEngine.h"
#include "Processor.h"

namespace FileUtilsEngine
{
	class ActionSymmetricCipher
	{
	private:
		//Auxilliary
		Return DoEnvelop(const AuthCipherData& acd, WriteDataFunctor& writeFunctor);
		Return DoUnenvelop(AuthCipherData& acd, ReadDataFunctor& readFunctor);
		Return DoAuthCipher(const Buffer& secret, ReadDataFunctor& readFunctor, WriteDataFunctor& writeFunctor, const StatusOperationFunc& s= nullptr);
		Return DoAuthUncipher(const Buffer& secret, ReadDataFunctor& readFunctor, WriteDataFunctor& writeFunctor, const StatusOperationFunc& s= nullptr);
	public:
		ActionSymmetricCipher() {}
		virtual ~ActionSymmetricCipher() {}
		//Operations
		Return CipherBuffer(const Buffer& secret, const Buffer& input, Buffer& output, const StatusOperationFunc& s= nullptr);
		Return CipherFile(const Buffer& secret, const std::string& inputFilePath, const std::string& outputFilePath, const StatusOperationFunc& s = nullptr);
		Return CipherFileUsingSecretFile(const std::string& secretFilePath, const std::string& inputFilePath, const std::string& outputFilePath, const StatusOperationFunc& s = nullptr);
		Return UncipherBuffer(const Buffer& secret, const Buffer& input, Buffer& output, const StatusOperationFunc& s= nullptr);
		Return UncipherFile(const Buffer& secret, const std::string& inputFilePath, const std::string& outputFilePath, const StatusOperationFunc& s = nullptr);
		Return UncipherFileUsingSecretFile(const std::string& secretFilePath, const std::string& inputFilePath, const std::string& outputFilePath, const StatusOperationFunc& s = nullptr);
	};
}
