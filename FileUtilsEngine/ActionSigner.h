#pragma once
#include "FileUtilsEngine.h"

namespace FileUtilsEngine
{
	class ActionSigner
	{
	public:
		ActionSigner() {}
		//Operations
		Return GenerateKeyPair(const std::string& privateKeyFilePath, const std::string& publicKeyFilePath, const StatusOperationFunc& s = nullptr);
		Return SignFile(const std::string& privateKeyFilePath, const std::string& inputFilePath, const std::string& signatureFilePath, const StatusOperationFunc& s = nullptr);
		Return VerifySignature(const std::string& publicKeyFilePath, const std::string& inputFilePath, const std::string& signatureFilePath, const StatusOperationFunc& s = nullptr);
	};
}