#include "FileUtilsEngine.h"
#include "ActionSigner.h"
using namespace FileUtilsEngine;

Return ActionSigner::GenerateKeyPair(const std::string& privateKeyFilePath, const std::string& publicKeyFilePath, const StatusOperationFunc& s)
{
	if (privateKeyFilePath.size() == 0)
		return Return::PrivateKeyFilePathEmpty;
	if (publicKeyFilePath.size() == 0)
		return Return::PublicKeyFilePathEmpty;
	//Creating signer
	WRL::ComPtr<Crypto::ISigner> signer;
	auto ret = OpenSigner(signer.GetAddressOf());
	if (ReturnTraits::IsFail(ret))
		return ret;
	//Generating keyPair
	if (Crypto::ReturnTraits::IsFail(signer->GenerateKeyPair()))
		return Return::SignerGenerateKeyError;
	//Exporting key
	Buffer privateKey, publicKey;
	if (Crypto::ReturnTraits::IsFail(signer->ExportKeyPair(privateKey, publicKey)))
		return Return::SignerExportKeyPairError;
	//Saving PrivateKey
	ret = FileUtilsEngine::WriteDataOnFile(FileType::PrivateKey, privateKeyFilePath, privateKey);
	if (ReturnTraits::IsFail(ret))
		return ret;
	//Saving PublicKey
	ret = FileUtilsEngine::WriteDataOnFile(FileType::PublicKey, publicKeyFilePath, publicKey);
	if (ReturnTraits::IsFail(ret))
	{
		IO::FileHandler::Delete(privateKeyFilePath);
		return ret;
	}

	return Return::OK;
}

Return ActionSigner::SignFile(const std::string& privateKeyFilePath, const std::string& inputFilePath, const std::string& signatureFilePath, const StatusOperationFunc& s)
{
	if (privateKeyFilePath.size() == 0)
		return Return::PrivateKeyFilePathEmpty;
	if (inputFilePath.size() == 0)
		return Return::InputFilePathEmpty;
	if (signatureFilePath.size() == 0)
		return Return::SignatureFilePathEmpty;
	//Getting privateKey
	Buffer privateKey;
	auto ret = FileUtilsEngine::ReadDataFromFile(FileType::PrivateKey, privateKeyFilePath, privateKey);
	if (ReturnTraits::IsFail(ret))
		return ret;
	//Getting hash from file
	Buffer hash;
	ret = FileUtilsEngine::CalculateHashFromFile(inputFilePath, Crypto::HasherAlgorithm::SHA1, hash, s);
	if (ReturnTraits::IsFail(ret))
		return ret;
	//Creating signer
	WRL::ComPtr<Crypto::ISigner> signer;
	ret = OpenSigner(signer.GetAddressOf());
	if (ReturnTraits::IsFail(ret))
		return ret;
	//Signing
	Buffer signature;
	if (Crypto::ReturnTraits::IsFail(signer->SignHash(privateKey, hash, signature)))
		return Return::SignerSignError;

	return FileUtilsEngine::WriteDataOnFile(FileType::Signature, signatureFilePath, signature);
}

Return ActionSigner::VerifySignature(const std::string& publicKeyFilePath, const std::string& inputFilePath, const std::string& signatureFilePath, const StatusOperationFunc& s)
{
	if (publicKeyFilePath.size() == 0)
		return Return::PublicKeyFilePathEmpty;
	if (inputFilePath.size() == 0)
		return Return::InputFilePathEmpty;
	if (signatureFilePath.size() == 0)
		return Return::SignatureFilePathEmpty;
	//Getting publicKey
	Buffer publicKey;
	auto ret = FileUtilsEngine::ReadDataFromFile(FileType::PublicKey, publicKeyFilePath, publicKey);
	if (ReturnTraits::IsFail(ret))
		return ret;
	//Getting hash from file
	Buffer hash;
	ret = FileUtilsEngine::CalculateHashFromFile(inputFilePath, Crypto::HasherAlgorithm::SHA1, hash, s);
	if (ReturnTraits::IsFail(ret))
		return ret;
	//Getting signature
	Buffer signature;
	ret = FileUtilsEngine::ReadDataFromFile(FileType::Signature, signatureFilePath, signature);
	if (ReturnTraits::IsFail(ret))
		return ret;
	//Creating signer
	WRL::ComPtr<Crypto::ISigner> signer;
	ret = OpenSigner(signer.GetAddressOf());
	if (ReturnTraits::IsFail(ret))
		return ret;

	return Crypto::ReturnTraits::IsSuccess(signer->VerifySignature(publicKey, hash, signature)) ? Return::OK : Return::SignerVerifyError;
}
