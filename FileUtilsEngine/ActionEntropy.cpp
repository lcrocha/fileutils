#include "ActionEntropy.h"
using namespace FileUtilsEngine;

Return ActionEntropy::CalcutateEntropyFromFile(const std::string& inputFilePath, float& entropy, const StatusOperationFunc& s)
{
	if (inputFilePath.size() == 0)
		return Return::InputFilePathEmpty;
	Crypto::EntropyStreamer es;
	//Iterating file
	auto fileType = FileType::Input;
	IO::FileHandler f;
	auto ret = OpenFile(fileType, f, inputFilePath, IO::FileHandler::AccessMode::Read);
	if (ReturnTraits::IsFail(ret))
		return ret;
	//Checking size
	if (0ul == f.GetFileSize())
		return Return::InputFileEmpty;
	//Reading
	auto index = 0ull;
	auto totalSize = f.GetFileSize();
	auto& pageSize = FileUtilsEngine::GetConfig()->GetGlobalConfig().PageSize;
	while (index < totalSize)
	{
		auto size = std::min<ulonglong>(pageSize, totalSize - index);
		auto br = 0ull;
		Buffer b(size);
		ret = ReturnUtil::Convert(fileType, f.ReadData(b, br));
		if (ReturnTraits::IsFail(ret))
			return ret;
		//Hashing
		if (Crypto::ReturnTraits::IsFail(es.Update(b)))
			return Return::InputReadingBreak;
		//Incrementing
		index += size;
		//Logging
		if (s && s(StatusOperationType::Entropy, index, totalSize))
			return Return::Break;
	}
	//Calculating hash
	float e = 0.0f;
	if (Crypto::ReturnTraits::IsFail(es.Calculate(e)))
		return Return::EntropyCalculateError;
	//All ok
	entropy = e;

	return Return::OK;
}