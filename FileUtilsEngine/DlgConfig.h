#pragma once
#include "../../libs/Helper/Windows/DlgInput.h"
#include "FileUtilsEngine.h"

namespace FileUtilsEngine
{
	struct DlgData
	{
		uint DialogId;
		uint EncodingId;
		uint PrettyPrintId;
		uint SecretId;
		uint SaveSecretId;
		uint ClearSecretId;
	};

	typedef std::function<bool(Buffer&)> GetSecretFunc;

	class DlgConfig : public Windows::Dialog
	{
	private:
		IConfig* _config;
		const DlgData& _data;
		const GetSecretFunc _getSecretFunc;
		//Controls
		Windows::ComboControl Encoding;
		Windows::CheckControl PrettyPrint;
		Windows::LabelControl Secret;
		Windows::ButtonControl ButtonOk, ButtonCancel, ButtonSaveSecret, ButtonClearSecret;
		//Auxilliary
		void Update();
	public:
		DlgConfig(const HINSTANCE instance, const DlgData& data, const GetSecretFunc& getSecretFunc, const unsigned& iconId);
		virtual ~DlgConfig() {}
		//Operations
		void Save();
		//Events
		virtual void OnOk() override;
		virtual void OnInitialize() override;
		virtual void OnCommand(UINT uMsg, WPARAM wParam, LPARAM lParam) override;
	};
}
