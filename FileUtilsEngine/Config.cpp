#include "Config.h"
#include <memory>
#include "../Common/Version.h"
#include "../../libs/Helper/Xml/tinyxml2.h"
#include "../../libs/Helper/Xml/Converter.h"
#include "../../libs/Helper/IO/FileHandler.h"
#include <ShlObj.h>
#include <sstream>
#include "../../Libs/Helper/Windows/GDIUtils.h"
using namespace FileUtilsEngine;

#define ACM_ATTR "acm"
#define PAGESIZE_ATTR "pageSize"
#define PRETTYPRINT_ATTR "prettyPrint"
#define SALT_ATTR "salt"
#define SALTEDSECRET_ATTR "saltedsecret"

std::unique_ptr<IConfig> _config;

Return FileUtilsEngine::InitializeConfig(HINSTANCE instance)
{
	_config.reset(new Config(instance));

	return Return::OK;
}

IConfig* FileUtilsEngine::GetConfig()
{
	_ASSERT(_config);

	return _config.get();
}

Return FileUtilsEngine::FinalizeConfig()
{
	_config.reset();

	return Return::OK;
}

Config::Config(const HMODULE& instance) : _instance(instance)
{
	std::string path(MAX_PATH, 0);
	if (SUCCEEDED(SHGetFolderPathA(NULL, CSIDL_APPDATA | CSIDL_FLAG_CREATE, NULL, 0, &path[0])))
	{
		path.erase(std::find(path.begin(), path.end(), 0), path.end());
		std::ostringstream oss;
		oss << path << "\\" << APP_NAME_STR;
		//Creating folder
		_folderPath = oss.str();
		oss << "\\" << APP_NAME_STR << ".cfg";
		_filePath = oss.str();
	}
	//Adding resources
	for (auto& id : _iconIds)
	{
		_icons.insert({ id, ::LoadIcon(instance, MAKEINTRESOURCE(id)) });
		_bitmaps.insert({ id, Windows::GDIUtils::ConvertIcon(instance, id, _iconSize) });
	}

	LoadConfig();
}

Config::~Config()
{
	for (auto& k : _bitmaps) if (NULL != k.second)
		::DeleteObject(k.second);

	for (auto& k : _icons) if (NULL != k.second)
		::DestroyIcon(k.second);
}

const bool Config::IsSecretSaved() const
{
	return !_secret.IsEmpty();
}

void Config::ClearSecret()
{
	_secret.Clear();
}

const bool Config::GetSecret(Buffer& secret) const
{
	if (!_secret.Get(secret))
		return false;

	return true;
}

Buffer Config::GetMasterSecret() const
{
	Buffer m = _globalConfig.MasterSecret, u, h;
	if (Crypto::GetMachineGuid(u))
		m.insert(m.end(), u.begin(), u.end());
	Crypto::HashBuffer(Crypto::HasherAlgorithm::SHA1, m, h);

	return h;
}

bool Config::DoUnenvelop(Buffer& data)
{
	if (IO::ReturnTraits::IsFail(IO::FileHandler::IsExist(_filePath)) || IO::ReturnTraits::IsSuccess(IO::FileHandler::IsEmpty(_filePath)))
		return false;
	//Open config
	IO::FileHandler f;
	if (IO::ReturnTraits::IsFail(f.Open(_filePath, IO::FileHandler::AccessMode::Read)))
		return false;
	//Reading config
	Buffer d;
	if (IO::ReturnTraits::IsFail(f.ReadData(_globalConfig.PageSize, d)))
		return false;
	//Unpacking
	if (!Crypto::DoGenericUncipher(GetMasterSecret(), d, data))
	{
		f.Delete();
		return false;
	}

	return true;
}

void Config::LoadConfig()
{
	Buffer d;
	if (!DoUnenvelop(d))
	{
		ClearSecret();
		IO::FileHandler::Delete(_filePath);
		return;
	}
	//Parsing cfg
	std::string v;
	if (ReturnTraits::IsFail(Envelopper::ParseConfig({d.begin(), d.end()}, v, this)))
	{
		ClearSecret();
		IO::FileHandler::Delete(_filePath);
		return;
	}
}

bool Config::DoEnvelop(const Buffer& data) const
{
	Buffer e;
	if (!Crypto::DoGenericCipher(GetMasterSecret(), data, e))
		return false;
	//Creating directory
	::CreateDirectoryA(_folderPath.c_str(), nullptr);
	//Saving file
	IO::FileHandler f;
	if (IO::ReturnTraits::IsFail(f.Open(_filePath, IO::FileHandler::AccessMode::Write)))
		return false;
	//Writing config
	auto bw = 0ull;
	if (IO::ReturnTraits::IsFail(f.WriteData(e, bw)))
		return false;
	
	return true;
}

void Config::SaveConfig() const
{
	//Creating cfg
	std::string cfg;
	if (ReturnTraits::IsFail(Envelopper::GenerateConfig(this, cfg)) || cfg.empty())
	{
		IO::FileHandler::Delete(_filePath);
		return;
	}
	//Saving cfg
	if (!DoEnvelop({ cfg.begin(), cfg.end() }))
	{
		IO::FileHandler::Delete(_filePath);
		return;
	}
}