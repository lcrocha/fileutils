#pragma once
#include "FileUtilsEngine.h"

namespace FileUtilsEngine
{
	class ActionAsymmetricCipher
	{
	public:
		ActionAsymmetricCipher() {}
		//Operations
		Return GenerateKeyPair(const Crypto::AsymmetricKeySize keySize, const std::string& privateKeyFilePath, const std::string& publicKeyFilePath, const StatusOperationFunc& s = nullptr);
		Return CipherFile(const std::string& publicKeyFilePath, const std::string& inputFilePath, const std::string& outputFilePath, const StatusOperationFunc& s = nullptr);
		Return UncipherFile(const std::string& privateKeyFilePath, const std::string& inputFilePath, const std::string& outputFilePath, const StatusOperationFunc& s = nullptr);
	};
}