#include "FileUtilsEngine.h"
#include "ActionEncoder.h"
using namespace FileUtilsEngine;

Return ActionEncoder::ConvertData(const Buffer& input, const Crypto::Encoding inputEncoding, Buffer& output, const Crypto::Encoding outputEncoding)
{
	//Validating parameters
	if (input.size() == 0)
		return Return::InputFilePathEmpty;
	//Checking encodings
	if (inputEncoding == outputEncoding)
	{
		output = input;
		return Return::OK;
	}
	//Encoding input
	Buffer inputBin;
	if (inputEncoding == Crypto::Encoding::Base64)
	{
		std::string t(input.begin(), input.end());
		if (Crypto::ReturnTraits::IsFail(Crypto::Base64Decode(t, inputBin)))
			return Return::Base64DecodeError;
	}
	else if (inputEncoding == Crypto::Encoding::Hex)
	{
		std::string t(input.begin(), input.end());
		if (Crypto::ReturnTraits::IsFail(Crypto::HexDecode(t, inputBin)))
			return Return::HexDecodeError;
	}
	else if (inputEncoding == Crypto::Encoding::Bin)
	{
		inputBin = input;
	}
	//Converting
	if (outputEncoding == Crypto::Encoding::Base64)
	{
		std::string t;
		if (Crypto::ReturnTraits::IsFail(Crypto::Base64Encode(inputBin, t)))
			return Return::Base64EncodeError;
		output.assign(t.begin(), t.end());
	}
	else if (outputEncoding == Crypto::Encoding::Hex)
	{
		std::string t;
		if (Crypto::ReturnTraits::IsFail(Crypto::HexEncode(inputBin, t)))
			return Return::HexEncodeError;
		output.assign(t.begin(), t.end());
	}
	else if (outputEncoding == Crypto::Encoding::Bin)
	{
		output = inputBin;
	}

	return Return::OK;
}

Return ActionEncoder::ConvertFile(const std::string& inputFilePath, const Crypto::Encoding inputEncoding, const std::string& outputFilePath, const Crypto::Encoding outputEncoding, const StatusOperationFunc& s)
{
	//Validating parameters
	if (inputFilePath.size() == 0)
		return Return::InputFilePathEmpty;
	if (outputFilePath.size() == 0)
		return Return::EncodedFilePathEmpty;
	//Creating streamers
	ReadDataFileFunctor rdf(FileType::Input, inputFilePath);
	if (!rdf.IsOpened())
		return Return::InputFileNotFound;
	WriteDataFileFunctor wdf(FileType::Encoded, outputFilePath);
	//Creating converters
	ConverterBase64Encoder b64Encoder;
	ConverterBase64Decoder b64Decoder;
	ConverterHexEncoder hexEncoder;
	ConverterHexDecoder hexDecoder;
	//Setting reader
	DataReader rdr(rdf);
	if (Crypto::Encoding::Base64 == inputEncoding)
		rdr.SetConverters({ b64Decoder });
	else if (Crypto::Encoding::Hex == inputEncoding)
		rdr.SetConverters({ hexDecoder });
	//Setting writer
	DataWriter wtr(wdf);
	if (Crypto::Encoding::Base64 == outputEncoding)
		wtr.SetConverters({ b64Encoder });
	else if (Crypto::Encoding::Hex == outputEncoding)
		wtr.SetConverters({ hexEncoder });
	//Converting data
	auto totalSize = rdf.GetTotalBytes(), index = 0ull;
	auto& pageSize = FileUtilsEngine::GetConfig()->GetGlobalConfig().PageSize;
	auto lastBlock = false;
	while (!lastBlock)
	{
		//Reading data
		Buffer b(pageSize);
		auto r = rdr.Read(b, lastBlock);
		if (ReturnTraits::IsFail(r))
			return r;
		//Writting data
		r = wtr.Write(b, lastBlock);
		if (ReturnTraits::IsFail(r))
			return r;
		//Logging
		index += b.size();
		if (s)
		{
			auto i = index;
			if (Crypto::Encoding::Base64 == inputEncoding)
				i = 3 * index / 2;
			else if (Crypto::Encoding::Base64 == inputEncoding)
				i *= 2ull;
			s(StatusOperationType::Encoding, std::min<ulonglong>(index * i, totalSize), totalSize);
		}
	}

	return Return::OK;
}

Return ActionEncoder::GenerateFileRandomData(const Helper::BufferType& bufferType, const std::string& filePath, const ulonglong& totalSize, const StatusOperationFunc& s)
{
	//Validating parameters
	if (filePath.size() == 0)
		return Return::InputFilePathEmpty;
	if (totalSize == 0)
		return Return::SizeEmpty;
	//Open file
	IO::FileHandler f;
	auto r = FileUtilsEngine::OpenFile(FileType::Random, f, filePath, IO::FileHandler::AccessMode::Write);
	if (ReturnTraits::IsFail(r))
		return r;
	//randomizing
	auto index = 0ull;
	auto& pageSize = FileUtilsEngine::GetConfig()->GetGlobalConfig().PageSize;
	while (index < totalSize)
	{
		auto size = std::min<ulonglong>(pageSize, totalSize - index);
		Buffer data;
		if (Helper::BufferType::BinaryRandom != bufferType)
			data = Helper::GenerateBuffer(bufferType, size);
		else
			data = Crypto::GenRandom(size);
		if (data.empty())
		{
			f.Delete();
			return Return::GenerateRandomError;
		}
		//Saving to file
		r = ReturnUtil::Convert(FileType::Random, f.WriteData(data));
		if (ReturnTraits::IsFail(r))
		{
			f.Delete();
			return r;
		}
		//Incrementing
		index += size;
		//Logging
		if (s)
			s(StatusOperationType::GeneratingRandom, index, totalSize);
	}

	return Return::OK;
}