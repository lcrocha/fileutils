#include "FileUtilsEngine.h"
#include "ActionSymmetricCipher.h"
#include "../../Libs/Helper/Concurrent/Consumer.h"
#include "../../Libs/Helper/Concurrent/notifier.h"
#include "../../libs/Helper/Time/Stopwatch.h"
#include "../../libs/Helper/Utils/Lazy.h"
#include "PreProcessor.h"
#include "Processor.h"
#include "PosProcessor.h"

using namespace FileUtilsEngine;
using AuthCipherConsumer = Concurrent::Consumer<PreProcessorAuthCipher, ProcessorAuthCipher, PosProcessorAuthCipher, ReturnTraits, InputData, OutputData>;

class CustomNotifier
{
private:
	const ulong& _blocksPerWorker;
public:
	CustomNotifier(const ulong& blocksPerWorker) : _blocksPerWorker(blocksPerWorker)
	{}

	bool operator()(std::string& custom)
	{
		custom = " " + std::to_string(_blocksPerWorker);
		return true;
	}
};

class StatusNotifierFunctor
{
	const ulonglong _totalBytes;
	const ulonglong _pageSize;
	const StatusOperationFunc _s;
	const StatusOperationType _operationType;
	Time::Stopwatch _sw;
public:
	StatusNotifierFunctor(const StatusOperationType& operationType, const StatusOperationFunc& s, const ulonglong& ps, const ulonglong& tb) : _s(s), _operationType(operationType), _totalBytes(tb), _pageSize(ps) {}
	
	bool operator()(const Concurrent::Status& s)
	{
		if (!_s)
			return false;
		if (!s.keepPosProcessing && !s.keepProcessing && !s.keepPosProcessing)
			return _s(_operationType, 100ul, 100ul);
		if (s.preProcessed > 0 && _sw.GetElapsed() > 100)
		{
			_sw.Reset();
			auto partial = 100 * ((float)s.posProcessed / (float)s.preProcessed);

			return _s(_operationType, (ulong)partial, 100ul);
		}

		return false;
	}
};

Return ActionSymmetricCipher::DoEnvelop(const AuthCipherData& acd, WriteDataFunctor& writeFunctor)
{
	std::string e;
	auto r = Envelopper::GenerateSymmetricCipher(acd, e);
	if (ReturnTraits::IsFail(r))
		return r;
	//Getting total bytes written
	ulonglong totalBytesWritten = 0ull;
	r = writeFunctor.GetTotalBytesWritten(totalBytesWritten);
	if (ReturnTraits::IsFail(r))
		return r;
	//Writting envelop
	e.insert(e.begin(), '\n');
	r = writeFunctor.Write({ e.begin(), e.end() }, false);
	if (ReturnTraits::IsFail(r))
		return r;
	//Converting base64
	std::string b64;
	if (Crypto::ReturnTraits::IsFail(Crypto::Base64Encode({ (byte*)&totalBytesWritten, (byte*)&totalBytesWritten + sizeof(totalBytesWritten) }, b64)))
		return r;
	//Writting size
	r = writeFunctor.Write({ b64.begin(), b64.end() }, false);
	if (ReturnTraits::IsFail(r))
		return r;

	return Return::OK;
}

Return ActionSymmetricCipher::DoAuthCipher(const Buffer& secret, ReadDataFunctor& readFunctor, WriteDataFunctor& writeFunctor, const StatusOperationFunc& s)
{
	const auto& config = FileUtilsEngine::GetConfig();
	const auto& globalConfig = config->GetGlobalConfig();
	//Setting data
	auto operation = ProcessorOperation::Cipher;
	auto threads = GetTotalThreads();
	AuthCipherData acd = { secret, globalConfig.PageSize, threads > 1 ? threads - 1u : 1u, config->GetEncoding() };
	//Creating consumer
	auto r = Return::OK;
	{
		//Converters
		ConverterDeflator deflator;
		ConverterHashGenerator generator;
		ConverterBase64Encoder b64;
		ConverterHexEncoder hex;
		Converters cc = { generator };
		if (Crypto::Encoding::Base64 == acd.Encoding)
			cc.push_back(b64);
		else if (Crypto::Encoding::Hex == acd.Encoding)
			cc.push_back(hex);
		//Creating reader/writer
		DataReader reader(readFunctor, { deflator });
		DataWriter writer(writeFunctor, std::move(cc));
		//Executing consumer
		auto totalBytes = 0ull;
		readFunctor.GetTotalBytes(totalBytes);
		StatusNotifierFunctor snf(StatusOperationType::Ciphering, s, acd.PageSize, totalBytes);
		AuthCipherConsumer c((ushort)acd.WorkerDatas.size(), globalConfig.PagesPerTime, globalConfig.IdleTimeout, Concurrent::PageSelectorMode::RoundRobin, true, snf);
		r = c
			.CreatePreProcessor(std::cref(operation), std::cref(acd.PageSize), std::ref(reader))
			.CreateProcessors(std::cref(operation), std::ref(acd))
			.CreatePosProcessor(std::ref(writer))
			.Start();
		if (ReturnTraits::IsFail(r))
			return r;
	}

	return DoEnvelop(acd, writeFunctor);
}

Return ActionSymmetricCipher::CipherBuffer(const Buffer& secret, const Buffer& input, Buffer& output, const StatusOperationFunc& s)
{
	//Validating parameters
	if (secret.size() == 0)
		return Return::SecretBufferEmpty;

	return DoAuthCipher(secret, ReadDataBufferFunctor(input), WriteDataBufferFunctor(output), s);
}

Return ActionSymmetricCipher::CipherFile(const Buffer& secret, const std::string& inputFilePath, const std::string& outputFilePath, const StatusOperationFunc& s)
{
	//Validating parameters
	if (secret.size() == 0)
		return Return::SecretBufferEmpty;
	if (inputFilePath.size() == 0)
		return Return::InputFilePathEmpty;
	if (outputFilePath.size() == 0)
		return Return::CipheredFilePathEmpty;
	auto r = DoAuthCipher(secret, ReadDataFileFunctor(FileType::Input, inputFilePath), WriteDataFileFunctor(FileType::Ciphered, outputFilePath), s);
	if (ReturnTraits::IsFail(r))
		IO::FileHandler::Delete(outputFilePath);

	return r;
}

Return ActionSymmetricCipher::CipherFileUsingSecretFile(const std::string& secretFilePath, const std::string& inputFilePath, const std::string& outputFilePath, const StatusOperationFunc& s)
{
	//Validating data
	if (secretFilePath.size() == 0)
		return Return::SecretFilePathEmpty;
	if (inputFilePath.size() == 0)
		return Return::InputFilePathEmpty;
	if (outputFilePath.size() == 0)
		return Return::CipheredFilePathEmpty;
	//Openning secret file
	Buffer secret;
	auto ret = FileUtilsEngine::ReadDataFromFile(FileType::Secret, secretFilePath, secret);
	if (ReturnTraits::IsFail(ret))
		return ret;

	return CipherFile(secret, inputFilePath, outputFilePath, std::move(s));
}

Return ActionSymmetricCipher::DoUnenvelop(AuthCipherData& acd, ReadDataFunctor& readFunctor)
{
	if (!readFunctor.IsOpened())
		return Return::CipheredFileNotFound;
	if (readFunctor.IsEmpty())
		return Return::CipheredFileEmpty;
	ulonglong totalBytes = 0ull;
	if (ReturnTraits::IsFail(readFunctor.GetTotalBytes(totalBytes)))
		return Return::CipheredFileNotRecognized;
	//Getting global config for validation
	auto dataSizeSize = (ulonglong)(sizeof(ulonglong) * 1.5);
	auto& globalConfig = FileUtilsEngine::GetConfig()->GetGlobalConfig();
	if (totalBytes < globalConfig.IvSize + globalConfig.NonceSize + dataSizeSize)
		return Return::CipheredFileNotRecognized;
	//Advancing to getting the data size
	if (ReturnTraits::IsFail(readFunctor.SetOffset(totalBytes - dataSizeSize)))
		return Return::CipheredFileNotRecognized;
	//Reading data size
	Buffer b64(dataSizeSize);
	bool lb = false;
	if (ReturnTraits::IsFail(readFunctor.Read(b64, lb)) || dataSizeSize != b64.size())
		return Return::CipheredFileNotRecognized;
	//Converting from b64
	Buffer b;
	if (Crypto::ReturnTraits::IsFail(Crypto::Base64Decode({ b64.begin(), b64.end()}, b)))
		return Return::CipheredFileNotRecognized;
	ulonglong dataSize = 0ul;
	::memcpy(&dataSize, &b[0], sizeof(dataSize));
	//Checking size
	if (dataSize >= totalBytes)
		return Return::CipheredFileNotRecognized;
	//Advancing to envelop
	if (ReturnTraits::IsFail(readFunctor.SetOffset(dataSize + 1)))
		return Return::CipheredFileNotRecognized;
	//Getting Envelop
	b.resize(totalBytes - dataSize - dataSizeSize - 1);
	if (ReturnTraits::IsFail(readFunctor.Read(b, lb)) || lb)
		return Return::CipheredFileNotRecognized;
	//Parsing envelop
	std::string v;
	auto r = Envelopper::ParseSymmetricCipher({ b.begin(), b.end() }, v, acd);
	if (ReturnTraits::IsFail(r))
		return r;
	//Setting end of file
	if (ReturnTraits::IsFail(readFunctor.SetEndOfData(dataSize)))
		return Return::CipheredFileNotRecognized;

	return Return::OK;
}

Return ActionSymmetricCipher::DoAuthUncipher(const Buffer& secret, ReadDataFunctor& readFunctor, WriteDataFunctor& writeFunctor, const StatusOperationFunc& s)
{
	auto& globalConfig = FileUtilsEngine::GetConfig()->GetGlobalConfig();
	//Setting data
	auto operation = ProcessorOperation::Uncipher;
	AuthCipherData acd(secret);
	//Unenveloping
	auto r = DoUnenvelop(acd, readFunctor);
	if (ReturnTraits::IsFail(r))
		return r;
	//Creating consumer
	{
		//Converters
		ConverterInflator inflator;
		ConverterHashChecker checker;
		ConverterBase64Decoder b64;
		ConverterHexDecoder hex;
		Converters cc = { checker };
		if (Crypto::Encoding::Base64 == acd.Encoding)
			cc.push_front(b64);
		else if (Crypto::Encoding::Hex == acd.Encoding)
			cc.push_front(hex);
		//Creating reader/writer
		DataReader reader(readFunctor, std::move(cc));
		DataWriter writer(writeFunctor, { inflator });
		//Executing consumer
		auto totalBytes = 0ull;
		readFunctor.GetTotalBytes(totalBytes);
		StatusNotifierFunctor snf(StatusOperationType::Unciphering, s, acd.PageSize, totalBytes);
		AuthCipherConsumer c((ushort)acd.WorkerDatas.size(), globalConfig.PagesPerTime, globalConfig.IdleTimeout, Concurrent::PageSelectorMode::RoundRobin, true, snf);
		r = c
			.CreatePreProcessor(std::cref(operation), std::cref(acd.PageSize), std::ref(reader))
			.CreateProcessors(std::cref(operation), std::ref(acd))
			.CreatePosProcessor(std::ref(writer))
			.Start();
	}

	return r;
}

Return ActionSymmetricCipher::UncipherBuffer(const Buffer& secret, const Buffer& input, Buffer& output, const StatusOperationFunc& s)
{
	//Validating parameters
	if (secret.size() == 0)
		return Return::SecretBufferEmpty;

	return DoAuthUncipher(secret, ReadDataBufferFunctor(input), WriteDataBufferFunctor(output), s);
}

Return ActionSymmetricCipher::UncipherFile(const Buffer& secret, const std::string& inputFilePath, const std::string& outputFilePath, const StatusOperationFunc& s)
{
	//Validating parameters
	if (secret.size() == 0)
		return Return::SecretBufferEmpty;
	if (inputFilePath.size() == 0)
		return Return::CipheredFilePathEmpty;
	if (outputFilePath.size() == 0)
		return Return::UncipheredFilePathEmpty;
	auto r = DoAuthUncipher(secret, ReadDataFileFunctor(FileType::Ciphered, inputFilePath), WriteDataFileFunctor(FileType::Unciphered, outputFilePath), s);
	if (ReturnTraits::IsFail(r))
		IO::FileHandler::Delete(outputFilePath);

	return r;
}

Return ActionSymmetricCipher::UncipherFileUsingSecretFile(const std::string& secretFilePath, const std::string& inputFilePath, const std::string& outputFilePath, const StatusOperationFunc& s)
{
	//Validating data
	if (secretFilePath.size() == 0)
		return Return::SecretFilePathEmpty;
	if (inputFilePath.size() == 0)
		return Return::CipheredFilePathEmpty;
	if (outputFilePath.size() == 0)
		return Return::UncipheredFilePathEmpty;
	//Openning secret file
	Buffer secret;
	auto ret = FileUtilsEngine::ReadDataFromFile(FileType::Secret, secretFilePath, secret);
	if (ret == Return::InputFileNotFound)
		return Return::SecretFileNotFound;
	if (ret == Return::InputFileEmpty)
		return Return::SecretFileEmpty;
	if (ReturnTraits::IsFail(ret))
		return ret;

	return UncipherFile(secret, inputFilePath, outputFilePath, std::move(s));
}