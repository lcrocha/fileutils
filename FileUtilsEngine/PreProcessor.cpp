#include "PreProcessor.h"
using namespace FileUtilsEngine;

PreProcessorAuthCipher::PreProcessorAuthCipher(const ProcessorOperation& operation, const ulonglong& pageSize, DataReader& reader) : _operation(operation), _pageSize(pageSize), _reader(reader)
{}

PreProcessorAuthCipher::~PreProcessorAuthCipher()
{}

Return PreProcessorAuthCipher::operator()(Concurrent::PreProcessorData<InputData>& data)
{
	if (!_initialized)
	{
		if (!_reader.IsOpened())
			return ProcessorOperation::Cipher == _operation ? Return::InputFileNotFound : Return::CipheredFileNotFound;
		if (_reader.IsEmpty())
			return ProcessorOperation::Cipher == _operation ? Return::InputFileEmpty : Return::CipheredFileEmpty;
		_initialized = true;
	}
	//Reading data from reader
	bool lastBlock = false;
	Buffer b(_pageSize);
	auto r = _reader.Read(b, lastBlock);
	if (ReturnTraits::IsFail(r))
		return r;
	//Setting data input
	data.SetInput(std::move(b), lastBlock);

	return Return::OK;
}
