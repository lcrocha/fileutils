#pragma once
#include "FileUtilsEngine.h"
#include "../../libs/Helper/Concurrent/Consumer.h"
#include <wrl.h>
using namespace Microsoft;

namespace FileUtilsEngine
{
	class PosProcessorAuthCipher
	{
	private:
		DataWriter& _writer;
	public:
		PosProcessorAuthCipher(DataWriter& writer);
		virtual ~PosProcessorAuthCipher();
		//Operation
		Return operator()(Concurrent::PosProcessorData<InputData>& data);
	};
}