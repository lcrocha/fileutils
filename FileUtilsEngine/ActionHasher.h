#pragma once
#include "FileUtilsEngine.h"

namespace FileUtilsEngine
{
	class ActionHasher
	{
	private:
		Return DoHashes(ReadDataFunctor& readFunctor, Hashes& hashes, const Crypto::Encoding& encoding, const StatusOperationFunc& s = nullptr);
	public:
		ActionHasher() {}
		virtual ~ActionHasher() {}
		//Operations
		Return CreateHashesFromBuffer(const Buffer& input, Hashes& hashes, const Crypto::Encoding& encoding, const StatusOperationFunc& s = nullptr);
		Return CreateHashFromFile(const Crypto::HasherAlgorithm alg, const std::string& inputFilePath, const std::string& outputFilePath, Crypto::Encoding encoding, const StatusOperationFunc& s = nullptr);
		Return CreateHashesFromFile(const std::string& inputFilePath, Hashes& hashes, const Crypto::Encoding& encoding, const StatusOperationFunc& s = nullptr);
		Return CheckHashFromFile(const Crypto::HasherAlgorithm alg, const std::string& inputFilePath, const std::string& outputFilePath, Crypto::Encoding encoding, const StatusOperationFunc& s = nullptr);
	};
}