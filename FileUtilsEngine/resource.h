//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by FileUtilsEngine.rc
//

//Icons
#define IDI_CIPHER						200
#define IDI_CONFIG						201
#define IDI_ENTROPY						202
#define IDI_HASH						203
#define IDI_LOCK						204
#define IDI_LOCKERS						205
#define IDI_TOOL						206
#define IDI_UNCIPHER					207
#define IDI_UNLOCK						208
#define IDI_GEAR0						209
#define IDI_GEAR1						210
#define IDI_EXCLAMATION0				211
#define IDI_EXCLAMATION1				212
#define IDI_ABOUT						213
#define IDI_EXIT						214
#define IDI_CUBE						215
#define IDI_REPORT						216

#define IDD_PROGRESS                    101
#define IDD_EDIT                        102
#define IDD_INPUT                       103
#define IDD_ABOUT                       104
#define IDR_MAINFRAME					128
#define IDC_PROGRESS1                   1001
#define IDC_LABEL1                      1002
#define IDC_LABEL2                      1005
#define IDC_EDIT                        1007
#define IDC_STATIC                      1008
#define IDC_LINK_EMAIL                  1009
#define IDC_LINK_SOURCECODE             1010

//DlgConfig
#define IDD_CONFIG                      106
#define IDC_COMBO_ENCODING              1001
#define IDC_CHECK_PRETTYPRINT           1002
#define IDC_LABEL_SECRET                1003
#define ID_SAVE_SECRET                  1004
#define ID_CLEAR_SECRET                 1005

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        102
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
