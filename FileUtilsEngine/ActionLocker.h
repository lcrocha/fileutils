#pragma once
#include "FileUtilsEngine.h"
#include <list>

namespace FileUtilsEngine
{
	struct AppInfo
	{
		ushort type = 0u;
		std::string name;
		uint processId = 0u;
		std::string fullPath;

		AppInfo(ushort t, std::string& n, uint p, std::string& f) : type(t), name(n), processId(p), fullPath(f)
		{}
	};
	typedef std::list<AppInfo> AppInfos;

	class LockFileControl
	{
	private:
		FILE* _f = nullptr;
	public:
		LockFileControl(FILE* f = nullptr);
		LockFileControl(LockFileControl&& lfc);
		virtual ~LockFileControl();
		//Operations
		bool Unlock();
		void operator=(FILE*&& f);
		bool IsLocked() const { return nullptr != _f; }
	};

	class ActionLocker
	{
	public:
		ActionLocker() {}
		//Operations
		Return LockFile(const std::string& filePath, LockFileControl& lfc);
		Return LockFile(const std::string& filePath, std::function<void()> func);
		Return GetAppsLockingFile(const std::string& filePath, AppInfos& apps);
	};
}