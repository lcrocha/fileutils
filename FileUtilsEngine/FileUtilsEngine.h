#pragma once
#include <wrl.h>
#include <map>
#include <list>
#include <vector>
#include <string>
#include <algorithm>
#include <iterator>
#include <memory>
#include <atomic>
#include "../Common/Version.h"
#include "../../Libs/Helper/Helper.h"
#include "../../Libs/Helper/IO/FileHandler.h"
#include "../../Libs/Crypto/ICrypto.h"
#include "../../Libs/Helper/Zip/Zip.h"
#include "../../libs/Helper/Xml/tinyxml2.h"
using namespace Microsoft;

#ifdef _DEBUG
#define VERIFY(e) _ASSERT(e)
#else
#define VERIFY(e) e
#endif

namespace FileUtilsEngine
{
	enum class ProcessorOperation { Cipher, Uncipher };
	enum class FileType { Input, Hashed, Ciphered, Unciphered, Secret, Random, PrivateKey, PublicKey, Signature, Encoded };

	typedef Buffer InputData;
	typedef Buffer OutputData;

	enum class Return
	{
		OK,
		Break,
		Empty,
		HasherInitializationError,
		SignerInitializationError,
		EntropyCalculateError,
		HashDataError,
		HashCalculateError,
		ReadingFileOpeningError,
		ReadingFileError,
		ReadingFileIterateError,
		WritingFileOpeningError,
		WritingFileError,
		Base64EncodeError,
		Base64DecodeError,
		HexEncodeError,
		HexDecodeError,
		InflatorError,
		DeflatorError,
		HashNotMatch,
		AsymmetricCipherInitializationError,
		AsymmetricCipherGenerateKeyError,
		AsymmetricCipherExportKeyPairError,
		SignerGenerateKeyError,
		SignerExportKeyPairError,
		SignerSignError,
		SignerVerifyError,
		SymmetricCipherInitializationError,
		SymmetricCipherGenerateKeyError,
		SymmetricCipherIVError,
		GenerateRandomError,
		NullValue,
		SizeEmpty,
		SecretBufferEmpty,

		InputFileNotFound,
		InputFilePathEmpty,
		InputFileEmpty,
		InputReadAccessError,
		InputWriteAccessError,
		InputReadingBreak,

		HashedFileNotFound,
		HashedFilePathEmpty,
		HashedFileEmpty,
		HashedReadAccessError,
		HashedWriteAccessError,

		CipheredFileNotFound,
		CipheredFilePathEmpty,
		CipheredFileEmpty,
		CipheredFileNotRecognized,
		CipheredReadAccessError,
		CipheredReadingBreak,
		CipheredWriteAccessError,
		CipheredWritingBreak,

		UncipheredFileNotFound,
		UncipheredFilePathEmpty,
		UncipheredFileEmpty,
		UncipheredReadAccessError,
		UncipheredWriteAccessError,

		SecretFileNotFound,
		SecretFilePathEmpty,
		SecretFileEmpty,
		SecretReadAccessError,
		SecretWriteAccessError,

		RandomFileNotFound,
		RandomFilePathEmpty,
		RandomFileEmpty,
		RandomReadAccessError,
		RandomWriteAccessError,

		PrivateKeyFileNotFound,
		PrivateKeyFilePathEmpty,
		PrivateKeyFileEmpty,
		PrivateKeyReadAccessError,
		PrivateKeyWriteAccessError,

		PublicKeyFileNotFound,
		PublicKeyFilePathEmpty,
		PublicKeyFileEmpty,
		PublicKeyReadAccessError,
		PublicKeyWriteAccessError,

		SignatureFileNotFound,
		SignatureFilePathEmpty,
		SignatureFileEmpty,
		SignatureReadAccessError,
		SignatureWriteAccessError,
		SignatureReadingBreak,

		EncodedFileNotFound,
		EncodedFilePathEmpty,
		EncodedFileEmpty,
		EncodedReadAccessError,
		EncodedWriteAccessError,

		ConfigNotRecognized,

		UtilLibNotMapped,
	};

	using ReturnTraits = GenericReturnTraits<Return, Return::OK, Return::Break>;

	typedef std::map<IO::Return, Return> UtilReturnsMap;
	typedef std::map<FileType, UtilReturnsMap> FileTypedUtilReturnsMap;

	struct ReturnUtil
	{
		static Return Convert(const FileType fileType, IO::Return uRet);
	private:
		static FileTypedUtilReturnsMap _fileTypedUtilReturnsMap;
	};

	//Configuration
	struct GlobalConfig
	{
		std::string Version = APP_PRODUCTVERSION_STR;
		ulonglong PageSize = 16ull * 1024ull;
		ushort IdleTimeout = 0;
		ushort PagesPerTime = 0;
		//Don't change bellow or will break compatibility!!!
		const ushort IvSize = 16;
		const ushort NonceSize = 12;
		const Buffer MasterSecret = { 0xa8, 0xb8, 0xd1, 0x5c, 0x33, 0x1a, 0x3c, 0x36, 0x93, 0x4, 0xd1, 0x4a, 0x9d, 0x42, 0x4a, 0x64, 0xaa, 0x5b, 0xcd, 0xdd, 0xb, 0xaa, 0xea, 0x91, 0xae, 0x30, 0x4, 0x83, 0xad, 0x29, 0x96, 0x5f };
	};

	struct AuthCipherWorkerData
	{
		Buffer Nonce;
		Buffer Iv;
		Buffer Tag;
	};

	typedef std::vector<AuthCipherWorkerData> AuthCipherWorkerDatas;

	class AuthCipherData
	{
	public:
		AuthCipherData(const Buffer& s, const ulonglong& p = 0ull, const ushort totalWorkers = 0, const Crypto::Encoding& e = Crypto::Encoding::Bin) : Secret(s), WorkerDatas(totalWorkers), PageSize(p), Encoding(e) {}
		 //Properties
		Buffer Secret;
		ulonglong PageSize;
		Crypto::Encoding Encoding;
		AuthCipherWorkerDatas WorkerDatas;
		//Auxilliary
		const Crypto::HasherAlgorithm GetHashAlgorithm() const;
		const Crypto::AuthSymmetricChainingMode GetAcm() const;
		static void GetSaltedSecret(const Crypto::HasherAlgorithm& hashAlg, const Buffer& secret, const Buffer& salt, Buffer& saltedSecret);
		static const Crypto::HasherAlgorithm GetHashAlgorithm(const byte& b);
	};

	struct IConfig
	{
		virtual GlobalConfig& GetGlobalConfig() = 0;
		//Operations
		virtual void LoadConfig() = 0;
		virtual void SaveConfig() const = 0;
		//Secret
		virtual const bool IsSecretSaved() const = 0;
		virtual const bool& IsShowProgress() const = 0;
		virtual void ClearSecret() = 0;
		virtual const bool GetSecret(Buffer& secret) const = 0;
		virtual void SetSecret(const Buffer& secret) = 0;
		//Get's and Set's
		virtual void SetConfig(const Crypto::Encoding& e, const bool& pp) = 0;
		virtual const Crypto::Encoding& GetEncoding() const = 0;
		virtual void SetEncoding(const Crypto::Encoding& e) = 0;
		virtual const bool& IsPretyPrint() const = 0;
		virtual void SetPretyPrint(const bool& pp) = 0;
		virtual void SetShowProgress(bool v) = 0;
		//Resources
		virtual unsigned GetIconId(unsigned index) = 0;
		virtual uint GetIconIdsSize() const = 0;
		virtual HICON GetIcon(unsigned id) = 0;
		virtual uint GetIconsSize() const = 0;
		virtual HBITMAP GetBitmap(unsigned id) = 0;
		virtual uint GetBitmapsSize() const = 0;
	};

	Return InitializeConfig(HINSTANCE instance);
	IConfig* GetConfig();
	Return FinalizeConfig();

	enum class StatusOperationType
	{
		Encoding,
		Hashing,
		Entropy,
		Ciphering,
		Unciphering,
		Signing,
		Reading,
		Writing,
		GeneratingRandom,
		VerifyingSignature,
	};

	const char* GetOperationType(const FileUtilsEngine::StatusOperationType& operationType);

	typedef std::function<bool(const StatusOperationType& operationType, const ulonglong partial, const ulonglong total)> StatusOperationFunc;

	class StatusOperationFunctor
	{
	private:
		const StatusOperationType _operationType;
		const StatusOperationFunc _s;
		ulonglong _read = 0ull;
	public:
		StatusOperationFunctor(const StatusOperationType& operationType, const StatusOperationFunc& s) : _operationType(operationType), _s(s) {}
		
		bool operator()(const ulonglong partial, const ulonglong total)
		{
			_read += partial;
			if (_s)
				return _s(_operationType, _read, total);

			return false;
		}
	};

	typedef std::map<std::string, std::string> Attributtes;
	struct Node
	{
		std::string name;
		std::string value;
		Attributtes attrs;
		std::list<Node> nodes;
	};

	//ICrypto
	Return OpenAsymmetricCipher(Crypto::IAsymmetricCipher** s);
	Return OpenAuthSymmetricCipher(Crypto::IAuthSymmetricCipher** s, Crypto::IAuthInfo** a, const Crypto::AuthSymmetricChainingMode& acm, const Buffer& secret, const Buffer& nonce, uint& blockSize, uint& tagSize);
	Return GetSymmetricCipherBlockSize(const Crypto::SymmetricChainingMode& scm, ulong& blockSize);
	Return GetAuthSymmetricCipherBlockSize(const Crypto::AuthSymmetricChainingMode& acm, ulong& blockSize, ulong& tagSize);
	Return OpenSymmetricCipher(Crypto::ISymmetricCipher** s, const Crypto::SymmetricChainingMode& scm, const Buffer& secret, ulong& blockSize);
	Return OpenHasher(Crypto::IHasher** h, Crypto::HasherAlgorithm alg);
	Return OpenSigner(Crypto::ISigner** s);
	Return CalculateHashFromFile(const std::string& filePath, Crypto::HasherAlgorithm alg, Buffer& hash, const StatusOperationFunc& s = StatusOperationFunc());
	//FileHandler
	Return OpenFile(const FileType fileType, IO::FileHandler& f, const std::string& filePath, const IO::FileHandler::AccessMode accessMode);
	Return ReadDataFromFile(const FileType fileType, const std::string& filePath, Buffer& data);
	Return ReadDataFromFile(const FileType fileType, const std::string& filePath, const IO::FuncReadIterateFile& fRead);
	Return WriteDataOnFile(const FileType fileType, const std::string& filePath, const Buffer& data);
	Return WriteDataOnFile(const FileType fileType, const std::string& filePath, const ulong bytesToWrite, IO::FuncWriteIterateFile fWrite);
	//Messages
	const char* GetReturnCodeMessge(const Return& c);
	//Utils
	const ushort GetTotalThreads();
	const std::string GetAppName();
	const std::string GetAppDescription();
	const bool GetSecretOrOpenDialog(HINSTANCE instance, const bool retype, Buffer& secret);
	const bool OpenConfigDialog(HINSTANCE instance);

	struct Converter
	{
		virtual Return Execute(const Buffer& in, Buffer& ou, const bool& lastBlock) = 0;
	};
	typedef std::list<std::reference_wrapper<Converter>> Converters;

	class DataConverter
	{
	private:
		Converters _converters;

	public:
		DataConverter(Converters&& converters = Converters()) : _converters(std::move(converters)) {}

	protected:
		void SetConverters(Converters&& converters = Converters()) { _converters = std::move(converters); }

		Return ConvertData(const Buffer& in, Buffer& ou, const bool& lastBlock)
		{
			Buffer lin(in.begin(), in.end());
			for (auto& converter : _converters)
			{
				Buffer lou;
				auto r = converter.get().Execute(lin, lou, lastBlock);
				if (ReturnTraits::IsFail(r))
					return r;
				lin = std::move(lou);
				if (lin.empty())
					break;
			}
			ou = std::move(lin);

			return Return::OK;
		}
	};

	//Readers
	struct ReadDataFunctor
	{
		virtual bool IsOpened() = 0;
		virtual bool IsEmpty() = 0;
		virtual Return Read(Buffer& data, bool& lastBlock) = 0;
		virtual const ulonglong GetTotalBytes() const = 0;
		virtual Return GetTotalBytes(ulonglong& totalBytes) const = 0;
		virtual Return SetOffset(ulonglong offset) = 0;
		virtual Return SetEndOfData(ulonglong endOfData) = 0;
	};

	class DataReader : public DataConverter
	{
	private:
		Buffer _data;
		ReadDataFunctor& _rdf;
		bool _lastBlock = false;

	public:
		DataReader(ReadDataFunctor& rdf, Converters&& converters = Converters()) : DataConverter(std::move(converters)), _rdf(rdf) {}

		DataReader(DataReader&& d) : _data(std::move(d._data)), _rdf(std::move(d._rdf)) {}

		void SetConverters(Converters&& converters) { DataConverter::SetConverters(std::move(converters)); }

		bool IsOpened() { return _rdf.IsOpened(); }

		bool IsEmpty() { return _rdf.IsEmpty(); }

		Return Read(Buffer& data, bool& lastBlock)
		{
			auto desiredSize = (ulong)data.size();
			while (!_lastBlock && _data.size() < desiredSize)
			{
				//Reading data from inner reader
				Buffer in(desiredSize), ou;
				auto r = _rdf.Read(in, _lastBlock);
				if (ReturnTraits::IsFail(r))
					return r;
				//Convert data
				r = DataConverter::ConvertData(in, ou, _lastBlock);
				if (ReturnTraits::IsFail(r))
					return r;
				//Moving to buffer
				std::move(ou.begin(), ou.end(), std::back_inserter(_data));
			}
			//Sending data
			if (_data.size() > desiredSize)
			{
				data.assign(_data.begin(), _data.begin() + desiredSize);
				_data.erase(_data.begin(), _data.begin() + desiredSize);
			}
			else if (_data.size() == desiredSize || _lastBlock)
			{
				data = std::move(_data);
				lastBlock = _lastBlock;
			}

			return Return::OK;
		}
	};

	//Writers
	struct WriteDataFunctor
	{
		virtual const ulonglong GetTotalBytesWritten() const = 0;
		virtual Return GetTotalBytesWritten(ulonglong& totalBytesWritten) const = 0;
		virtual Return Write(const Buffer& data, const bool& lastBlock) = 0;
	};

	class DataWriter : public DataConverter
	{
	private:
		WriteDataFunctor& _wdf;

	public:
		DataWriter(WriteDataFunctor& wdf, Converters&& converters = Converters()) : DataConverter(std::move(converters)), _wdf(wdf) {}

		void SetConverters(Converters&& converters) { DataConverter::SetConverters(std::move(converters)); }

		Return Write(const Buffer& data, const bool& lastBlock)
		{
			if (data.empty())
				return Return::OK;
			//Converting data
			Buffer ou;
			auto r = DataConverter::ConvertData(data, ou, lastBlock);
			if (ReturnTraits::IsFail(r))
				return r;
			//Writing
			if (ou.empty())
				return Return::OK;

			return _wdf.Write(ou, lastBlock);
		}
	};

	//Buffer
	class ReadDataBufferFunctor : public ReadDataFunctor
	{
	private:
		ulonglong _deltaSize = 0ul;
		ulonglong _index = 0ul;
		const Buffer& _buffer;
		//Auxilliary
		const ulonglong GetSize() const
		{
			return _buffer.size() - _deltaSize;
		}
	public:
		ReadDataBufferFunctor(const Buffer& b) : _buffer(b) {}
		//Operations
		virtual bool IsOpened() override
		{
			return true;
		}

		virtual bool IsEmpty() override
		{
			return 0ul == GetSize();
		}

		virtual Return Read(Buffer& data, bool& lastBlock) override
		{
			auto size = std::min<ulonglong>(data.size(), GetSize() - _index);
			data.assign(_buffer.begin() + _index, _buffer.begin() + _index + size);
			_index += size;
			lastBlock = GetSize() == _index;

			return Return::OK;
		}

		virtual const ulonglong GetTotalBytes() const override
		{
			return GetSize();
		}

		virtual Return GetTotalBytes(ulonglong& totalBytes) const override
		{
			totalBytes = GetSize();

			return Return::OK;
		}

		virtual Return SetOffset(ulonglong offset) override
		{
			_index = std::min<ulonglong>(offset, GetSize());

			return Return::OK;
		}

		virtual Return SetEndOfData(ulonglong endOfData) override
		{
			_deltaSize = _buffer.size() - endOfData;
			_index = 0;

			return Return::OK;
		}

	};

	class WriteDataBufferFunctor : public WriteDataFunctor
	{
	private:
		Buffer& _buffer;
	public:
		WriteDataBufferFunctor(Buffer& b) : _buffer(b) {}
		//Operations
		virtual const ulonglong GetTotalBytesWritten() const override
		{
			return _buffer.size();
		}

		virtual Return GetTotalBytesWritten(ulonglong& totalBytesWritten) const override
		{
			totalBytesWritten = _buffer.size();

			return Return::OK;
		}

		virtual Return Write(const Buffer& data, const bool&) override
		{
			std::move(data.begin(), data.end(), std::back_inserter(_buffer));

			return Return::OK;
		}
	};

	class ReadDataFileFunctor : public ReadDataFunctor
	{
	private:
		IO::FileHandler _f;
		const FileType& _fileType;
		ulonglong _dataRead = 0ull;
		ulonglong _delta = 0ul;
		//Auxilliary
		const ulonglong GetSize() const
		{
			return _f.GetFileSize() - _delta;
		}
	public:
		ReadDataFileFunctor(const FileType& fileType, const std::string& filePath) : _fileType(fileType)
		{
			_f.Open(filePath, IO::FileHandler::AccessMode::Read);
		}

		virtual bool IsOpened() override
		{
			return IO::ReturnTraits::IsSuccess(_f.IsOpened());
		}

		virtual bool IsEmpty() override
		{
			return 0ul == GetSize();
		}

		virtual Return Read(Buffer& data, bool& lastBlock) override
		{
			auto dataRead = 0ull;
			Buffer l(std::min<ulonglong>(data.size(), GetSize() - _dataRead));
			auto r = ReturnUtil::Convert(_fileType, _f.ReadData(l, dataRead));
			if (ReturnTraits::IsFail(r))
				return r;
			_dataRead += dataRead;
			lastBlock = _dataRead == GetSize();
			data = std::move(l);

			return r;
		}

		virtual const ulonglong GetTotalBytes() const override
		{
			return GetSize();
		}

		virtual Return GetTotalBytes(ulonglong& totalBytes) const override
		{
			totalBytes = GetSize();

			return Return::OK;
		}

		virtual Return SetOffset(ulonglong offset) override
		{
			return ReturnUtil::Convert(_fileType, _f.SetOffset(std::min<ulonglong>(offset, GetSize()), IO::FileHandler::OffsetPoint::Begin));
		}

		virtual Return SetEndOfData(ulonglong endOfData) override
		{
			_dataRead = 0ull;
			_delta = _f.GetFileSize() - endOfData;
			auto r = ReturnUtil::Convert(_fileType, _f.Rewind());
			if (ReturnTraits::IsFail(r))
				return r;

			return Return::OK;
		}
	};

	class WriteDataFileFunctor : public WriteDataFunctor
	{
	private:
		IO::FileHandler _f;
		const FileType& _fileType;
		ulonglong _totalBytesWritten = 0ull;

	public:
		WriteDataFileFunctor(const FileType& fileType, const std::string& filePath) : _fileType(fileType)
		{
			_f.Open(filePath, IO::FileHandler::AccessMode::Write);
		}

		WriteDataFileFunctor(WriteDataFileFunctor&& f) : _f(std::move(f._f)), _fileType(std::move(f._fileType)), _totalBytesWritten(std::move(f._totalBytesWritten)) {}

		virtual const ulonglong GetTotalBytesWritten() const override
		{
			return _totalBytesWritten;
		}

		virtual Return GetTotalBytesWritten(ulonglong& totalBytesWritten) const override
		{
			totalBytesWritten = _totalBytesWritten;

			return Return::OK;
		}

		virtual Return Write(const Buffer& data, const bool& lastBlock) override
		{
			auto dataWritten = 0ull;
			auto r = ReturnUtil::Convert(_fileType, _f.WriteData(data, dataWritten));
			if (ReturnTraits::IsFail(r))
				return r;
			_totalBytesWritten += dataWritten;

			return Return::OK;
		}
	};

	//Converters
	class ConverterBase64Encoder : public Converter
	{
	private:
		WRL::ComPtr<Crypto::IStreamBase64Encoder> _encoder;

	public:
		ConverterBase64Encoder()
		{
			Crypto::CreateStreamBase64Encoder(_encoder.GetAddressOf());
		}

		virtual Return Execute(const Buffer& in, Buffer& ou, const bool& lastBlock) override
		{
			std::string o;
			auto r = _encoder->EncodeData(in, lastBlock, o);
			if (Crypto::ReturnTraits::IsFail(r))
				return Return::Base64EncodeError;
			std::move(o.begin(), o.end(), std::back_inserter(ou));

			return Return::OK;
		}
	};

	class ConverterBase64Decoder : public Converter
	{
	private:
		WRL::ComPtr<Crypto::IStreamBase64Decoder> _decoder;
	public:
		ConverterBase64Decoder()
		{
			Crypto::CreateStreamBase64Decoder(_decoder.GetAddressOf());
		}

		virtual Return Execute(const Buffer& in, Buffer& ou, const bool& lastBlock) override
		{
			auto r = _decoder->DecodeData(std::string(in.begin(), in.end()), ou);
			if (Crypto::ReturnTraits::IsFail(r))
				return Return::Base64DecodeError;

			return Return::OK;
		}
	};

	class ConverterHexEncoder : public Converter
	{
	public:
		ConverterHexEncoder() {}

		virtual Return Execute(const Buffer& in, Buffer& ou, const bool& lastBlock) override
		{
			std::string o;
			auto r = Crypto::HexEncode(in, o);
			if (Crypto::ReturnTraits::IsFail(r))
				return Return::HexEncodeError;
			std::move(o.begin(), o.end(), std::back_inserter(ou));

			return Return::OK;
		}
	};

	class ConverterHexDecoder : public Converter
	{
	public:
		ConverterHexDecoder() {}

		virtual Return Execute(const Buffer& in, Buffer& ou, const bool& lastBlock) override
		{
			Buffer o;
			auto r = Crypto::HexDecode({ in.begin(), in.end() }, o);
			if (Crypto::ReturnTraits::IsFail(r))
				return Return::HexDecodeError;
			std::move(o.begin(), o.end(), std::back_inserter(ou));

			return Return::OK;
		}
	};

	class ConverterInflator : public Converter
	{
	private:
		Zip::Inflator _inflator;

	public:
		ConverterInflator() {}

		virtual Return Execute(const Buffer& in, Buffer& ou, const bool& lastBlock) override
		{
			if (Zip::ReturnTraits::IsFail(_inflator.Inflate(in, ou)))
				return Return::InflatorError;

			return Return::OK;
		}
	};

	class ConverterDeflator : public Converter
	{
	private:
		Zip::Deflator _deflator;

	public:
		ConverterDeflator() {}

		virtual Return Execute(const Buffer& in, Buffer& ou, const bool& lastBlock) override
		{
			if (Zip::ReturnTraits::IsFail(_deflator.Deflate(in, ou, lastBlock)))
				return Return::DeflatorError;

			return Return::OK;
		}
	};

	static Crypto::HasherAlgorithm GetHaslAlg(const Buffer& b)
	{
		return (Crypto::HasherAlgorithm)(b[0] % 7);
	}

	typedef std::function<Crypto::HasherAlgorithm()> GetHashAlgFunc;
	class ConverterHashGenerator : public Converter
	{
	private:
		WRL::ComPtr<Crypto::IHasher> _hasher;
		bool _initialized = false;

	public:
		ConverterHashGenerator() {}

		virtual Return Execute(const Buffer& in, Buffer& ou, const bool& lastBlock) override
		{
			if (!_initialized)
			{
				if (S_OK != Crypto::CreateHasher(_hasher.GetAddressOf()))
					return Return::HasherInitializationError;
				if (Crypto::ReturnTraits::IsFail(_hasher->Initialize(GetHaslAlg(in))))
					return Return::HasherInitializationError;
				_initialized = true;
			}

			ou = std::move(in);
			auto r = _hasher->HashData(ou);
			if (Crypto::ReturnTraits::IsFail(r))
				return Return::HashDataError;
			if (lastBlock)
			{
				Buffer hash;
				auto r = _hasher->Calculate(hash);
				if (Crypto::ReturnTraits::IsFail(r))
					return Return::HashCalculateError;
				std::move(hash.begin(), hash.end(), std::back_inserter(ou));
			}

			return Return::OK;
		}
	};

	class ConverterHashChecker : public Converter
	{
	private:
		WRL::ComPtr<Crypto::IHasher> _hasher;
		ushort _hashSize = 0u;
		Buffer _lastBlock;
		bool _initialized = false;

	public:
		ConverterHashChecker() {}

		virtual Return Execute(const Buffer& in, Buffer& ou, const bool& lastBlock) override
		{
			//Initializing
			if (!_initialized)
			{
				if (S_OK != Crypto::CreateHasher(_hasher.GetAddressOf()) || Crypto::ReturnTraits::IsFail(_hasher->Initialize(GetHaslAlg(in))) || Crypto::ReturnTraits::IsFail(_hasher->GetHashSize(_hashSize)))
					return Return::HasherInitializationError;
				_initialized = true;
			}
			_lastBlock.insert(_lastBlock.end(), in.begin(), in.end());
			if (_lastBlock.size() > _hashSize)
			{
				auto diff = _lastBlock.size() - _hashSize;
				std::move(_lastBlock.begin(), _lastBlock.begin() + diff, std::back_inserter(ou));
				_lastBlock.erase(_lastBlock.begin(), _lastBlock.begin() + diff);
				//Hashing
				auto r = _hasher->HashData(ou);
				if (Crypto::ReturnTraits::IsFail(r))
					return Return::HashDataError;
				if (lastBlock)
				{
					Buffer hash;
					auto r = _hasher->Calculate(hash);
					if (Crypto::ReturnTraits::IsFail(r) || hash != _lastBlock)
						return Return::CipheredFileNotRecognized;
				}
			}
			else if (lastBlock && _lastBlock.size() < _hashSize)
				return Return::HashDataError;

			return Return::OK;
		}
	};

	typedef std::map<std::string, Buffer> Hashes;

	class Envelopper
	{
	private:
		//Auxilliary
		static Return Generate(tinyxml2::XMLDocument& doc, tinyxml2::XMLElement* father, const Node& node);
		static Return Generate(const bool& prettyPrint, std::string& envelop, const Node& node);
		static Return Parse(tinyxml2::XMLElement* father, Node& node);
		static Return Parse(const std::string& envelop, Node& node);
	public:
		//Symmetric cipher
		static Return GenerateSymmetricCipher(const AuthCipherData& acd, std::string& envelop);
		static Return ParseSymmetricCipher(const std::string& envelop, std::string& version, AuthCipherData& acd);
		//Config
		static Return GenerateConfig(const IConfig* cfg, std::string& envelop);
		static Return ParseConfig(const std::string& envelop, std::string& version, IConfig* cfg);
		//Hashes
		static Return GenerateHashes(const Hashes& hh, const IConfig* cfg, const std::string& file, std::string& envelop);
	};
}