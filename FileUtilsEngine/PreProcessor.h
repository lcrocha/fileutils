#pragma once
#include "FileUtilsEngine.h"
#include "../../libs/Helper/Concurrent/Consumer.h"

namespace FileUtilsEngine
{
	class PreProcessorAuthCipher
	{
	private:
		DataReader& _reader;
		const ProcessorOperation& _operation;
		const ulonglong _pageSize = 1024ull;
		bool _initialized = false;
	public:
		PreProcessorAuthCipher(const ProcessorOperation& operation, const ulonglong& pageSize, DataReader& reader);
		virtual ~PreProcessorAuthCipher();
		//Operation
		Return operator()(Concurrent::PreProcessorData<InputData>& data);
	};
}