#pragma once
#include "../FileUtilsEngine/FileUtilsEngine.h"
#include "../../libs/Helper/Windows/NotifyIcon.h"
#include "../FileUtilsEngine/resource.h"
#include <tuple>

struct EngineControl
{
	virtual void SetInput(Command&& cmd) = 0;
};

enum class EngineStatus { None, Idle, Working, SecretNotSaved, Config, About };

class Engine : public Windows::NotifyIcon, public EngineControl, public CommandProcessorEvents
{
private:
	CommandChannel _channel;
	//Status
	std::tuple<EngineStatus, bool> _status;
	//Auxlliary
	void DoConfig();
	void DoAbout();
	void DoExit();
	void DetectStatus();
	void UpdateStatus();
	static bool IsReportableCommand(const CommandType cmd);
public:
	Engine(HINSTANCE instance);
	virtual ~Engine();
	//Life cycle
	virtual bool Initialize();
	virtual bool Finalize();
	//Status
	bool IsIdle();
	//EngineControl
	virtual void SetInput(Command&& cmd) override;
	//Events NotifyIcon
	virtual void OnLeftClick() override;
	virtual void OnLeftDblClick() override;
	virtual void OnRightClick() override;
	virtual void OnBalloonUserClick() override;
	//Events CommandProcessor events
	virtual void OnBatchEnded(const CommandType cmd, const ulong totalFiles, const ulong elapsed) override;
	virtual void OnBatchError(const CommandType cmd, const Filepath& file) override;
	virtual void OnBatchBreak(const CommandType cmd, const Filepath& file) override;
	virtual void OnTimer(const unsigned id) override;
	virtual void OnCommand(WPARAM wParam, LPARAM lParam) override;
};

