#include "stdafx.h"
#include <strsafe.h>
#include <future>
#include <sstream>
#include <iomanip>
#include "ShellExtension.h"
#include "../FileUtilsEngine/resource.h"
#include "../FileUtilsEngine/FileUtilsEngine.h"
#include "../../Libs/Helper/Windows/GDIUtils.h"

extern unsigned s_serverLock;

ShellExtension::ShellExtension(EngineControl* ec) : _ec(ec)
{

	::_InterlockedIncrement(&s_serverLock);
	auto menuName = FileUtilsEngine::GetAppName();
	_menuName.assign(menuName.begin(), menuName.end());
}

ShellExtension::~ShellExtension()
{
	::_InterlockedDecrement(&s_serverLock);
}

HRESULT __stdcall ShellExtension::Initialize(LPCITEMIDLIST pidlFolder, LPDATAOBJECT pDataObj, HKEY hKeyProgID)
{
	//Validating parameters
	if (NULL == pDataObj)
		return E_INVALIDARG;
	//Getting file
	HRESULT hr = E_FAIL;
	FORMATETC fe = { CF_HDROP, NULL, DVASPECT_CONTENT, -1, TYMED_HGLOBAL };
	STGMEDIUM stm;
	if (FAILED(pDataObj->GetData(&fe, &stm)))
		return E_FAIL;
	HDROP hDrop = static_cast<HDROP>(::GlobalLock(stm.hGlobal));
	if (hDrop != NULL)
	{
		auto files = ::DragQueryFile(hDrop, 0xFFFFFFFF, NULL, 0);
		for (auto i = 0u; i < files; ++i)
		{
			auto size = ::DragQueryFile(hDrop, i, nullptr, 0);
			if (size == 0)
				continue;
			std::wstring file;
			file.resize(size + 1);
			::DragQueryFile(hDrop, i, &file[0], (UINT)file.size());
			file.resize(size);
			_selectedFiles.emplace_back(file.begin(), file.end());
		}
		hr = S_OK;
		::GlobalUnlock(stm.hGlobal);
	}
	::ReleaseStgMedium(&stm);

	return hr;
}

HRESULT __stdcall ShellExtension::QueryContextMenu(HMENU hMenu, UINT indexMenu, UINT idCmdFirst, UINT idCmdLast, UINT uFlags)
{
	//Validating parameters
	if (CMF_DEFAULTONLY & uFlags)
		return MAKE_HRESULT(SEVERITY_SUCCESS, 0, USHORT(0));
	// Add a separator
	MENUITEMINFO sep = { sizeof(sep) };
	sep.fMask = MIIM_TYPE;
	sep.fType = MFT_SEPARATOR;
	if (!::InsertMenuItem(hMenu, indexMenu + 0, TRUE, &sep))
		return HRESULT_FROM_WIN32(GetLastError());
	//Creating submenus
	HMENU hSubmenu = ::CreatePopupMenu();
	UINT uID = idCmdFirst;
	auto index = 0u;
	auto cfg = FileUtilsEngine::GetConfig();
	Windows::GDIUtils::AddMenuItem(hSubmenu, index++, uID++, cfg->GetBitmap(IDI_CIPHER), L"Cipher");
	Windows::GDIUtils::AddMenuItem(hSubmenu, index++, uID++, cfg->GetBitmap(IDI_UNCIPHER), L"Uncipher");
	::InsertMenuItem(hSubmenu, index++, TRUE, &sep);
	Windows::GDIUtils::AddMenuItem(hSubmenu, index++, uID++, cfg->GetBitmap(IDI_LOCK), L"Lock file");
	Windows::GDIUtils::AddMenuItem(hSubmenu, index++, uID++, cfg->GetBitmap(IDI_LOCKERS), L"Get Lockers");
	::InsertMenuItem(hSubmenu, index++, TRUE, &sep);
	Windows::GDIUtils::AddMenuItem(hSubmenu, index++, uID++, cfg->GetBitmap(IDI_HASH), L"Hash");
	Windows::GDIUtils::AddMenuItem(hSubmenu, index++, uID++, cfg->GetBitmap(IDI_ENTROPY), L"Entropy");
	//Creating main menu
	MENUITEMINFO mii = { sizeof(mii) };
	mii.fMask = MIIM_BITMAP | MIIM_STRING | MIIM_FTYPE | MIIM_ID | MIIM_STATE | MIIM_SUBMENU;
	mii.wID = uID++;
	mii.fType = MFT_STRING;
	mii.dwTypeData = (LPWSTR)_menuName.c_str();
	mii.fState = MFS_ENABLED;
	mii.hbmpItem = static_cast<HBITMAP>(cfg->GetBitmap(IDI_TOOL));
	mii.hSubMenu = hSubmenu;
	if (!::InsertMenuItem(hMenu, indexMenu + 1, TRUE, &mii))
		return HRESULT_FROM_WIN32(GetLastError());
	// Add a separator
	if (!::InsertMenuItem(hMenu, indexMenu + 2, TRUE, &sep))
		return HRESULT_FROM_WIN32(GetLastError());

	return MAKE_HRESULT(SEVERITY_SUCCESS, FACILITY_NULL, uID - idCmdFirst);
}

HRESULT __stdcall ShellExtension::InvokeCommand(LPCMINVOKECOMMANDINFO pici)
{
	if (_selectedFiles.empty())
		return E_INVALIDARG;
	auto verb = LOWORD(pici->lpVerb);
	switch (verb)
	{
		break;
	case 0:
		_ec->SetInput({ CommandType::Cipher, _selectedFiles });
		break;
	case 1:
		_ec->SetInput({ CommandType::Uncipher, _selectedFiles });
		break;
	case 2:
		_ec->SetInput({ CommandType::Lock, _selectedFiles });
		break;
	case 3:
		_ec->SetInput({ CommandType::GetLockers, _selectedFiles });
		break;
	case 4:
		_ec->SetInput({ CommandType::Hash, _selectedFiles });
		break;
	case 5:
		_ec->SetInput({ CommandType::Entropy, _selectedFiles });
		break;
	default:
		return E_INVALIDARG;
		break;
	}

	return S_OK;
}

HRESULT __stdcall ShellExtension::GetCommandString(UINT_PTR idCommand, UINT uFlags, UINT *pwReserved, LPSTR pszName, UINT cchMax)
{
	HRESULT hr = E_INVALIDARG;

	switch (uFlags)
	{
	case GCS_HELPTEXTW:
		hr = StringCchCopy(reinterpret_cast<PWSTR>(pszName), cchMax, L"FileUtils helper");
		break;
	case GCS_VERBW:
		hr = StringCchCopy(reinterpret_cast<PWSTR>(pszName), cchMax, L"FileUtils Verb");
		break;

	default:
		hr = S_OK;
	}

	return hr;
}
