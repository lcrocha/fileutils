#include "stdafx.h"
#include "Engine.h"
#include "../FileUtilsEngine/resource.h"
#include "../FileUtilsEngine/DlgAbout.h"
#include "../../Libs/Helper/Windows/GDIUtils.h"
#include <sstream>

#define ID_CONFIG	1000
#define ID_ABOUT	1001
#define ID_EXIT		1002

#define ID_TIMER_1	10001
#define ID_TIMER_2	10002

Engine::Engine(HINSTANCE instance) : Windows::NotifyIcon(instance, "FileUtils")
{
	_status = { EngineStatus::None, false };
}

Engine::~Engine()
{
	Finalize();
}

bool Engine::Initialize()
{
	_channel.CreateProcessor(this).StartAsync();
	//Setting tip
	std::ostringstream oss;
	oss << FileUtilsEngine::GetAppName() << std::endl
		<< FileUtilsEngine::GetAppDescription() << std::endl
		<< APP_CONTACT_STR;
	//Initializing
	if (!Windows::NotifyIcon::Initialize(IDI_TOOL, oss.str().c_str()))
		return false;
	//Setting timer
	Windows::NotifyIcon::SetTimer(ID_TIMER_1, 500);

	return true;
}

bool Engine::Finalize()
{
	Windows::NotifyIcon::SetTimer(ID_TIMER_1);
	_channel.GetControl().Stop();
	_channel.GetControl().Wait();

	return Windows::NotifyIcon::Finalize();
}

bool Engine::IsIdle()
{
	return EngineStatus::Idle == std::get<0>(_status);
}

void Engine::SetInput(Command&& cmd)
{
	switch (std::get<0>(_status))
	{
	case EngineStatus::SecretNotSaved:
		::MessageBoxA(NotifyIcon::GetHwnd(), "Secret is not Saved!", FileUtilsEngine::GetAppName().c_str(), MB_ICONEXCLAMATION);
		DoConfig();
		return;
	case EngineStatus::Config:
		return;
	}
	_channel.GetControl().SetInput(std::move(cmd));
}

void Engine::OnLeftClick()
{
	if (EngineStatus::Working == std::get<0>(_status))
	{
		auto cfg = FileUtilsEngine::GetConfig();
		cfg->SetShowProgress(!cfg->IsShowProgress());
		cfg->SaveConfig();
	}
}

void Engine::DoConfig()
{
	auto s0 = std::get<0>(_status);
	if (EngineStatus::Idle == s0 || EngineStatus::SecretNotSaved == s0)
	{
		_status = { EngineStatus::Config, false };
		FileUtilsEngine::OpenConfigDialog(s_hInstance);
		_status = { EngineStatus::Config, true };
	}
}

void Engine::DoAbout()
{
	_status = { EngineStatus::About, false };
	FileUtilsEngine::DlgAbout(s_hInstance, IDD_ABOUT, IDI_ABOUT).DoModal();
	_status = { EngineStatus::About, true };
}

void Engine::DoExit()
{

}

void Engine::OnLeftDblClick()
{
	DoConfig();
}

void Engine::OnRightClick()
{
	if (EngineStatus::Idle != std::get<0>(_status) && EngineStatus::SecretNotSaved != std::get<0>(_status))
		return;
	//Setting separotor
	MENUITEMINFO sep = { sizeof(sep) };
	sep.fMask = MIIM_TYPE;
	sep.fType = MFT_SEPARATOR;
	//Creating menu
	HMENU menu = ::CreatePopupMenu();
	Windows::GDIUtils::AddMenuItem(menu, 0, ID_CONFIG, nullptr, L"&Config...", _channel.IsIdle());
	Windows::GDIUtils::AddMenuItem(menu, 1, ID_ABOUT, nullptr, L"&About...");
	::InsertMenuItem(menu, 2, TRUE, &sep);
	Windows::GDIUtils::AddMenuItem(menu, 3, ID_EXIT, nullptr, L"&Exit", false);
	//Setting menu
	::SetMenuDefaultItem(menu, ID_CONFIG, FALSE);
	::SetFocus(NotifyIcon::GetHwnd());
	::SendMessage(NotifyIcon::GetHwnd(), WM_INITMENUPOPUP, (WPARAM)menu, 0);
	//Show menu
	POINT pt = { 0 };
	::GetCursorPos(&pt);
	auto cmd = ::TrackPopupMenu(menu, TPM_LEFTALIGN | TPM_RIGHTBUTTON | TPM_RETURNCMD | TPM_NONOTIFY, pt.x, pt.y, 0, NotifyIcon::GetHwnd(), NULL);
	if (0 != cmd)
		::SendMessage(NotifyIcon::GetHwnd(), WM_COMMAND, cmd, 0);
	::DestroyMenu(menu);
}

void Engine::OnBalloonUserClick()
{
	auto s0 = std::get<0>(_status);
	if (EngineStatus::SecretNotSaved == s0)
		DoConfig();
	else if (EngineStatus::Working != s0)
		DoAbout();
}

void Engine::OnCommand(WPARAM wParam, LPARAM lParam)
{
	switch (LOWORD(wParam))
	{
	case ID_CONFIG:
		DoConfig();
		break;
	case ID_ABOUT:
		DoAbout();
		break;
	case ID_EXIT:
		DoExit();
		break;
	}
}

bool Engine::IsReportableCommand(const CommandType cmd)
{
	return CommandType::Cipher == cmd || CommandType::Uncipher == cmd || CommandType::Hash == cmd || CommandType::Entropy == cmd;
}

void Engine::OnBatchEnded(const CommandType cmd, const ulong totalFiles, const ulong elapsed)
{
	if (!IsReportableCommand(cmd))
		return;
	std::ostringstream oss;
	oss << "Total files processed: " << totalFiles << " [" << elapsed << "ms]";
	Windows::NotifyIcon::ShowInfo(Windows::NotifyInfoType::Info, oss.str().c_str(), FileUtilsEngine::GetAppName().c_str());
}

void Engine::OnBatchError(const CommandType cmd, const Filepath& file)
{
	if (!IsReportableCommand(cmd))
		return;
	std::ostringstream oss;
	oss << "Error processing: " << file;
	Windows::NotifyIcon::ShowInfo(Windows::NotifyInfoType::Error, oss.str().c_str(), FileUtilsEngine::GetAppName().c_str());
}

void Engine::OnBatchBreak(const CommandType cmd, const Filepath& file)
{
	if (!IsReportableCommand(cmd))
		return;
	std::ostringstream oss;
	oss << "Process break during: " << file;
	Windows::NotifyIcon::ShowInfo(Windows::NotifyInfoType::Warning, oss.str().c_str(), FileUtilsEngine::GetAppName().c_str());
}

void Engine::DetectStatus()
{
	auto& s0 = std::get<0>(_status);
	auto& s1 = std::get<1>(_status);
	//Getting status
	auto secretSaved = FileUtilsEngine::GetConfig()->IsSecretSaved();
	auto working = _channel.IsWorking();
	//Detecting status
	if ((EngineStatus::Config == s0 && !s1) || (EngineStatus::About == s0 && !s1))
	{
		return;
	}
	else if (working)
	{
		if (EngineStatus::Working == s0)
			return;
		s0 = EngineStatus::Working;
		s1 = false;
	}
	else if (!secretSaved)
	{
		if (EngineStatus::SecretNotSaved == s0)
			return;
		s0 = EngineStatus::SecretNotSaved;
		s1 = false;
	}
	else if ((EngineStatus::None == s0 && s1) || (EngineStatus::Idle != s0 && EngineStatus::None != s0))
	{
		s0 = EngineStatus::Idle;
		s1 = false;
	}
}

void Engine::UpdateStatus()
{
	auto& s0 = std::get<0>(_status);
	auto& s1 = std::get<1>(_status);
	switch (s0)
	{
	case EngineStatus::None:
		if (s1)
			return;
		//Windows::NotifyIcon::ShowInfo(Windows::NotifyInfoType::Info, "Started!", FileUtilsEngine::GetAppName().c_str());
		s1 = true;
	case EngineStatus::Idle:
		NotifyIcon::UpdateIcon(IDI_TOOL);
		s1 = true;
	break;
	case EngineStatus::Working:
	{
		static bool icon = false;
		Windows::NotifyIcon::UpdateIcon(icon ? IDI_GEAR0 : IDI_GEAR1);
		icon = !icon;
		s1 = true;
	}
	break;
	case EngineStatus::SecretNotSaved:
	{
		static bool icon = false;
		Windows::NotifyIcon::UpdateIcon(icon ? IDI_EXCLAMATION0 : IDI_EXCLAMATION1);
		icon = !icon;
		if (!s1)
		{
			Windows::NotifyIcon::ShowInfo(Windows::NotifyInfoType::Warning, "Secret not saved. Please save a secret!", FileUtilsEngine::GetAppName().c_str());
			s1 = true;
		}
	}
	break;
	case EngineStatus::Config:
		NotifyIcon::UpdateIcon(IDI_CONFIG);
		break;
	case EngineStatus::About:
		NotifyIcon::UpdateIcon(IDI_ABOUT);
		break;
	}
}

void Engine::OnTimer(const unsigned id)
{
	//Updating config
	FileUtilsEngine::GetConfig()->LoadConfig();
	//Detecting status
	DetectStatus();
	//Updating status
	UpdateStatus();
}
