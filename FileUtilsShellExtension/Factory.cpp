#include "stdafx.h"
#include "Factory.h"
#include "ShellExtension.h"

extern unsigned s_serverLock;

Factory::Factory(EngineControl* ec) : _ec(ec)
{
	::_InterlockedIncrement(&s_serverLock);
}

Factory::~Factory()
{
	::_InterlockedDecrement(&s_serverLock);
}

HRESULT __stdcall Factory::QueryInterface(IID const & id, void** result)
{
	WRL::ComPtr<Factory> c = WRL::Make<Factory>(_ec);
	if (!c)
		return E_OUTOFMEMORY;
	*result = c.Detach();

	return S_OK;
}

HRESULT __stdcall Factory::CreateInstance(IUnknown* outer, IID const & iid, void ** result)
{
	if (outer != nullptr)
		return CLASS_E_NOAGGREGATION;
	WRL::ComPtr<ShellExtension> c = WRL::Make<ShellExtension>(_ec);
	if (!c)
		return E_OUTOFMEMORY;

	return c->QueryInterface(iid, result);
}

HRESULT __stdcall Factory::LockServer(BOOL lock)
{
	if (lock)
	{
		::_InterlockedIncrement(&s_serverLock);
	}
	else
	{
		::_InterlockedDecrement(&s_serverLock);
	}

	return S_OK;
}