#pragma once
#include "../../libs/Helper/Concurrent/Channel.h"

enum class CommandType { Cipher, Uncipher, Lock, GetLockers, Hash, Entropy };

typedef std::string Filepath;
typedef std::vector<Filepath> Filepaths;

struct Command
{
	CommandType Type;
	Filepaths Files;
};

enum class CommandProcessorReturn { Ok, Error, Break };

struct CommandProcessorEvents
{
	virtual void OnBatchEnded(const CommandType cmd, const ulong totalFiles, const ulong elapsed) = 0;
	virtual void OnBatchError(const CommandType cmd, const Filepath& file) = 0;
	virtual void OnBatchBreak(const CommandType cmd, const Filepath& file) = 0;
};

class CommandProcessor
{
private:
	CommandProcessorEvents* _events = nullptr;
	//Operations
	static CommandProcessorReturn DoCipher(const Filepath& filePath, const std::string& subTitle, bool end);
	static CommandProcessorReturn DoUncipher(const Filepath& filePath, const std::string& subTitle, bool end);
	static CommandProcessorReturn DoLock(const Filepath& filePath, const std::string& subTitle, bool end);
	static CommandProcessorReturn DoGetLockers(const Filepath& filePath, const std::string& subTitle, bool end);
	static CommandProcessorReturn DoHash(const Filepath& filePath, const std::string& subTitle, bool end);
	static CommandProcessorReturn DoEntropy(const Filepath& filePath, const std::string& subTitle, bool end);
public:
	CommandProcessor(CommandProcessorEvents* events) : _events(events) {}
	virtual ~CommandProcessor() {}

	bool operator()(Concurrent::ChannelProcessorData<Command>& d);
};

typedef Concurrent::Channel<CommandProcessor, BoolTraits, Command> CommandChannel;
typedef Concurrent::ChannelControl<bool, Command> CommandControl;