#pragma once
#include "Engine.h"

class __declspec(uuid("10f7db48-f912-405b-8edd-6c41c398f983")) Factory : public WRL::RuntimeClass<WRL::RuntimeClassFlags<WRL::ClassicCom>, IClassFactory>
{
private:
	EngineControl* _ec;
public:
	Factory(EngineControl* ec);
	virtual ~Factory();
	//IClassFactory
	HRESULT __stdcall QueryInterface(IID const & id, void** result) override;
	HRESULT __stdcall CreateInstance(IUnknown* outer, IID const & iid, void ** result) override;
	HRESULT __stdcall LockServer(BOOL lock) override;
};