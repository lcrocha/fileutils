#include "stdafx.h"
#include <Guiddef.h>
#include <io.h>
#include "Registration.h"
#include "Factory.h"
#include "ShellExtension.h"
#include "../FileUtilsEngine/FileUtilsEngine.h"
#include "../../Libs/Helper/Debugger/Debugger.h"
#include "Engine.h"

HMODULE s_hInstance = nullptr;
unsigned s_serverLock = 0;
std::unique_ptr<Engine> s_Engine;

HRESULT __stdcall DllGetClassObject(CLSID const& clsid, IID const& iid, void** result)
{
	if (!IsEqualCLSID(__uuidof(ShellExtension), clsid))
		return CLASS_E_CLASSNOTAVAILABLE;

	if (!s_Engine && nullptr != s_hInstance)
	{
		s_Engine.reset(new Engine(s_hInstance));
		s_Engine->Initialize();
	}

	WRL::ComPtr<Factory> c = WRL::Make<Factory>(s_Engine.get());
	if (!c)
		return E_OUTOFMEMORY;

	return c->QueryInterface(iid, result);
}

HRESULT __stdcall DllCanUnloadNow()
{
	auto shIdle = s_serverLock == 0;
	auto enIdle = s_Engine && s_Engine->IsIdle();
	if (!shIdle || !enIdle)
		return FALSE;
	s_Engine.reset();

	return TRUE;
}

HRESULT __stdcall DllRegisterServer()
{
	HRESULT hr;

	wchar_t szModule[MAX_PATH] = { L"" };
	if (::GetModuleFileName(s_hInstance, szModule, _ARRAYSIZE(szModule)) == 0)
	{
		hr = HRESULT_FROM_WIN32(GetLastError());
		return hr;
	}

	// Register the component.
	hr = RegisterInprocServer(szModule, __uuidof(ShellExtension), L"FileUtilsShellExtContextMenuHandler.FileContextMenuExt Class", L"Apartment");
	if (SUCCEEDED(hr))
	{
		hr = RegisterShellExtContextMenuHandler(L"*", __uuidof(ShellExtension), L"FileUtilsShellExtContextMenuHandler.FileContextMenuExt");
	}

	return hr;
}

HRESULT __stdcall DllUnregisterServer()
{
	HRESULT hr = S_OK;

	wchar_t szModule[MAX_PATH];
	if (GetModuleFileName(s_hInstance, szModule, _ARRAYSIZE(szModule)) == 0)
	{
		hr = HRESULT_FROM_WIN32(GetLastError());
		return hr;
	}
	// Unregister the component.
	hr = UnregisterInprocServer(__uuidof(ShellExtension));
	if (SUCCEEDED(hr))
	{
		hr = UnregisterShellExtContextMenuHandler(L"*", __uuidof(ShellExtension));
	}

	return hr;
}

void LaunchDebugger()
{
#ifdef _DEBUG
	if (0 == ::_access("C:\\Users\\user\\Desktop\\Code\\fileutils\\debug", 0) ||
		0 == ::_access("X:\\Code\\fileutils\\debug", 0))
		Debugger::Launch();
#endif
}

BOOL __stdcall DllMain(HINSTANCE module, DWORD reason, void*)
{
	LaunchDebugger();

	if (DLL_PROCESS_ATTACH == reason)
	{
		s_hInstance = module;
		::DisableThreadLibraryCalls(module);
		FileUtilsEngine::InitializeConfig(module);
	}
	else if (DLL_PROCESS_DETACH)
	{
		s_Engine.reset();
		FileUtilsEngine::FinalizeConfig();
	}

	return TRUE;
}