#pragma once

#include <Windows.h>
#include <memory>
#include <ktmw32.h>
#include <Unknwn.h>
#pragma comment(lib, "ktmw32.lib")

#include <wrl.h>
using namespace Microsoft;

#include <atlbase.h>
#define ASSERT ATLASSERT
#define TRACE ATLTRACE
#include "CommandProcessor.h"

extern HMODULE s_hInstance;