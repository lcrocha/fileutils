#pragma once
#include <ShlObj.h>
#include <string>
#include <vector>
#include "Engine.h"

class __declspec(uuid("accefa9a-c2a7-4a5d-a3f6-07687cda59ae")) ShellExtension : public WRL::RuntimeClass<WRL::RuntimeClassFlags<WRL::ClassicCom>, IShellExtInit, IContextMenu>
{
private:
	EngineControl* _ec;
	std::vector<std::string> _selectedFiles;
	//Menu
	std::wstring _menuName;
public:
	ShellExtension(EngineControl* ec);
	virtual ~ShellExtension();
	// IShellExtInit
	HRESULT __stdcall Initialize(LPCITEMIDLIST pidlFolder, LPDATAOBJECT pDataObj, HKEY hKeyProgID) override;
	// IContextMenu
	HRESULT __stdcall QueryContextMenu(HMENU hMenu, UINT indexMenu, UINT idCmdFirst, UINT idCmdLast, UINT uFlags) override;
	HRESULT __stdcall InvokeCommand(LPCMINVOKECOMMANDINFO pici) override;
	HRESULT __stdcall GetCommandString(UINT_PTR idCommand, UINT uFlags, UINT *pwReserved, LPSTR pszName, UINT cchMax) override;
};