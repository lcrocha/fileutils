#include "stdafx.h"
#include "CommandProcessor.h"
#include "../FileUtilsEngine/ActionHasher.h"
#include "../FileUtilsEngine/ActionSymmetricCipher.h"
#include "../FileUtilsEngine/ActionLocker.h"
#include "../FileUtilsEngine/ActionEntropy.h"
#include "../../libs/Helper/Windows/DlgInput.h"
#include "../../libs/Helper/Windows/DlgProgress.h"
#include "../../libs/Helper/Time/Stopwatch.h"
#include "../FileUtilsEngine/resource.h"
#include <sstream>
#include <iomanip>

typedef std::function<CommandProcessorReturn(const Filepath&, const std::string& subTitle, bool end)> Operation;

class ProgressFunctor
{
private:
	const HINSTANCE _instance;
	unsigned _iconId;
	bool _break = false;
	std::shared_ptr<Windows::ProgressDialog> _dlg;
	std::shared_ptr<std::thread> _t;
	const std::string _subTitle;
	const bool& _show;
	bool _lastShow = false;
public:
	ProgressFunctor(const HINSTANCE& i, unsigned id, const std::string& subTitle, const bool& show) : _instance(i), _iconId(id), _subTitle(subTitle), _show(show)
	{
		_lastShow = !_show;
	}

	virtual ~ProgressFunctor()
	{
		if (_dlg)
			_dlg->Close();
		if (_t && _t->joinable())
			_t->join();
	}

	void Create()
	{
		Concurrent::Event e(true);
		_t.reset(new std::thread([&]
		{
			Windows::ProgressConfig c(FileUtilsEngine::GetAppName() + _subTitle, IDD_PROGRESS, "", IDC_LABEL1, IDC_PROGRESS1);
			_dlg.reset(new Windows::ProgressDialog(_instance, c, _iconId, _show));
			_break = !_dlg->DoModal(&e);
		}));
		e.Wait();
	}

	bool operator()(const FileUtilsEngine::StatusOperationType& operationType, const ulonglong partial, const ulonglong total)
	{
		if (!_dlg)
		{
			Create();
			_dlg->Label.SetText(FileUtilsEngine::GetOperationType(operationType));
			_dlg->Progress.SetRange(0, 100);
		}
		if (0 == total || _break)
			return _break;
		//Setting reading/writting percentage
		auto r = (ushort)(100.0f*partial / (float)total);
		_dlg->Progress.SetPos(r);
		if (_show != _lastShow)
		{
			_dlg->SetShow(_show);
			_lastShow = _show;
		}

		return false;
	}
};

bool CommandProcessor::operator()(Concurrent::ChannelProcessorData<Command>& d)
{
	auto& command = d.GetInput();
	Operation op;
	switch (command.Type)
	{
	case CommandType::Cipher:
		op = CommandProcessor::DoCipher;
		break;
	case CommandType::Uncipher:
		op = CommandProcessor::DoUncipher;
		break;
	case CommandType::Lock:
		op = &CommandProcessor::DoLock;
		break;
	case CommandType::GetLockers:
		op = CommandProcessor::DoGetLockers;
		break;
	case CommandType::Hash:
		op = CommandProcessor::DoHash;
		break;
	case CommandType::Entropy:
		op = CommandProcessor::DoEntropy;
		break;
	}
	//Browsing files
	Time::Stopwatch sw;
	auto totalFiles = (ulong)command.Files.size();
	for (auto i = 0u; i < totalFiles; i++)
	{
		auto& f = command.Files[i];
		//Setting subTitle
		std::ostringstream oss;
		oss << " [" << i + 1 << "/" << totalFiles << "]: " << IO::FileHandler::GetFilename(f);
		//Executing operation
		auto r = op(f, oss.str(), totalFiles - 1 == i);
		if (CommandProcessorReturn::Break == r)
		{
			_events->OnBatchBreak(command.Type, f);
			break;
		}
		else if (CommandProcessorReturn::Error == r)
		{
			_events->OnBatchError(command.Type, f);
			break;
		}
	}
	_events->OnBatchEnded(command.Type, totalFiles, sw.GetElapsed());

	return true;
}

CommandProcessorReturn CommandProcessor::DoCipher(const Filepath& filePath, const std::string& subTitle, bool end)
{
	Buffer s;
	if (!FileUtilsEngine::GetSecretOrOpenDialog(s_hInstance, true, s))
		return CommandProcessorReturn::Break;
	auto r = FileUtilsEngine::ActionSymmetricCipher().CipherFile(s, filePath, filePath + ".cip", ProgressFunctor(s_hInstance, IDI_CIPHER, subTitle, FileUtilsEngine::GetConfig()->IsShowProgress()));
	if (FileUtilsEngine::ReturnTraits::IsFail(r) && !FileUtilsEngine::ReturnTraits::IsBreak(r))
	{
		::MessageBoxA(0L, "Error on processing file", FileUtilsEngine::GetAppName().c_str(), MB_ICONERROR);
		return CommandProcessorReturn::Error;
	}
	
	return FileUtilsEngine::ReturnTraits::IsBreak(r) ? CommandProcessorReturn::Break : CommandProcessorReturn::Ok;
}

CommandProcessorReturn CommandProcessor::DoUncipher(const Filepath& filePath, const std::string& subTitle, bool end)
{
	Buffer s;
	if (!FileUtilsEngine::GetSecretOrOpenDialog(s_hInstance, false, s))
		return CommandProcessorReturn::Break;
	auto r = FileUtilsEngine::ActionSymmetricCipher().UncipherFile(s, filePath, filePath + ".unc", ProgressFunctor(s_hInstance, IDI_UNCIPHER, subTitle, FileUtilsEngine::GetConfig()->IsShowProgress()));
	if (FileUtilsEngine::ReturnTraits::IsFail(r) && !FileUtilsEngine::ReturnTraits::IsBreak(r))
	{
		::MessageBoxA(0L, "Error on processing file", FileUtilsEngine::GetAppName().c_str(), MB_ICONERROR);
		return CommandProcessorReturn::Error;
	}

	return FileUtilsEngine::ReturnTraits::IsBreak(r) ? CommandProcessorReturn::Break : CommandProcessorReturn::Ok;
}

typedef std::list<std::tuple<Filepath, FileUtilsEngine::LockFileControl>> Locks;

CommandProcessorReturn CommandProcessor::DoLock(const Filepath& filePath, const std::string& subTitle, bool end)
{
	static thread_local Locks locks;
	FileUtilsEngine::LockFileControl lfc;
	FileUtilsEngine::ActionLocker().LockFile(filePath, lfc);
	locks.emplace_back(filePath, std::move(lfc));
	if (!end)
		return CommandProcessorReturn::Ok;
	//Show message box
	Concurrent::Event e(true);
	std::thread([](Locks& locks, Concurrent::Event& e)
	{
		auto ll = std::move(locks);
		e.Notify();
		std::stringstream oss;
		oss << "Files locked:" << std::endl;
		for (auto& f : ll)
		{
			oss << IO::FileHandler::GetFilename(std::get<0>(f)) << ": ";
			oss << (std::get<1>(f).IsLocked() ? "Locked successfully" : "could not be locked") << std::endl;
		}
		::MessageBoxA(0L, oss.str().c_str(), FileUtilsEngine::GetAppName().c_str(), 0L);
	}, std::ref(locks), std::ref(e)).detach();
	e.Wait();

	return CommandProcessorReturn::Ok;
}

typedef std::list<std::tuple<Filepath, FileUtilsEngine::AppInfos>> Lockers;

CommandProcessorReturn CommandProcessor::DoGetLockers(const Filepath& filePath, const std::string& subTitle, bool end)
{
	static thread_local Lockers lockers;
	//Getting lockers
	FileUtilsEngine::AppInfos appInfos;
	FileUtilsEngine::ActionLocker().GetAppsLockingFile(filePath, appInfos);
	lockers.emplace_back(filePath, std::move(appInfos));
	if (!end)
		return CommandProcessorReturn::Ok;
	//Show message box
	Concurrent::Event e(true);
	std::thread([](Lockers& lockers, Concurrent::Event& e)
	{
		auto ll = std::move(lockers);
		e.Notify();
		std::stringstream oss;
		oss << "Files locked:" << std::endl;
		for (auto& f : ll)
		{
			auto& file = std::get<0>(f);
			auto& appInfos = std::get<1>(f);
			oss << IO::FileHandler::GetFilename(file) << ": ";
			if (appInfos.empty())
				oss << "is free";
			else
			{
				oss << "[ ";
				for (auto& appInfo : appInfos)
				{
					oss << appInfo.name << ":" << appInfo.processId << " ";
				}
				oss << "]";
			}
			oss << std::endl;
		}
		oss << std::endl << "Click ok to unlock.";
		::MessageBoxA(0L, oss.str().c_str(), FileUtilsEngine::GetAppName().c_str(), 0L);
	}, std::ref(lockers), std::ref(e)).detach();
	e.Wait();

	return CommandProcessorReturn::Ok;
}

CommandProcessorReturn CommandProcessor::DoHash(const Filepath& filePath, const std::string& subTitle, bool end)
{
	auto config = FileUtilsEngine::GetConfig();
	FileUtilsEngine::Hashes hh;
	if (FileUtilsEngine::ReturnTraits::IsFail(FileUtilsEngine::ActionHasher().CreateHashesFromFile(filePath, hh, config->GetEncoding(), ProgressFunctor(s_hInstance, IDI_HASH, subTitle, FileUtilsEngine::GetConfig()->IsShowProgress()))))
		return CommandProcessorReturn::Error;
	//Creating envelop for hashes
	std::string e;
	auto r = FileUtilsEngine::Envelopper::GenerateHashes(hh, config, filePath, e);
	if (FileUtilsEngine::Return::InputFileEmpty == r)
	{
		::MessageBoxA(0L, "File is empty", FileUtilsEngine::GetAppName().c_str(), MB_ICONERROR);
		return CommandProcessorReturn::Error;
	}
	else if (FileUtilsEngine::ReturnTraits::IsBreak(r))
	{
		return CommandProcessorReturn::Break;
	}
	else if (FileUtilsEngine::ReturnTraits::IsFail(r))
	{
		::MessageBoxA(0L, "Error on processing file", FileUtilsEngine::GetAppName().c_str(), MB_ICONERROR);
		return CommandProcessorReturn::Error;
	}
	//Saving file
	FileUtilsEngine::WriteDataOnFile(FileUtilsEngine::FileType::Hashed, filePath + ".hashes", { e.begin(), e.end() });

	return CommandProcessorReturn::Ok;
}

CommandProcessorReturn CommandProcessor::DoEntropy(const Filepath& filePath, const std::string& subTitle, bool end)
{
	float e = 0.0f;
	auto r = FileUtilsEngine::ActionEntropy().CalcutateEntropyFromFile(filePath, e, ProgressFunctor(s_hInstance, IDI_ENTROPY, subTitle, FileUtilsEngine::GetConfig()->IsShowProgress()));
	if (FileUtilsEngine::Return::InputFileEmpty == r)
	{
		::MessageBoxA(0L, "File is empty", FileUtilsEngine::GetAppName().c_str(), MB_ICONERROR);
		return CommandProcessorReturn::Error;
	}
	else if (FileUtilsEngine::ReturnTraits::IsBreak(r))
	{
		return CommandProcessorReturn::Break;
	}
	else if (FileUtilsEngine::ReturnTraits::IsFail(r))
	{
		::MessageBoxA(0L, "Error on processing file", FileUtilsEngine::GetAppName().c_str(), MB_ICONERROR);
		return CommandProcessorReturn::Error;
	}
	std::ostringstream oss;
	oss << "   File: " << filePath << std::endl;
	oss << "Entropy: " << std::fixed << std::setprecision(3) << e;
	::MessageBoxA(0L, oss.str().c_str(), FileUtilsEngine::GetAppName().c_str(), MB_ICONINFORMATION);

	return CommandProcessorReturn::Ok;
}