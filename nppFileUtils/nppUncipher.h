#pragma once
#include "nppFileUtils.h"
#include "../../libs/nppUtils/ScintillaHelper.h"

class nppUncipher
{
private:
	const nppUtils::ScintillaHelper& _sh;
public:
	nppUncipher(const nppUtils::ScintillaHelper& sh) : _sh(sh)
	{}

	void operator()();
};

void DoUncipher();