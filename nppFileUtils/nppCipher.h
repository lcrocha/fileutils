#pragma once
#include "nppFileUtils.h"
#include "../../libs/nppUtils/ScintillaHelper.h"

class nppCipher
{
private:
	const nppUtils::ScintillaHelper& _sh;
public:
	nppCipher(const nppUtils::ScintillaHelper& sh) : _sh(sh)
	{}

	void operator()();
};

void DoCipher();