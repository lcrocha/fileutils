#include "nppCipher.h"
#include "../FileUtilsEngine/ActionSymmetricCipher.h"

void DoCipher()
{
	try
	{
		nppCipher c(nppUtils::GetScintillaHelper());
		c();
	}
	catch (std::exception& e)
	{
		EXCEPTION(e.what());
	}
}

void nppCipher::operator()()
{
	Buffer secret;
	if (!FileUtilsEngine::GetSecretOrOpenDialog(nppUtils::GetInstance(), true, secret))
		return;
	//Getting text
	std::string in, ou;
	_sh.GetText(in);
	Buffer bin(in.begin(), in.end()), bou;
	//Ciphering
	if (FileUtilsEngine::ReturnTraits::IsFail(FileUtilsEngine::ActionSymmetricCipher().CipherBuffer(secret, bin, bou)))
		throw std::runtime_error("Error on cipher");
	//Setting output
	_sh.SetFullText(std::string(bou.begin(), bou.end()).c_str());
}