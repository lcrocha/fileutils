#include "nppUncipher.h"
#include "../FileUtilsEngine/ActionSymmetricCipher.h"

void DoUncipher()
{
	try
	{
		nppUncipher u(nppUtils::GetScintillaHelper());
		u();
	}
	catch (std::exception& e)
	{
		EXCEPTION(e.what());
	}
}

void nppUncipher::operator()()
{
	Buffer secret;
	if (!FileUtilsEngine::GetSecretOrOpenDialog(nppUtils::GetInstance(), false, secret))
		return;
	//Getting text
	std::string in, ou;
	_sh.GetText(in);
	Buffer bin(in.begin(), in.end()), bou;
	//Unciphering
	auto r = FileUtilsEngine::ActionSymmetricCipher().UncipherBuffer(secret, bin, bou);
	if (FileUtilsEngine::Return::OK != r && FileUtilsEngine::Return::CipheredFileNotRecognized != r)
		throw std::runtime_error("Error on decipher");
	ou.assign(bou.begin(), bou.end());
	//Setting output
	_sh.SetFullText(ou.c_str());
}
