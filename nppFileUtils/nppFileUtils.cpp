#include "nppFileUtils.h"
#include "..\Common\Version.h"
#include "nppCipher.h"
#include "nppUncipher.h"
#include <sstream>
#include <numeric>
#include <iomanip>
#include "../FileUtilsEngine/resource.h"
#include "../../libs/Helper/Windows/DlgInput.h"
#include "../FileUtilsEngine/DlgConfig.h"
#include "../FileUtilsEngine/FileUtilsEngine.h"
#include "../FileUtilsEngine/ActionHasher.h"
#include "../../Libs/Helper/Windows/DlgInput.h"

FileUtilsEngine::IConfig* _config = nullptr;

static void DisplayMessage(const char* message, unsigned icon)
{
	::MessageBoxA(nppUtils::GetScintillaHelper().GetScintillaHwnd(), message, "File Utils", icon);
}

void nppFileUtils::DisplayInfoMessage(const char* message)
{
	DisplayMessage(message, MB_ICONINFORMATION);
}

void nppFileUtils::DisplayErrorMessage(const char* message)
{
	DisplayMessage(message, MB_ICONERROR);
}

static void DoEntropy()
{
	try
	{
		//Getting data
		auto& sh = nppUtils::GetScintillaHelper();
		std::string in;
		sh.GetText(in);
		if (in.empty())
			return;
		//Getting entropy
		auto e = Crypto::CalculateEntropy({ in.begin(), in.end() });
		//Displaying hashes
		std::ostringstream oss;
		oss << "Entropy: " << std::fixed << std::setprecision(3) << e;
		DisplayMessage(oss.str().c_str(), MB_ICONINFORMATION);
	}
	catch (std::exception& e)
	{
		EXCEPTION(e.what());
	}
}

static void DoHash()
{
	try
	{
		const Crypto::Encoding& encoding(FileUtilsEngine::GetConfig()->GetEncoding());
		//Getting data
		auto& sh = nppUtils::GetScintillaHelper();
		std::string in;
		sh.GetText(in);
		if (in.empty())
			return;
		//Hashing
		FileUtilsEngine::Hashes hashes;
		Buffer bin(in.begin(), in.end());
		if (FileUtilsEngine::ReturnTraits::IsFail(FileUtilsEngine::ActionHasher().CreateHashesFromBuffer(bin, hashes, encoding)))
			throw std::runtime_error("Error on hashing data");
		//Displaying hashes
		std::ostringstream oss;
		for (auto& h : hashes)
			oss << h.first << ": " << std::string(h.second.begin(), h.second.end()) << std::endl;
		DisplayMessage(oss.str().c_str(), MB_ICONINFORMATION);
	}
	catch (std::exception& e)
	{
		EXCEPTION(e.what());
	}
}

static void DoConfig()
{
	FileUtilsEngine::OpenConfigDialog(nppUtils::GetInstance());
}

static void DoSecret()
{
	try
	{
		Windows::InputConfig config = { FileUtilsEngine::GetAppName(), IDD_INPUT, "Enter number of bytes", IDC_LABEL1, "", IDC_LABEL2, false, IDC_EDIT, "16" };
		Windows::InputDialog dlg(nppUtils::GetInstance(), config, IDI_TOOL);
		if (!dlg.DoModal() || dlg.Get().empty())
			return;
		auto size = (unsigned)std::atoi(dlg.Get().c_str());
		if (0 == size)
			return;
		//Generate secret
		auto b = Helper::GenerateBuffer(Helper::BufferType::TextRandom, std::min<unsigned>(size, 128));
		//Sending to clipboard
		::OpenClipboard(nppUtils::GetScintillaHelper().GetScintillaHwnd());
		::EmptyClipboard();
		auto glob = ::GlobalAlloc(GMEM_FIXED, b.size());
		::memcpy(glob, b.data(), b.size());
		::SetClipboardData(CF_TEXT, glob);
		::CloseClipboard();
		//Sending to scintilla
		DisplayMessage("Data copied to the clipboard", MB_ICONINFORMATION);
	}
	catch (std::exception& e)
	{
		EXCEPTION(e.what());
	}
}

BOOL APIENTRY DllMain(HMODULE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved)
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
	{
		FileUtilsEngine::InitializeConfig(hModule);
		_config = FileUtilsEngine::GetConfig();
		//Setting name
		auto n = FileUtilsEngine::GetAppName();
		std::wstring name(n.begin(), n.end());
		//Creating nppManager
		nppUtils::Create(hModule, name.c_str());
		nppUtils::AddFuncItem(L"Cipher", DoCipher);
		nppUtils::AddFuncItem(L"Uncipher", DoUncipher);
		nppUtils::AddFuncItem(L"Hash", DoHash);
		nppUtils::AddFuncItem(L"Generate Secret", DoSecret);
		nppUtils::AddFuncItem(L"Entropy", DoEntropy);
		nppUtils::AddFuncItem(L"Config...", DoConfig);
	}
	break;
	case DLL_PROCESS_DETACH:
		nppUtils::Release();
		FileUtilsEngine::FinalizeConfig();
		break;
	}

	return TRUE;
}