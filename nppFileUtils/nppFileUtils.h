#pragma once
#ifdef NOWINOFFSETS
#undef NOWINOFFSETS
#endif
#include <Windows.h>
#include "../../libs/nppUtils/nppHooks.h"

#define EXCEPTION(m) nppFileUtils::DisplayErrorMessage(m)

namespace nppFileUtils
{
	void DisplayErrorMessage(const char* message);
	void DisplayInfoMessage(const char* message);
}