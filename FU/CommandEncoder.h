#pragma once
#include "main.h"
#include <string>
#include "../FileUtilsEngine/FileUtilsEngine.h"

namespace FileUtils
{
	class CommandEncoder : public ICommand
	{
	private:
		Crypto::Encoding _inputEncoding;
		Crypto::Encoding _outputEncoding;
	public:
		CommandEncoder() {}
		virtual bool ShouldRun() override;
		virtual bool Run(FileUtilsEngine::Return& ret) override;
		virtual void PrintHelp() override;
		//Auxiliary
		static bool GetDataEncoding(const std::string& s, Crypto::Encoding& e);
	};
}