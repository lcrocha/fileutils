#pragma once
#include "main.h"
#include <string>
#include "../FileUtilsEngine/FileUtilsEngine.h"

namespace FileUtils
{
	class CommandSigner : public ICommand
	{
	private:
		enum operation { generate, sign, verify } _operation;
		//Auxilliary
		bool ExecuteGenerate(const std::string& privateKeyPath, const std::string& publicKeyPath, FileUtilsEngine::Return& ret);
		bool ExecuteSign(const std::string& privateKeyPath, const std::string& inputFilePath, const std::string& signaturePath, FileUtilsEngine::Return& ret);
		bool ExecuteVerify(const std::string& publicKeyPath, const std::string& inputFilePath, const std::string& signaturePath, FileUtilsEngine::Return& ret);
	public:
		CommandSigner() {}
		virtual bool ShouldRun() override;
		virtual bool Run(FileUtilsEngine::Return& ret) override;
		virtual void PrintHelp() override;
	};
}