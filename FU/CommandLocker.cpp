#include "main.h"
#include "CommandLocker.h"
#include <iostream>
#include "../FileUtilsEngine/ActionLocker.h"
using namespace FileUtils;

CommandLocker::CommandLocker()
{}

bool CommandLocker::ShouldRun()
{
	if (_params.size() >= 2)
	{
		if (ConvertToLower(_params[0]) == "-ll")
			_operation = lock;
		else if (ConvertToLower(_params[0]) == "-lg")
			_operation = getapps;
		else
			return false;
		return true;
	}

	return false;
}

bool CommandLocker::Run(FileUtilsEngine::Return& ret)
{
	auto& file = _params[1];
	if (lock == _operation)
	{
		ret = FileUtilsEngine::ActionLocker().LockFile(_params[1], [&]()
		{
			std::string s;
			std::cout << "Press any key to release the file " << file << std::endl;
			std::getchar();
			std::cout << "File released!";
		});
	}
	else if (getapps == _operation)
	{
		auto i = 0u;
		FileUtilsEngine::AppInfos apps;
		ret = FileUtilsEngine::ActionLocker().GetAppsLockingFile(_params[1], apps);
		if (apps.empty())
		{
			std::cout << "File " << file << " is free!" << std::endl;
		}
		else for (auto& app : apps)
		{
			std::cout << i << ".ApplicationType = " << app.type << std::endl;
			std::cout << i << ".AppName = " << app.name << std::endl;
			std::cout << i << ".ProcessId = " << app.processId << std::endl;
			std::cout << i << ".FullPath = " << app.fullPath << std::endl << std::endl;
			i++;
		}
	}

	return true;
}

void CommandLocker::PrintHelp()
{
	std::cout
		<< std::endl << "Locker operations:"
		<< std::endl << "  - Lock a file"
		<< std::endl << "    Sintax:          " << PROJECT_NAME << " -ll <InputFilePath>"
		<< std::endl << "    <InputFilePath>  Input file to be locked"
		<< std::endl << "  - Getting all apps locking a file"
		<< std::endl << "    Sintax:          " << PROJECT_NAME << " -lg <InputFilePath>"
		<< std::endl << "    <InputFilePath>  Input file to be checked if is locked by another app";
}