#pragma once
#include <tchar.h>
#include <vector>
#include <algorithm>
#include <ctype.h>
#include "../Common/Version.h"
#include "../FileUtilsEngine/FileUtilsEngine.h"

namespace FileUtils
{
	const std::string ConvertToUpper(const std::string& s);
	const std::string ConvertToLower(const std::string& s);
	bool LogOperation(const FileUtilsEngine::StatusOperationType& operationType, const ulonglong, const ulonglong);

	class ICommand
	{
	protected:
		std::vector<std::string> _params;
	public:
		ICommand* Initialize(uint argc, const char** argv)
		{
			for (uint i = 1; i < argc; ++i) if(argv[i] != nullptr && ::strlen(argv[i]) > 0)
			{
				_params.push_back(argv[i]);
			}

			return this;
		}

		virtual bool ShouldRun() = 0;
		virtual bool Run(FileUtilsEngine::Return& ret) = 0;
		virtual void PrintHelp() = 0;
	};
};
