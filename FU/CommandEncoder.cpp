#include "main.h"
#include "CommandEncoder.h"
#include <iostream>
#include "../FileUtilsEngine/ActionEncoder.h"
using namespace FileUtils;

bool CommandEncoder::GetDataEncoding(const std::string& s, Crypto::Encoding& e)
{
	auto ret = false;
	auto ss = ConvertToLower(s);
	//Checking
	if (ss == "base64")
	{
		e = Crypto::Encoding::Base64;
		ret = true;
	}
	else if (ss == "hex")
	{
		e = Crypto::Encoding::Hex;
		ret = true;
	}
	else if (ss == "bin")
	{
		e = Crypto::Encoding::Bin;
		ret = true;
	}

	return ret;
}


bool CommandEncoder::ShouldRun()
{
	if (_params.size() >= 5 && ConvertToLower(_params[0]) == "-e")
		return true;

	return false;
}

bool CommandEncoder::Run(FileUtilsEngine::Return& ret)
{
	//Getting enconding for input file
	if (!GetDataEncoding(_params[2], _inputEncoding))
	{
		std::cout << "Error: Invalid encoding [" << _params[2] << "]" << std::endl;
		std::cout << "Please, choose one as follows: hex, base64, bin" << std::endl;
		return false;
	}
	//Getting enconding for output file
	if (!GetDataEncoding(_params[4], _outputEncoding))
	{
		std::cout << "Error: Invalid encoding [" << _params[4] << "]" << std::endl;
		std::cout << "Please, choose one as follows: hex, base64, bin" << std::endl;
		return false;
	}
	//Getting remaining data
	auto inputFilePath = _params[1];
	auto outputFilePath = _params[3];
	//Logging
	std::cout << "Input encoding: " << ConvertToLower(_params[2]) << std::endl;
	std::cout << "Input file: " << inputFilePath << std::endl;
	std::cout << "Output encoding: " << ConvertToLower(_params[4]) << std::endl;
	std::cout << "Output file: " << outputFilePath << std::endl;
	std::cout << "Encoding file" << std::endl;
	//Executing
	ret = FileUtilsEngine::ActionEncoder().ConvertFile(inputFilePath, _inputEncoding, outputFilePath, _outputEncoding, LogOperation);

	return true;
}

void CommandEncoder::PrintHelp()
{
	std::cout
		<< std::endl << "Encoder operation:"
		<< std::endl << "  - Encode a file"
		<< std::endl << "    Sintax:          " << PROJECT_NAME << " -e <InputFilePath> <InputEncoding> <OutputFilePath> <OutputEncoding>"
		<< std::endl << "    <InputFilePath>  Input file to be encoded"
		<< std::endl << "    <InputEncoding>  Input data encoding [hex, base64, bin]"
		<< std::endl << "    <OutputFilePath> Output file to be encoded"
		<< std::endl << "    <OutputEncoding> Output data encoding [hex, base64, bin]";
}