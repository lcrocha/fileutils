#pragma once
#include "main.h"
#include <string>
#include "../FileUtilsEngine/FileUtilsEngine.h"

namespace FileUtils
{
	class CommandHasher : public ICommand
	{
	private:
		enum operation { none, create, check } _operation = none;
		Crypto::HasherAlgorithm _alg;
		std::string _hashPath;
		//Auxilliary
		bool GetHasherAlgorithm(const std::string& h);
		bool ExecuteCreate(const std::string& inputFilePath, const std::string& hashFilePath, FileUtilsEngine::Return& ret);
		bool ExecuteCheck(const std::string& inputFilePath, const std::string& hashFilePath, FileUtilsEngine::Return& ret);
	public:
		CommandHasher() {}

		virtual bool ShouldRun() override;
		virtual bool Run(FileUtilsEngine::Return& ret) override;
		virtual void PrintHelp() override;
	};
}

