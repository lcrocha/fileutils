#include "main.h"
#include <vector>
#include <algorithm>
#include <iostream>
#include <iomanip>
#include "CommandHasher.h"
#include "CommandRandomGenerator.h"
#include "CommandEncoder.h"
#include "CommandSigner.h"
#include "CommandSymmetricCipher.h"
#include "CommandAsymmetricCipher.h"
#include "CommandLocker.h"
#include "../../Libs/Helper/Time/Stopwatch.h"
#include "resource.h"

const std::string FileUtils::ConvertToUpper(const std::string& s)
{
	auto ss = s;
	std::transform(ss.begin(), ss.end(), ss.begin(), ::toupper);

	return ss;
}

const std::string FileUtils::ConvertToLower(const std::string& s)
{
	auto ss = s;
	std::transform(ss.begin(), ss.end(), ss.begin(), ::tolower);

	return ss;
}

bool FileUtils::LogOperation(const FileUtilsEngine::StatusOperationType& operationType, const ulonglong partial, const ulonglong total)
{
	static auto first = ::GetTickCount();
	auto r = 100.0f * ((float)partial / (float)total); 
	if (::GetTickCount() - first < 250ul && r > 0.0f && r < 100.0f)
		return false;
	first = ::GetTickCount();
	//Logging
	std::cout << "\r[";
	const auto totalPoints = 20u;
	for (auto i = 0u; i < totalPoints; ++i)
	{
		auto p = 100.0f * (float)i / (float)(totalPoints - 1);
		if (p <= r)
			std::cout << ".";
		else
			std::cout << " ";
	}
	std::cout << "] " << std::fixed << std::setprecision(2) << std::setw(7) << r << "%";

	return false;
}

void PrintHeader()
{
	std::cout 
		<< APP_PRODUCTNAME_STR << " "
		<< APP_PRODUCTVERSION_STR << std::endl;
}

void PrintFooter(const Time::Stopwatch& sw)
{
	std::cout << "Time elapsed: " << sw.GetElapsed() << std::endl;
}

int main(int argc, const char* argv[])
{
	FileUtilsEngine::InitializeConfig(0ul);
	Time::Stopwatch sw;
	auto first = std::chrono::system_clock::now();
	auto appRet = 0;
	//Commands
	FileUtils::CommandHasher hasher;
	FileUtils::CommandRandomGenerator rng;
	FileUtils::CommandEncoder encoder;
	FileUtils::CommandSigner signer;
	FileUtils::CommandSymmetricCipher symmetricCipher;
	FileUtils::CommandAsymmetricCipher asymmetricCipher;
	FileUtils::CommandLocker locker;
	//Stacking commands
	std::vector<FileUtils::ICommand*> commands = { &hasher, &rng, &encoder, &signer, &symmetricCipher, &asymmetricCipher, &locker };
	//Browsing commands
	PrintHeader();
	auto& it = std::find_if(commands.begin(), commands.end(), [&](FileUtils::ICommand* cmd)
	{
		return cmd->Initialize(argc, argv)->ShouldRun();
	});
	if (it != commands.end())
	{
		auto& cmd = *it;
		FileUtilsEngine::Return ret;
		if (cmd->Run(ret))
		{
			//Checking results
			if (FileUtilsEngine::ReturnTraits::IsFail(ret))
			{
				std::cout << std::endl << "Error: " << FileUtilsEngine::GetReturnCodeMessge(ret) << std::endl;
			}
			else
			{
				//All ok
				std::cout << std::endl << "Success!" << std::endl;
				appRet = 1;
			}
		}
	}
	else
	{
		std::cout << APP_DESCRIPTION_STR << std::endl;
		std::cout << APP_COMPANY_STR << std::endl;
		std::cout << "HELP:";
		//Printing all help
		for (auto command : commands)
		{
			command->PrintHelp();
			std::cout << std::endl;
		}
	}
	PrintFooter(sw);

	FileUtilsEngine::FinalizeConfig();
	return appRet;
}