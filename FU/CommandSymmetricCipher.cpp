#include "main.h"
#include "CommandSymmetricCipher.h"
#include <iostream>
#include "../FileUtilsEngine/ActionSymmetricCipher.h"
using namespace FileUtils;

bool CommandSymmetricCipher::ExecuteCipher(const std::string& passwordPath, const std::string& inputFilePath, const std::string& cipheredFilePath, FileUtilsEngine::Return& ret)
{
	//Logging
	std::cout << "Password file path: " << passwordPath << std::endl;
	std::cout << "Input file path: " << inputFilePath << std::endl;
	std::cout << "Ciphered file path: " << cipheredFilePath << std::endl;
	std::cout << "Ciphering file";
	//Executing
	ret = FileUtilsEngine::ActionSymmetricCipher().CipherFileUsingSecretFile(passwordPath, inputFilePath, cipheredFilePath, LogOperation);

	return true;
}

bool CommandSymmetricCipher::ExecuteUncipher(const std::string& passwordPath, const std::string& cipheredFilePath, const std::string& uncipheredFilePath, FileUtilsEngine::Return& ret)
{
	//Logging
	std::cout << "Password file path: " << passwordPath << std::endl;
	std::cout << "Ciphered file path: " << cipheredFilePath << std::endl;
	std::cout << "Unciphered file path: " << uncipheredFilePath << std::endl;
	std::cout << "Unciphering file";
	//Executing
	ret = FileUtilsEngine::ActionSymmetricCipher().UncipherFileUsingSecretFile(passwordPath, cipheredFilePath, uncipheredFilePath, LogOperation);

	return true;
}

bool CommandSymmetricCipher::ShouldRun()
{
	auto ret = false;
	if (_params.size() >= 4)
	{
		auto o = ConvertToLower(_params[0]);
		if (o == "-pc")
		{
			_operation = cipher;
			ret = true;
		}
		else if (o == "-pu")
		{
			_operation = uncipher;
			ret = true;
		}
	}

	return ret;
}

bool CommandSymmetricCipher::Run(FileUtilsEngine::Return& ret)
{
	//Choosing commands
	if (_operation == cipher)
		return ExecuteCipher(_params[1], _params[2], _params[3], ret);

	return ExecuteUncipher(_params[1], _params[2], _params[3], ret);
}

void CommandSymmetricCipher::PrintHelp()
{
	std::cout
		<< std::endl << "Password cipher operations:"
		<< std::endl << "  - Cipher a file"
		<< std::endl << "    Sintax:          " << PROJECT_NAME << " -pc <PasswordPath> <InputFilePath> <CipheredFile>"
		<< std::endl << "    <PasswordPath>   Password file path"
		<< std::endl << "    <InputFilePath>  Input file to cipher"
		<< std::endl << "    <CipheredFile>   Ciphered file path"
		<< std::endl << "  - Uncipher a file"
		<< std::endl << "    Sintax:          " << PROJECT_NAME << " -pu <PasswordPath> <CipheredFile> <UncipheredFile>"
		<< std::endl << "    <PasswordPath>   Password file path"
		<< std::endl << "    <CipheredFile>   Ciphered file path"
		<< std::endl << "    <UncipheredFile> Unciphered file path";
}