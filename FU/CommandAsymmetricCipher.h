#pragma once
#include "main.h"
#include <string>
#include "../FileUtilsEngine/FileUtilsEngine.h"

namespace FileUtils
{
	class CommandAsymmetricCipher : public ICommand
	{
	private:
		enum operation { generate, cipher, uncipher } _operation;
		Crypto::AsymmetricKeySize _keySize;
		//Auxilliary
		bool GetKeySize(const std::string& k);
		bool ExecuteGenerate(const std::string& privateKeyPath, const std::string& publicKeyPath, FileUtilsEngine::Return& ret);
		bool ExecuteCipher(const std::string& publicKeyPath, const std::string& inputFilePath, const std::string& cipheredFilePath, FileUtilsEngine::Return& ret);
		bool ExecuteUncipher(const std::string& privateKeyPath, const std::string& cipheredFilePath, const std::string& uncipheredFilePath, FileUtilsEngine::Return& ret);
	public:
		CommandAsymmetricCipher() {}
		virtual bool ShouldRun() override;
		virtual bool Run(FileUtilsEngine::Return& ret) override;
		virtual void PrintHelp() override;
	};
}
