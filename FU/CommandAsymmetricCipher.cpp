#include "main.h"
#include "CommandAsymmetricCipher.h"
#include <iostream>
#include "../FileUtilsEngine/ActionAsymmetricCipher.h"
using namespace FileUtils;

bool CommandAsymmetricCipher::GetKeySize(const std::string& k)
{
	auto ret = false;
	if (k == "512") { _keySize = Crypto::AsymmetricKeySize::RSA_512; ret = true; }
	else if (k == "1024") { _keySize = Crypto::AsymmetricKeySize::RSA_1024; ret = true; }
	else if (k == "2048") { _keySize = Crypto::AsymmetricKeySize::RSA_2048; ret = true; }
	else if (k == "4096") { _keySize = Crypto::AsymmetricKeySize::RSA_4096; ret = true; }

	return ret;
}

bool CommandAsymmetricCipher::ExecuteGenerate(const std::string& privateKeyPath, const std::string& publicKeyPath, FileUtilsEngine::Return& ret)
{
	//Logging
	std::cout << "Private key path: " << privateKeyPath << std::endl;
	std::cout << "Public key path: " << publicKeyPath << std::endl;
	std::cout << "Generating key pair" << std::endl;
	//Executing
	ret = FileUtilsEngine::ActionAsymmetricCipher().GenerateKeyPair(_keySize, privateKeyPath, publicKeyPath, LogOperation);

	return true;
}

bool CommandAsymmetricCipher::ExecuteCipher(const std::string& publicKeyPath, const std::string& inputFilePath, const std::string& cipheredFilePath, FileUtilsEngine::Return& ret)
{
	//Logging
	std::cout << "Public key path: " << publicKeyPath << std::endl;
	std::cout << "Input file path: " << inputFilePath << std::endl;
	std::cout << "Ciphered file path: " << cipheredFilePath << std::endl;
	std::cout << "Ciphering file" << std::endl;
	//Executing
	ret = FileUtilsEngine::ActionAsymmetricCipher().CipherFile(publicKeyPath, inputFilePath, cipheredFilePath, LogOperation);

	return true;
}

bool CommandAsymmetricCipher::ExecuteUncipher(const std::string& privateKeyPath, const std::string& cipheredFilePath, const std::string& uncipheredFilePath, FileUtilsEngine::Return& ret)
{
	//Logging
	std::cout << "Private key path: " << privateKeyPath << std::endl;
	std::cout << "Ciphered file path: " << cipheredFilePath << std::endl;
	std::cout << "Unciphered file path: " << uncipheredFilePath << std::endl;
	std::cout << "Unciphering file" << std::endl;
	//Executing
	ret = FileUtilsEngine::ActionAsymmetricCipher().UncipherFile(privateKeyPath, cipheredFilePath, uncipheredFilePath, LogOperation);

	return true;
}

bool CommandAsymmetricCipher::ShouldRun()
{
	auto ret = false;
	if (_params.size() >= 4)
	{
		auto o = ConvertToLower(_params[0]);
		if (o == "-ag")
		{
			_operation = generate;
			ret = true;
		}
		else if (o == "-ac")
		{
			_operation = cipher;
			ret = true;
		}
		else if (o == "-au")
		{
			_operation = uncipher;
			ret = true;
		}
	}

	return ret;
}

bool CommandAsymmetricCipher::Run(FileUtilsEngine::Return& ret)
{
	//getting algorithm param
	if (_operation == generate && !GetKeySize(_params[1]))
	{
		std::cout << "Error: Invalid key size [" << _params[1] << "]" << std::endl;
		std::cout << "Please, choose one as follows: 512, 1024, 2048, 4096" << std::endl;
		return false;
	}
	//Choosing commands
	if (_operation == generate)
		return ExecuteGenerate(_params[2], _params[3], ret);
	else if (_operation == cipher)
		return ExecuteCipher(_params[1], _params[2], _params[3], ret);

	return ExecuteUncipher(_params[1], _params[2], _params[3], ret);
}

void CommandAsymmetricCipher::PrintHelp()
{
	std::cout
		<< std::endl << "Asymmetric cipher operations:"
		<< std::endl << "  - Generate a key pair"
		<< std::endl << "    Sintax:          " << PROJECT_NAME << " -ag <KeySize> <PrivateKeyPath> <PublicKeyPath>"
		<< std::endl << "    <KeySize>        512, 1024, 2048, 4096"
		<< std::endl << "    <PrivateKeyPath> Private key file path"
		<< std::endl << "    <PublicKeyPath>  Public key file path"
		<< std::endl << "  - Cipher a file"
		<< std::endl << "    Sintax:          " << PROJECT_NAME << " -ac <PublicKeyPath> <InputFilePath> <CipheredFile>"
		<< std::endl << "    <PublicKeyPath>  Public key file path"
		<< std::endl << "    <InputFilePath>  Input file to cipher"
		<< std::endl << "    <CipheredFile>   Ciphered file path"
		<< std::endl << "  - Uncipher a file"
		<< std::endl << "    Sintax:          " << PROJECT_NAME << " -au <PrivateKeyPath> <CipheredFile> <UncipheredFile>"
		<< std::endl << "    <PrivateKeyPath> Private key file path"
		<< std::endl << "    <CipheredFile>   Ciphered file path"
		<< std::endl << "    <UncipheredFile> Unciphered file path";
}