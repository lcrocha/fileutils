#include "main.h"
#include "CommandHasher.h"
#include <iostream>
#include "../FileUtilsEngine/ActionHasher.h"
#include "main.h"
using namespace FileUtils;

bool CommandHasher::GetHasherAlgorithm(const std::string& hs)
{
	auto h = ConvertToUpper(hs);
	auto ret = false;
	if (h == "MD2")		 { _alg = Crypto::HasherAlgorithm::MD2; ret = true; }
	else if (h == "MD4") { _alg = Crypto::HasherAlgorithm::MD4; ret = true; }
	else if (h == "MD5") { _alg = Crypto::HasherAlgorithm::MD5; ret = true; }
	else if (h == "SHA1") { _alg = Crypto::HasherAlgorithm::SHA1; ret = true; }
	else if (h == "SHA256") { _alg = Crypto::HasherAlgorithm::SHA256; ret = true; }
	else if (h == "SHA384") { _alg = Crypto::HasherAlgorithm::SHA384; ret = true; }
	else if (h == "SHA512") { _alg = Crypto::HasherAlgorithm::SHA512; ret = true; }

	return ret;
}

bool CommandHasher::ShouldRun()
{
	auto ret = false;
	if (_params.size() >= 4)
	{
		auto cmd = ConvertToLower(_params[0]);
		//Setting command
		if (cmd == "-hh")
			_operation = create;
		else if (cmd == "-hc")
			_operation = check;
	}

	return _operation != none;
}

bool CommandHasher::Run(FileUtilsEngine::Return& ret)
{
	//getting algorithm param
	if (!GetHasherAlgorithm(_params[1]))
	{
		std::cout << "Error: Invalid algorithm [" << _params[1] << "]" << std::endl;
		std::cout << "Please, choose one as follows: MD2, MD4, MD5, SHA1, SHA256, SHA384, SHA512" << std::endl;
		return false;
	}
	//executing command
	if (_operation == create)
		return ExecuteCreate(_params[2], _params[3], ret);

	return ExecuteCheck(_params[2], _params[3], ret);
}

bool CommandHasher::ExecuteCreate(const std::string& inputFilePath, const std::string& hashFilePath, FileUtilsEngine::Return& ret)
{
	//Logging
	std::cout << "Hash algorithm: " << ConvertToUpper(_params[1]) << std::endl;
	std::cout << "File to hash: " << inputFilePath << std::endl;
	std::cout << "Hash file path: " << hashFilePath << std::endl;
	std::cout << "Creating a hash from file" << std::endl;
	//Executing
	ret = FileUtilsEngine::ActionHasher().CreateHashFromFile(_alg, inputFilePath, hashFilePath, Crypto::Encoding::Bin, LogOperation);

	return true;
}

bool CommandHasher::ExecuteCheck(const std::string& inputFilePath, const std::string& hashFilePath, FileUtilsEngine::Return& ret)
{
	//Logging
	std::cout << "Hash algorithm: " << ConvertToUpper(_params[1]) << std::endl;
	std::cout << "File to check: " << inputFilePath << std::endl;
	std::cout << "Hash file path: " << hashFilePath << std::endl;
	std::cout << "Checking file's hash" << std::endl;
	//Executing
	ret = FileUtilsEngine::ActionHasher().CheckHashFromFile(_alg, inputFilePath, hashFilePath, Crypto::Encoding::Bin, LogOperation);

	return true;
}


void CommandHasher::PrintHelp()
{
	std::cout
		<< std::endl << "Hasher operations:"
		<< std::endl << "  - Create a hash from file"
		<< std::endl << "    Sintax:          " << PROJECT_NAME << " -hh <Algorithm> <InputFilePath> <HashFilePath>"
		<< std::endl << "    <Algorithm>      MD2, MD4, MD5, SHA1, SHA256, SHA384, SHA512"
		<< std::endl << "    <InputFilePath>  Input file path to calculate hash"
		<< std::endl << "    <HashFilePath>   Hash of the input file calculated and saved as binary"
		<< std::endl << "  - Check a hash from file"
		<< std::endl << "    Sintax:          " << PROJECT_NAME << " -hc <Algorithm> <InputFilePath> <HashFilePath>"
		<< std::endl << "    <Algorithm>      MD2, MD4, MD5, SHA1, SHA256, SHA384, SHA512"
		<< std::endl << "    <InputFilePath>  Input file path to calculate hash"
		<< std::endl << "    <HashFilePath>   Hash of the input file to verify";
}