#pragma once
#include "main.h"
#include <string>
#include "../FileUtilsEngine/FileUtilsEngine.h"

namespace FileUtils
{
	class CommandRandomGenerator : public ICommand
	{
	private:
		enum operation { bin, pwd } _operation;
		//
		bool ExecuteBin(const std::string& filePath, const ulonglong size, FileUtilsEngine::Return& ret);
		bool ExecutePwd(const std::string& filePath, const ulonglong size, FileUtilsEngine::Return& ret);
	public:
		CommandRandomGenerator() {}
		virtual bool ShouldRun() override;
		virtual bool Run(FileUtilsEngine::Return& ret) override;
		virtual void PrintHelp() override;
	};
}