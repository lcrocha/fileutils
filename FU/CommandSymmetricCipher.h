#pragma once
#include "main.h"
#include <string>
#include "../FileUtilsEngine/FileUtilsEngine.h"

namespace FileUtils
{
	class CommandSymmetricCipher : public ICommand
	{
	private:
		enum operation { cipher, uncipher } _operation;
		//Auxilliary
		bool ExecuteCipher(const std::string& passwordPath, const std::string& inputFilePath, const std::string& cipheredFilePath, FileUtilsEngine::Return& ret);
		bool ExecuteUncipher(const std::string& passwordPath, const std::string& cipheredFilePath, const std::string& uncipheredFilePath, FileUtilsEngine::Return& ret);
	public:
		CommandSymmetricCipher() {}
		virtual bool ShouldRun() override;
		virtual bool Run(FileUtilsEngine::Return& ret) override;
		virtual void PrintHelp() override;
	};
}