#include "main.h"
#include "CommandSigner.h"
#include <iostream>
#include "../FileUtilsEngine/ActionSigner.h"
using namespace FileUtils;

bool CommandSigner::ShouldRun()
{
	auto ret = false;
	if (_params.size() >= 3)
	{
		auto o = ConvertToLower(_params[0]);
		if (o == "-sg")
		{
			_operation = generate;
			ret = true;
		}
		else if (_params.size() >= 4 && o == "-ss")
		{
			_operation = sign;
			ret = true;
		}
		else if (_params.size() >= 4 && o == "-sv")
		{
			_operation = verify;
			ret = true;
		}
	}

	return ret;
}

bool CommandSigner::ExecuteGenerate(const std::string& privateKeyPath, const std::string& publicKeyPath, FileUtilsEngine::Return& ret)
{
	//Logging
	std::cout << "Private key path: " << privateKeyPath << std::endl;
	std::cout << "Public key path: " << publicKeyPath << std::endl;
	std::cout << "Generating key pair";
	//Executing
	ret = FileUtilsEngine::ActionSigner().GenerateKeyPair(privateKeyPath, publicKeyPath, LogOperation);

	return true;
}

bool CommandSigner::ExecuteSign(const std::string& privateKeyPath, const std::string& inputFilePath, const std::string& signaturePath, FileUtilsEngine::Return& ret)
{
	//Logging
	std::cout << "Private key path: " << privateKeyPath << std::endl;
	std::cout << "Input file path: " << inputFilePath << std::endl;
	std::cout << "Signature file path: " << signaturePath << std::endl;
	std::cout << "Signing file" << std::endl;
	//Executing
	ret = FileUtilsEngine::ActionSigner().SignFile(privateKeyPath, inputFilePath, signaturePath, LogOperation);

	return true;
}

bool CommandSigner::ExecuteVerify(const std::string& publicKeyPath, const std::string& inputFilePath, const std::string& signaturePath, FileUtilsEngine::Return& ret)
{
	//Logging
	std::cout << "Public key path: " << publicKeyPath << std::endl;
	std::cout << "Input file path: " << inputFilePath << std::endl;
	std::cout << "Signature file path: " << signaturePath << std::endl;
	std::cout << "Veryfying signature file" << std::endl;
	//Executing
	ret = FileUtilsEngine::ActionSigner().VerifySignature(publicKeyPath, inputFilePath, signaturePath, LogOperation);

	return true;
}

bool CommandSigner::Run(FileUtilsEngine::Return& ret)
{
	//Choosing commands
	if (_operation == generate)
		return ExecuteGenerate(_params[1], _params[2], ret);

	if (_operation == sign)
		return ExecuteSign(_params[1], _params[2], _params[3], ret);

	return ExecuteVerify(_params[1], _params[2], _params[3], ret);
}

void CommandSigner::PrintHelp()
{
	std::cout
		<< std::endl << "Signer operations:"
		<< std::endl << "  - Generate a key pair"
		<< std::endl << "    Sintax:          " << PROJECT_NAME << " -sg <PrivateKeyPath> <PublicKeyPath>"
		<< std::endl << "    <PrivateKeyPath> Private key file path"
		<< std::endl << "    <PublicKeyPath>  Public key file path"
		<< std::endl << "  - Sign a file"
		<< std::endl << "    Sintax:          " << PROJECT_NAME << " -ss <PrivateKeyPath> <InputFilePath> <SignaturePath>"
		<< std::endl << "    <PrivateKeyPath> Private key file path"
		<< std::endl << "    <InputFilePath>  File to generate the signature"
		<< std::endl << "    <SignaturePath>  Signature file path"
		<< std::endl << "  - Verify a signature file"
		<< std::endl << "    Sintax:          " << PROJECT_NAME << " -sv <PublicKeyPath> <InputFilePath> <SignaturePath>"
		<< std::endl << "    <PublicKeyPath>  Public key file path"
		<< std::endl << "    <InputFilePath>  File to generate the signature"
		<< std::endl << "    <SignaturePath>  Signature file path";
}