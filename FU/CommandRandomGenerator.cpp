#include <iostream>
#include "main.h"
#include "CommandRandomGenerator.h"
#include "../FileUtilsEngine/ActionEncoder.h"
#include "../../Libs/Helper/Utils/RandomNumberGenerator.h"
using namespace FileUtils;

bool CommandRandomGenerator::ShouldRun()
{
	if (_params.size() >= 3 && ConvertToLower(_params[0]) == "-gb")
	{
		_operation = bin;
		return true;
	}
	else if (_params.size() >= 3 && ConvertToLower(_params[0]) == "-gp")
	{
		_operation = pwd;
		return true;
	}

	return false;
}

bool CommandRandomGenerator::ExecuteBin(const std::string& filePath, const ulonglong size, FileUtilsEngine::Return& ret)
{
	//Logging
	std::cout << "Output file: " << filePath << std::endl;
	std::cout << "File size: " << size << " bytes" << std::endl;
	std::cout << "Creating a random content file" << std::endl;
	//Executing
	ret = FileUtilsEngine::ActionEncoder().GenerateFileRandomData(Helper::BufferType::BinaryRandom, filePath, size, LogOperation);

	return true;
}

bool CommandRandomGenerator::ExecutePwd(const std::string& filePath, const ulonglong size, FileUtilsEngine::Return& ret)
{
	//Logging
	std::cout << "Output file: " << filePath << std::endl;
	std::cout << "File size: " << size << " bytes" << std::endl;
	std::cout << "Creating a random content file" << std::endl;
	//Executing
	ret = FileUtilsEngine::ActionEncoder().GenerateFileRandomData(Helper::BufferType::TextRandom, filePath, size, LogOperation);

	return true;
}

bool CommandRandomGenerator::Run(FileUtilsEngine::Return& ret)
{
	//checking if the size is a number
	auto n = _params[2];
	auto size = 0ull;
	if (std::find_if_not(n.begin(), n.end(), ::isdigit) != n.end() || (size = std::stoll(_params[2])) == 0)
	{
		std::cout << "Error: Invalid size number [" << _params[2] << "]" << std::endl;
		std::cout << "Please, choose a valid unsigned number and greater than zero" << std::endl;
		return false;
	}
	//Getting parameters
	std::string& outputFilePath = _params[1];

	return _operation == bin ? ExecuteBin(outputFilePath, size, ret) : ExecutePwd(outputFilePath, size, ret);
}

void CommandRandomGenerator::PrintHelp()
{
	std::cout
		<< std::endl << "Random Generator operation:"
		<< std::endl << "  - Create a random binary file"
		<< std::endl << "    Sintax:          " << PROJECT_NAME << " -gb <OutputFilePath> <SizeInBytes>"
		<< std::endl << "    <OutputFilePath> Output file path"
		<< std::endl << "    <SizeInBytes>    Size of the output file path"
		<< std::endl << "  - Create a random password file"
		<< std::endl << "    Sintax:          " << PROJECT_NAME << " -gp <OutputFilePath> <SizeInBytes>"
		<< std::endl << "    <OutputFilePath> Output file path"
		<< std::endl << "    <SizeInBytes>    Size of the output file path";
}