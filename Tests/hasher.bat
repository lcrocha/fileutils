@echo off
cls
set APP=..\_bin\Release-x64\FU
set BIN=.\_file.bin
set HASH=%BIN%.hash
set HASHALG=SHA512

::Creating file
%APP% -g %BIN% 1234567

::Creating hash
%APP% -hh %HASHALG% %BIN% %HASH%

::Checking hash
%APP% -hc %HASHALG% %BIN% %HASH%

del /f /q %HASH%
del /f /q %BIN%