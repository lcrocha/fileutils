@echo off
cls
set APP=..\_bin\Release-x64\FU

set BINFILE=.\_file.bin
set CYPHEREDFILE=%BINFILE%.cyphered
set UNCYPHEREDFILE=%BINFILE%.uncyphered
set PASSWORDFILE=%BINFILE%.password


::Creating bin file
%APP% -g %BINFILE% 52345678

::Creating password
%APP% -g %PASSWORDFILE% 128

::Cyphering file
%APP% -pc %PASSWORDFILE% %BINFILE% %CYPHEREDFILE%

::Uncyphering file
%APP% -pu %PASSWORDFILE% %CYPHEREDFILE% %UNCYPHEREDFILE%

::Comparing results
fc /b %BINFILE% %UNCYPHEREDFILE%

del /f /q %BINFILE%
del /f /q %CYPHEREDFILE%
del /f /q %UNCYPHEREDFILE%
del /f /q %PASSWORDFILE%
