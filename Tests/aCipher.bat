@echo off
cls
set APP=..\_bin\Release-x64\FU
set BINFILE=.\_file.bin
set CIPHEREDFILE=%BINFILE%.ciphered
set UNCIPHEREDFILE=%BINFILE%.unciphered
set PRKFILE=.\prk.bin
set PUKFILE=.\puk.bin


::Creating bin file
%APP% -g %BINFILE% 1234567

::Creating key pair
%APP% -ag 512 %PRKFILE% %PUKFILE%

::Ciphering file
%APP% -ac %PUKFILE% %BINFILE% %CIPHEREDFILE%

::Unciphering file
%APP% -au %PRKFILE% %CIPHEREDFILE% %UNCIPHEREDFILE%

::Comparing results
fc /b %BINFILE% %UNCIPHEREDFILE%

del /f /q %BINFILE%
del /f /q %CIPHEREDFILE%
del /f /q %UNCIPHEREDFILE%
del /f /q %PUKFILE%
del /f /q %PRKFILE%
