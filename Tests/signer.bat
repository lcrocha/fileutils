@echo off
cls
set APP=..\_bin\Release-x64\FU
set BINFILE=.\_file.bin
set SIGNFILE=%BINFILE%.signature
set PRKFILE=.\prk.bin
set PUKFILE=.\puk.bin


::Creating bin file
%APP% -g %BINFILE% 1234567

::Creating key pair
%APP% -sg %PRKFILE% %PUKFILE%

::Signing a file
%APP% -ss %PRKFILE% %BINFILE% %SIGNFILE%

::Verifying signature
%APP% -sv %PUKFILE% %BINFILE% %SIGNFILE%

del /f /q %BINFILE%
del /f /q %PRKFILE%
del /f /q %PUKFILE%
del /f /q %SIGNFILE%